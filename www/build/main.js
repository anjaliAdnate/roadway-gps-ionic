webpackJsonp([68],{

/***/ 140:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 140;

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(445);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(446);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl, toastCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.googleMapsAPIKey = "AIzaSyCmSXnoBBGekErvqBvu5N7dbeqdH1pHp7c";
        this.mainUrl = "https://www.oneqlik.in/";
        this.usersURL = "https://www.oneqlik.in/users/";
        this.devicesURL = "https://www.oneqlik.in/devices";
        this.gpsURL = "https://www.oneqlik.in/gps";
        this.geofencingURL = "https://www.oneqlik.in/geofencing";
        this.trackRouteURL = "https://www.oneqlik.in/trackRoute";
        this.groupURL = "https://www.oneqlik.in/groups/";
        this.notifsURL = "https://www.oneqlik.in/notifs";
        this.stoppageURL = "https://www.oneqlik.in/stoppage";
        this.summaryURL = "https://www.oneqlik.in/summary";
        this.shareURL = "https://www.oneqlik.in/share";
        this.appId = "OneQlikVTS";
        console.log('Hello ApiServiceProvider Provider');
    }
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.toastMsgStarted = function () {
        this.toast = this.toastCtrl.create({
            message: "Loading/Refreshing contents please wait...",
            position: "bottom",
            duration: 2000
        });
        return this.toast.present();
    };
    ApiServiceProvider.prototype.toastMsgDismised = function () {
        return this.toast.dismiss();
    };
    ApiServiceProvider.prototype.startLoadingnew = function (key) {
        var str;
        if (key == 1) {
            str = 'unlocking';
        }
        else {
            str = 'locking';
        }
        return this.loading1 = this.loadingCtrl.create({
            content: "Please wait for some time, as we are " + str + " your vehicle...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoadingnw = function () {
        return this.loading1.dismiss();
    };
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ApiServiceProvider.prototype.getCurrency = function () {
        return this.http.get('../assets/json/currency.json')
            .map(function (res) { return res.json(); });
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.updatePOIAPI = function (pay) {
        return this.http.post("https://www.oneqlik.in/vehtra/poi/updatePOI", pay, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addPOIAPI = function (payload) {
        return this.http.post("https://www.oneqlik.in/poi/addpoi", payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deletePOIAPI = function (_id) {
        return this.http.get("https://www.oneqlik.in/poi/deletePoi?_id=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.siginupverifyCall = function (usersignup) {
        return this.http.post(this.usersURL + "/signUpZogo", usersignup, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.resendOtp = function (phnum) {
        return this.http.post(this.usersURL + "/sendOtpZRides", phnum)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getpoireportAPI = function (_id, pageNo, fromT, toT, poiId, devid) {
        return this.http.get("https://www.oneqlik.in/poi/poiReport?user=" + _id + "&s=" + pageNo + "&l=9&from=" + fromT + "&to=" + toT + "&poi=" + poiId + "&device=" + devid, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getPoisAPI = function (id) {
        return this.http.get("https://www.oneqlik.in/poi/getPois?user=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getsingledevice = function (id) {
        return this.http.get(this.devicesURL + "/getDevicebyId?deviceId=" + id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSOSReportAPI = function (url) {
        // return this.http.get("http://192.168.1.20:3000/notifs/SOSReport?from_date=" + starttime + '&to_date=' + endtime + '&dev_id=' + sos_id + '&_u=' + _id, { headers: this.headers })
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.saveGoogleAddressAPI = function (data) {
        var url = this.mainUrl + "googleAddress/addGoogleAddress";
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.urlpasseswithdata = function (url, data) {
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.DealerSearchService = function (_id, pageno, limit, key) {
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDealerCall = function (deletePayload) {
        return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealersCall = function (_id, pageno, limit, searchKey) {
        if (searchKey != undefined)
            return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + searchKey, { headers: this.headers })
                .map(function (res) { return res.json(); });
        else
            return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
                .map(function (res) { return res.json(); });
    };
    // releaseAmount(uid) {
    //   return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid)
    //     .map(res => res.json());
    // }
    // updateTripStatus(uId) {
    //   return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId)
    //     .map(res => res.json());
    // }
    // proceedSecurely(withdrawObj) {
    //   return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj)
    //     .map(res => res.json());
    // }
    // getblockamt() {
    //   return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
    //     .map(res => res.json());
    // }
    // paytmbalance(userCred) {
    //   return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID=' + userCred + "&app_id=" + this.appId)
    //     .map(res => res.json());
    // }
    // addMoneyPaytm(amtObj) {
    //   return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj)
    //     .map(res => res);
    // }
    // paytmLoginWallet(walletcredentials) {
    //   return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials)
    //     .map(res => res.json());
    // }
    // paytmOTPvalidation(otpObj) {
    //   return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj)
    //     .map(res => res.json());
    // }
    // callSearchService(email, id, key) {
    //   return this.http.get(this.devicesURL + "/getDeviceByUser?email=" + email + "&id=" + id + "&skip=0&limit=10&search=" + key, { headers: this.headers })
    //     .map(resp => resp.json());
    // }
    ApiServiceProvider.prototype.callSearchService = function (baseURL) {
        return this.http.get(baseURL, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.callResponse = function (_id) {
        return this.http.get(this.mainUrl + "trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.serverLevelonoff = function (data) {
        return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePassword = function (data) {
        return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateprofile = function (data) {
        return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.get7daysData = function (a, t) {
        return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dataRemoveFuncCall = function (_id, did) {
        return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.tripReviewCall = function (device_id, stime, etime) {
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        return this.http.post(this.shareURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        // console.log("from date => "+ dates.fromDate.toISOString())
        // console.log("new date "+ new Date(dates.fromDate).toISOString())
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        // https://www.oneqlik.in/notifs/getNotifByFilters?from_date=2019-06-13T18:30:00.769Z&to_date=2019-06-14T09:40:16.769Z&user=5cde59324e4d600905f4e690&sortOrder=-1
        // return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getACReportAPI = function (fdate, tdate, userid, imei) {
        return this.http.get(this.notifsURL + '/ACSwitchReport?from_date=' + fdate + '&to_date=' + tdate + '&user=' + userid + '&device=' + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDetailACReportAPI = function (fdate, tdate, userid, imei) {
        return this.http.get(this.notifsURL + "/acReport?from_date=" + fdate + "&to_date=" + tdate + "&_u=" + userid + "&vname=" + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleListCall = function (_url) {
        return this.http.get(_url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (_id, starttime, endtime, did) {
        if (did == undefined) {
            return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
        else {
            return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        return this.http.post(this.trackRouteURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAddressApi = function (data) {
        return this.http.post("https://www.oneqlik.in/gps/getaddress", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport1 = function (url, payload) {
        return this.http.post(url, payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        console.log("payload: ", contactdata);
        return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.createTicketApi = function (contactdata) {
        return this.http.post("https://www.oneqlik.in/customer_support/post_inquiry", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (_id, status, entering, exiting) {
        return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (_id) {
        return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // dashboardcall(email, from, to, _id) {
    //   return this.http.get(this.gpsURL + '/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.dashboardcall = function (_baseUrl) {
        return this.http.get(_baseUrl, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (_id, email) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesForAllVehiclesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .timeout(500000000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        return this.http.get(this.summaryURL + "/summaryReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (_id, time) {
        return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // deviceupdateInCall(devicedetail) {
    //   return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id) {
        return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteCustomerCall = function (data) {
        return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.route_details = function (_id, user_id) {
        return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callcustomerSearchService = function (uid, pageno, limit, seachKey) {
        return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["ToastController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 182:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/ac-report/ac-report.module": [
		530,
		67
	],
	"../pages/add-devices/add-devices.module": [
		594,
		60
	],
	"../pages/add-devices/immobilize/immobilize.module": [
		531,
		59
	],
	"../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module": [
		532,
		58
	],
	"../pages/add-devices/update-device/update-device.module": [
		574,
		57
	],
	"../pages/add-devices/upload-doc/add-doc/add-doc.module": [
		533,
		56
	],
	"../pages/add-devices/upload-doc/upload-doc.module": [
		534,
		66
	],
	"../pages/add-devices/vehicle-details/vehicle-details.module": [
		595,
		14
	],
	"../pages/all-notifications/all-notifications.module": [
		575,
		55
	],
	"../pages/all-notifications/notif-map/notif-map.module": [
		535,
		54
	],
	"../pages/contact-us/contact-us.module": [
		536,
		53
	],
	"../pages/create-trip/create-trip.module": [
		576,
		52
	],
	"../pages/customers/customers.module": [
		577,
		51
	],
	"../pages/customers/modals/add-customer-modal/add-customer-modal.module": [
		578,
		50
	],
	"../pages/customers/modals/add-device-modal.module": [
		579,
		49
	],
	"../pages/customers/modals/group-modal/group-modal.module": [
		537,
		48
	],
	"../pages/customers/modals/update-cust/update-cust.module": [
		580,
		47
	],
	"../pages/daily-report-new/daily-report-new.module": [
		582,
		46
	],
	"../pages/daily-report/daily-report.module": [
		581,
		45
	],
	"../pages/dashboard/dashboard.module": [
		592,
		2
	],
	"../pages/dealers/add-dealer/add-dealer.module": [
		538,
		44
	],
	"../pages/dealers/dealers.module": [
		583,
		43
	],
	"../pages/dealers/edit-dealer/edit-dealer.module": [
		539,
		42
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		541,
		13
	],
	"../pages/distance-report/distance-report.module": [
		540,
		12
	],
	"../pages/expenses/add-expense/add-expense.module": [
		542,
		41
	],
	"../pages/expenses/expense-type-detail/expense-type-detail.module": [
		543,
		40
	],
	"../pages/expenses/expenses.module": [
		544,
		39
	],
	"../pages/fastag-list/fastag-list.module": [
		545,
		38
	],
	"../pages/fastag-list/fastag/fastag.module": [
		546,
		65
	],
	"../pages/feedback/feedback.module": [
		547,
		0
	],
	"../pages/fuel/fuel-chart/fuel-chart.module": [
		548,
		11
	],
	"../pages/fuel/fuel-consumption-report/fuel-consumption-report.module": [
		584,
		10
	],
	"../pages/fuel/fuel-consumption/fuel-consumption.module": [
		549,
		64
	],
	"../pages/fuel/fuel-events/fuel-events.module": [
		550,
		1
	],
	"../pages/geofence-report/geofence-report.module": [
		551,
		37
	],
	"../pages/geofence/add-geofence/add-geofence.module": [
		585,
		36
	],
	"../pages/geofence/geofence-show/geofence-show.module": [
		552,
		35
	],
	"../pages/geofence/geofence.module": [
		586,
		34
	],
	"../pages/groups/groups.module": [
		553,
		33
	],
	"../pages/history-device/history-device.module": [
		593,
		32
	],
	"../pages/idle-report/idle-report.module": [
		554,
		31
	],
	"../pages/ignition-report/ignition-report.module": [
		555,
		9
	],
	"../pages/live-single-device/device-settings/device-settings.module": [
		556,
		30
	],
	"../pages/live-single-device/live-single-device.module": [
		596,
		63
	],
	"../pages/live/expired/expired.module": [
		557,
		29
	],
	"../pages/live/live.module": [
		597,
		28
	],
	"../pages/login/login.module": [
		587,
		62
	],
	"../pages/maintenance-reminder/add-reminder/add-reminder.module": [
		558,
		27
	],
	"../pages/maintenance-reminder/maintenance-reminder.module": [
		590,
		26
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		559,
		8
	],
	"../pages/poi-list/add-poi/add-poi.module": [
		591,
		25
	],
	"../pages/poi-list/poi-list.module": [
		560,
		7
	],
	"../pages/poi-report/poi-report.module": [
		561,
		24
	],
	"../pages/profile/profile.module": [
		588,
		61
	],
	"../pages/profile/settings/notif-setting/notif-setting.module": [
		562,
		23
	],
	"../pages/profile/settings/settings.module": [
		563,
		22
	],
	"../pages/route-map-show/route-map-show.module": [
		564,
		21
	],
	"../pages/route-voilations/route-voilations.module": [
		565,
		20
	],
	"../pages/route/route.module": [
		566,
		19
	],
	"../pages/signup/signup-otp/signup-otp.module": [
		567,
		18
	],
	"../pages/signup/signup.module": [
		568,
		17
	],
	"../pages/sos-report/sos-report.module": [
		570,
		16
	],
	"../pages/speed-repo/speed-repo.module": [
		569,
		6
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		571,
		5
	],
	"../pages/trip-report/trip-report.module": [
		572,
		4
	],
	"../pages/trip-report/trip-review/trip-review.module": [
		589,
		15
	],
	"../pages/your-trips/your-trips.module": [
		573,
		3
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 182;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewDoc; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ViewDoc = /** @class */ (function () {
    function ViewDoc(navparams, viewCtrl) {
        this.navparams = navparams;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("param1: ", this.navparams.get("param1"));
        this._instData = this.navparams.get("param1");
        var str = this._instData.imageURL;
        var str1 = str.split('public/');
        this.imgUrl = "https://www.oneqlik.in/" + str1[1];
        console.log("img url: ", this._instData);
    }
    ViewDoc.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewDoc = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-view-doc',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\add-devices\upload-doc\view-doc\view-doc.html"*/'<div>\n\n    <ion-row>\n\n        <ion-col col-11>\n\n\n\n        </ion-col>\n\n        <ion-col col-1>\n\n            <ion-icon style="font-size:1.5em" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col style="text-align: center;">\n\n            <img src="{{imgUrl}}" width="300" height="450" />\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row padding-left>\n\n        <ion-col col-3>\n\n            <b>{{ "Expiry Date:" | translate }}</b>\n\n        </ion-col>\n\n        <ion-col col-9>{{_instData.expDate | date: \'medium\'}}</ion-col>\n\n    </ion-row>\n\n</div>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\add-devices\upload-doc\view-doc\view-doc.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"]])
    ], ViewDoc);
    return ViewDoc;
}());

//# sourceMappingURL=view-doc.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this._status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Online) {
                _this.setStatus(ConnectionStatusEnum.Offline);
                _this.eventCtrl.publish('network:offline');
            }
            // if (this.previousStatus === ConnectionStatusEnum.Online) {
            //   this.eventCtrl.publish('network:offline');
            // }
            // this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Offline) {
                _this.setStatus(ConnectionStatusEnum.Online);
                _this.eventCtrl.publish('network:online');
            }
            // setTimeout(() => {
            //   if (this.previousStatus === ConnectionStatusEnum.Offline) {
            //     this.eventCtrl.publish('network:online');
            //   }
            //   this.previousStatus = ConnectionStatusEnum.Online;
            // }, 3000);
        });
    };
    NetworkProvider.prototype.getNetworkType = function () {
        return this.network.type;
    };
    NetworkProvider.prototype.getNetworkStatus = function () {
        return this._status.asObservable();
    };
    NetworkProvider.prototype.setStatus = function (status) {
        this.status = status;
        this._status.next(this.status);
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
    }
    MenuProvider.prototype.getSideMenus = function () {
        return [{
                title: 'Home', component: 'DashboardPage', icon: 'home'
            }, {
                title: 'Groups', component: 'GroupsPage', icon: 'people'
            }, {
                title: 'Customers', component: 'CustomersPage', icon: 'contacts'
            }, {
                title: 'Notifications', component: 'AllNotificationsPage', icon: 'notifications'
            },
            {
                title: 'Reports',
                subPages: [{
                        title: 'Daily Report',
                        component: 'DailyReportPage',
                    }, {
                        title: 'Value Screen',
                        component: 'DailyReportNewPage',
                    }, {
                        title: 'Speed Variation Report',
                        component: 'SpeedRepoPage',
                    }, {
                        title: 'Summary Report',
                        component: 'DeviceSummaryRepoPage',
                    }, {
                        title: 'Geofenceing Report',
                        component: 'GeofenceReportPage',
                    },
                    {
                        title: 'Overspeed Report',
                        component: 'OverSpeedRepoPage',
                    }, {
                        title: 'Ignition Report',
                        component: 'IgnitionReportPage',
                    },
                    {
                        title: 'Route Violation Report',
                        component: 'RouteVoilationsPage',
                    }, {
                        title: 'Stoppage Report',
                        component: 'StoppagesRepoPage',
                    },
                    {
                        title: 'Fuel Report',
                        component: 'FuelReportPage',
                    }, {
                        title: 'Distnace Report',
                        component: 'DistanceReportPage',
                    },
                    {
                        title: 'Trip Report',
                        component: 'TripReportPage',
                    }],
                icon: 'clipboard'
            },
            {
                title: 'Routes',
                subPages: [{
                        title: 'Routes',
                        component: 'RoutePage',
                    }, {
                        title: 'Route Maping',
                        component: 'RouteMapPage',
                    }],
                icon: 'map'
            }, {
                title: 'Feedback', component: 'FeedbackPage', icon: 'paper'
            }, {
                title: 'Contact Us', component: 'ContactUsPage', icon: 'mail'
            }, {
                title: 'Support', component: 'SupportPage', icon: 'call'
            }, {
                title: 'About Us', component: 'AboutUsPage', icon: 'alert'
            },];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(521);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\shared\side-menu-content\side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n        <!-- It is a simple option -->\n        <ng-template [ngIf]="!option.suboptionsCount">\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                (tap)="select(option)" tappable>\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n                {{ option.displayText }}\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n            </ion-item>\n        </ng-template>\n\n        <!-- It has nested options -->\n        <ng-template [ngIf]="option.suboptionsCount">\n\n            <ion-list no-margin class="accordion-menu">\n\n                <!-- Header -->\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n                    (tap)="toggleItemOptions(option)" tappable>\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n                        item-left></ion-icon>\n                    <!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n                    {{ option.displayText }}\n                </ion-item>\n\n                <!-- Sub items -->\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n                            tappable (tap)="select(item)">\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n                            {{ item.displayText }}\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n                        </ion-item>\n                    </ng-template>\n                </div>\n            </ion-list>\n\n        </ng-template>\n\n    </ng-template>\n</ion-list>'/*ion-inline-end:"D:\New\roadway-gps-ionic\shared\side-menu-content\side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoginPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder, alertCtrl, apiservice, toastCtrl, popoverCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.apiservice = apiservice;
        this.toastCtrl = toastCtrl;
        this.popoverCtrl = popoverCtrl;
        this.translate = translate;
        this.showPassword = false;
        this.loginForm = formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    LoginPage_1 = LoginPage;
    // presentPopover() {
    //   let popover = this.popoverCtrl.create(LanguagesPage, {}, { enableBackdropDismiss: false });
    //   // popover.onDidDismiss(() => {
    //   //   var lng = localStorage.getItem("LANG_KEY");
    //   //   this.translate.use(lng);
    //   // })
    //   popover.present({});
    // }
    LoginPage.prototype.ionViewWillEnter = function () {
        console.log("ionViewWillEnter");
        // if (localStorage.getItem("LANG_KEY") == null) {
        //   this.presentPopover();
        // }
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        console.log("ionViewDidEnter");
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
        // this.presentPopover()
        // let alert = this.alertCtrl.create({
        //   message: "Select languages"
        // })
        // alert.present();
    };
    LoginPage.prototype.toggleShowPassword = function () {
        this.showPassword = !this.showPassword;
    };
    LoginPage.prototype.userlogin = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.loginForm.value.username == "") {
            /*alert("invalid");*/
            return false;
        }
        else if (this.loginForm.value.password == "") {
            /*alert("invalid");*/
            return false;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        var isEmail = validateEmail(this.loginForm.value.username);
        var isName = isNaN(this.loginForm.value.username);
        var strNum;
        if (isName == false) {
            strNum = this.loginForm.value.username.trim();
        }
        if (isEmail == false && isName == false && strNum.length == 10) {
            this.data = {
                "psd": this.loginForm.value.password,
                "ph_num": this.loginForm.value.username
            };
        }
        else if (isEmail) {
            this.data = {
                "psd": this.loginForm.value.password,
                "emailid": this.loginForm.value.username
            };
        }
        else {
            this.data = {
                "psd": this.loginForm.value.password,
                "user_id": this.loginForm.value.username
            };
        }
        this.apiservice.startLoading();
        this.apiservice.loginApi(this.data)
            .subscribe(function (response) {
            _this.logindata = response;
            _this.logindata = JSON.stringify(response);
            var logindetails = JSON.parse(_this.logindata);
            _this.userDetails = window.atob(logindetails.token.split('.')[1]);
            _this.details = JSON.parse(_this.userDetails);
            console.log(_this.details.email);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(_this.details));
            localStorage.setItem('condition_chk', _this.details.isDealer);
            localStorage.setItem('VeryFirstLoginUser', _this.details._id);
            localStorage.setItem("INTRO", "INTRO");
            _this.apiservice.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("Welcome! You're logged In successfully."),
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.setRoot('DashboardPage');
            });
            toast.present();
        }, function (error) {
            // console.log("login error => "+error)
            var body = error._body;
            var msg = JSON.parse(body);
            if (msg.message == "Mobile Phone Not Verified") {
                var confirmPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message = !msg.message ? '  Do you Want to verify it?' : msg.message + '  Do you Want to verify it?',
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: _this.translate.instant('Okay'),
                            handler: function () {
                                // this.navCtrl.push(MobileVerify);
                            }
                        }
                    ]
                });
                confirmPopup.present();
            }
            else {
                // Do something on error
                var alertPopup = _this.alertCtrl.create({
                    title: 'Login failed!',
                    message: msg.message,
                    buttons: [_this.translate.instant('Okay')]
                });
                alertPopup.present();
            }
            _this.apiservice.stopLoading();
        });
    };
    LoginPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2500
        });
        toast.present();
    };
    LoginPage.prototype.gotosignuppage = function () {
        this.navCtrl.push('SignupPage');
    };
    LoginPage.prototype.forgotPassFunc = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Forgot Password'),
            message: this.translate.instant('Enter your Registered Mobile Number and we will send you a confirmation code.'),
            inputs: [
                {
                    name: 'mobno',
                    placeholder: this.translate.instant('Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('SEND CONFIRMATION CODE'),
                    handler: function (data) {
                        var forgotdata = {
                            "cred": data.mobno
                        };
                        _this.apiservice.startLoading();
                        _this.apiservice.forgotPassApi(forgotdata)
                            .subscribe(function (data) {
                            _this.apiservice.stopLoading();
                            _this.presentToast(data.message);
                            _this.otpMess = data;
                            _this.PassWordConfirmPopup();
                        }, function (error) {
                            _this.apiservice.stopLoading();
                            var body = error._body;
                            var msg = JSON.parse(body);
                            var alert = _this.alertCtrl.create({
                                title: _this.translate.instant('Forgot Password Failed!'),
                                message: msg.message,
                                buttons: [_this.translate.instant('Okay')]
                            });
                            alert.present();
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.PassWordConfirmPopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Reset Password'),
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'newpass',
                    placeholder: this.translate.instant('Password')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Back'),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translate.instant('SAVE'),
                    handler: function (data) {
                        if (!data.newpass || !data.mobilenum || !data.confCode) {
                            var alertPopup = _this.alertCtrl.create({
                                title: _this.translate.instant('Warning'),
                                message: _this.translate.instant('Fill all mandatory fields!'),
                                buttons: [_this.translate.instant('Okay')]
                            });
                            alertPopup.present();
                        }
                        else {
                            if (data.newpass == data.mobilenum && data.newpass && data.mobilenum) {
                                if (data.newpass.length < 6 || data.newpass.length > 12) {
                                    var Popup = _this.alertCtrl.create({
                                        title: _this.translate.instant('Warning'),
                                        message: _this.translate.instant('Password length should be 6 - 12'),
                                        buttons: [_this.translate.instant('Okay')]
                                    });
                                    Popup.present();
                                }
                                else {
                                    var Passwordset = {
                                        "newpwd": data.newpass,
                                        "otp": data.confCode,
                                        "phone": _this.otpMess,
                                        "cred": _this.otpMess
                                    };
                                    _this.apiservice.startLoading();
                                    _this.apiservice.forgotPassMobApi(Passwordset)
                                        .subscribe(function (data) {
                                        _this.apiservice.stopLoading();
                                        _this.presentToast(data.message);
                                        _this.navCtrl.setRoot(LoginPage_1);
                                    }, function (error) {
                                        _this.apiservice.stopLoading();
                                        var body = error._body;
                                        var msg = JSON.parse(body);
                                        var alert = _this.alertCtrl.create({
                                            title: _this.translate.instant('Forgot Password Failed!'),
                                            message: msg.message,
                                            buttons: [_this.translate.instant('Okay')]
                                        });
                                        alert.present();
                                    });
                                }
                            }
                            else {
                                var alertPopup = _this.alertCtrl.create({
                                    title: _this.translate.instant('Warning'),
                                    message: _this.translate.instant('New Password and Confirm Password Not Matched'),
                                    buttons: [_this.translate.instant('Okay')]
                                });
                                alertPopup.present();
                            }
                            if (!data.newpass || !data.mobilenum || !data.confCode) {
                                //don't allow the user to close unless he enters model...
                                return false;
                            }
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\login\login.html"*/'<ion-content id=outer>\n\n  <div class="warning">\n\n    <!-- <div id=inner> -->\n\n    <div class="logo">\n\n      <img src="assets/imgs/icon.png">\n\n    </div>\n\n    <form [formGroup]="loginForm">\n\n      <div class="temp">\n\n        <ion-item class="logitem">\n\n          <ion-input formControlName="username" type="text" placeholder="{{\'Email/Mobile*\' | translate}}"></ion-input>\n\n        </ion-item>\n\n      </div>\n\n      <ion-item class="logitem1" *ngIf="!loginForm.controls.username.valid && (loginForm.controls.username.dirty || submitAttempt)">\n\n        <p>{{\'username required!\' | translate}}</p>\n\n      </ion-item>\n\n      <div class="temp">\n\n        <ion-item class="logitem">\n\n          <ion-input formControlName="password" *ngIf="!showPassword" type="password" placeholder="{{\'Password*\' | translate}}"></ion-input>\n\n          <ion-input formControlName="password" *ngIf="showPassword" type="text" placeholder="{{\'Password*\' | translate}}"></ion-input>\n\n\n\n          <button ion-button clear item-end (click)="toggleShowPassword()">\n\n            <ion-icon style="font-size: 1.6em;" *ngIf="showPassword" name="eye" color="light"></ion-icon>\n\n            <ion-icon style="font-size: 1.6em;" *ngIf="!showPassword" name="eye-off" color="light"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n      </div>\n\n      <ion-item class="logitem1" *ngIf="!loginForm.controls.password.valid && (loginForm.controls.password.dirty || submitAttempt)">\n\n        <p>{{\'Password required!\' | translate}}</p>\n\n      </ion-item>\n\n    </form>\n\n    <div class="btnDiv">\n\n      <button ion-button class="btnLog" color="gpsc" (tap)="userlogin()">{{\'SIGN IN\' | translate}}</button>\n\n      <ion-row>\n\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;" (tap)="forgotPassFunc()">{{\'Forgot Password\' | translate}} ?</ion-col>\n\n        <ion-col width-50 ion-text color="light" style="font-size: 1.1em;">{{\'New user?\' | translate}}&nbsp;&nbsp;\n\n          <span style="color:#d80622;" (tap)="gotosignuppage()">{{\'SIGN UP\' | translate}}</span>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n    <!-- </div> -->\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1;
}());

var LanguagesPage = /** @class */ (function () {
    function LanguagesPage(viewCtrl, translate, apiCall, toastCtrl, events) {
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    LanguagesPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    LanguagesPage.prototype.setLanguage = function (key) {
        var _this = this;
        var payload = {
            uid: this.islogin._id,
            lang: key
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + resp);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: resp.message,
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        });
        this.events.publish('lang:key', key);
        // this language will be used as a fallback when a translation isn't found in the current language
        // this.translate.use(key);
        // localStorage.setItem("LANG_KEY", key);
        this.viewCtrl.dismiss();
    };
    LanguagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-list-header style=\"text-align: center;\">{{'Select Language' | translate}}</ion-list-header>\n      <button ion-item (click)=\"setLanguage('en')\">{{'English' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('hi')\">{{'Hindi' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('bn')\">{{'Bangali' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('te')\">{{'Telugu' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ta')\">{{'Tamil' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('gu')\">{{'Gujarati' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('kn')\">{{'Kannada' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('mr')\">{{'Marathi' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ml')\">{{'Malayalam' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('sp')\">{{'Spanish' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('fa')\">{{'Persian' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('ar')\">{{'Arabic' | translate}}</button>\n      <button ion-item (click)=\"setLanguage('np')\">{{'Nepali' | translate}}</button>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], LanguagesPage);
    return LanguagesPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiveSingleDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_native_page_transitions__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_geocoder_geocoder__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_Subscription__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__live_map_style_model__ = __webpack_require__(501);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var LiveSingleDevice = /** @class */ (function () {
    function LiveSingleDevice(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, platform, event, modalCtrl, socialSharing, alertCtrl, toastCtrl, storage, plt, nativePageTransitions, viewCtrl, translate, geocoderApi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.platform = platform;
        this.event = event;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.plt = plt;
        this.nativePageTransitions = nativePageTransitions;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.geocoderApi = geocoderApi;
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.drawerState1 = __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.dockedHeight1 = 150;
        this.distanceTop1 = 378;
        this.minimumHeight1 = 0;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.service = new google.maps.DistanceMatrixService();
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.mapHideTraffic = false;
        this.locateme = false;
        this.deviceDeatils = {};
        this.tempArray = [];
        this.mapTrail = false;
        this.condition = 'gpsc';
        this.condition1 = 'light';
        this.condition2 = 'light';
        this.showaddpoibtn = false;
        this.btnString = "Create Trip";
        this.showIcon = false;
        this.expectation = {};
        this.polyLines = [];
        this.marksArray = [];
        this.shwBckBtn = false;
        this.zoomLevel = 18;
        this.markersArray = [];
        this.devices = [];
        this.latLngArray = [];
        this.fraction = 0;
        this.direction = 1;
        this.navigateBtn = false;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [10, 20],
        };
        this.userIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon,
            "user": this.userIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon
        };
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_13_rxjs_Subscription__["Subscription"]();
        // Environment.setBackgroundColor("black");
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.event.subscribe("tripstatUpdated", function (data) {
            if (data) {
                _this.checktripstat();
            }
        });
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device") != null) {
            this.deviceDeatils = navParams.get("device");
        }
        this.motionActivity = 'Activity';
        this.menuActive = false;
    }
    LiveSingleDevice.prototype.ngOnInit = function () {
        var that = this;
        that.tempArray = [];
        this.checktripstat();
    };
    LiveSingleDevice.prototype.ionViewDidLoad = function () {
        // this.setDocHeightAtFirst();
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = true;
        var paramData = this.navParams.get("device");
        this.titleText = paramData.Device_Name;
        this.temp(paramData);
        this.showActionSheet = true;
    };
    LiveSingleDevice.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.platform.ready().then(function () {
            _this.resumeListener = _this.platform.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant('Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?'),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LiveSingleDevice.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LiveSingleDevice.prototype.ngOnDestroy = function () {
        var that = this;
        if (that.intevalId) {
            clearInterval(that.intevalId);
        }
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that._io.on('disconnect', function () {
            that._io.open();
        });
        if (localStorage.getItem("livepagetravelDetailsObject") != null) {
            localStorage.removeItem("livepagetravelDetailsObject");
        }
    };
    LiveSingleDevice.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.setDocHeightAtFirst = function () {
        var that = this;
        that.distanceTop = 80;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.showNearby = function (key) {
        var _this = this;
        // for police stations
        var url, icurl;
        if (key === 'police') {
            // url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.recenterMeLat + "," + this.recenterMeLng + "&radius=1000&types=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
            icurl = './assets/imgs/police-station.png';
        }
        else if (key === 'petrol') {
            // url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.recenterMeLat + "," + this.recenterMeLng + "&radius=1000&types=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
            icurl = './assets/imgs/gas-pump.png';
        }
        console.log("google maps activity: ", url);
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            console.log(resp.status);
            if (resp.status === 'OK') {
                console.log(resp.results);
                console.log(resp);
                var mapData = resp.results.map(function (d) {
                    return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
                });
                var bounds = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["g" /* LatLngBounds */](mapData);
                for (var t = 0; t < resp.results.length; t++) {
                    _this.allData.map.addMarker({
                        title: resp.results[t].name,
                        position: {
                            lat: resp.results[t].geometry.location.lat,
                            lng: resp.results[t].geometry.location.lng
                        },
                        icon: icurl
                    }).then(function (mark) {
                    });
                }
                _this.allData.map.moveCamera({
                    target: bounds
                });
                _this.allData.map.setPadding(20, 20, 20, 20);
                _this.zoomLevel = 10;
            }
            else if (resp.status === 'ZERO_RESULTS') {
                if (key === 'police') {
                    _this.showToastMsg('No nearby police stations found.');
                }
                else if (key === 'petrol') {
                    _this.showToastMsg('No nearby petrol pump found.');
                }
            }
            else if (resp.status === 'OVER_QUERY_LIMIT') {
                _this.showToastMsg('Query limit exeed. Please try after some time.');
            }
            else if (resp.status === 'REQUEST_DENIED') {
                _this.showToastMsg('Please check your API key.');
            }
            else if (resp.status === 'INVALID_REQUEST') {
                _this.showToastMsg('Invalid request. Please try again.');
            }
            else if (resp.status === 'UNKNOWN_ERROR') {
                _this.showToastMsg('An unknown error occured. Please try again.');
            }
        });
    };
    LiveSingleDevice.prototype.showToastMsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    LiveSingleDevice.prototype.goBack = function () {
        if (this.navCtrl.canGoBack()) {
            var options = {
                direction: 'left',
                duration: 500,
                slowdownfactor: 3,
                slidePixels: 20,
                iosdelay: 100,
                androiddelay: 150,
                fixedPixelsTop: 0,
                fixedPixelsBottom: 60
            };
            this.nativePageTransitions.slide(options);
            this.navCtrl.pop();
        }
        else {
            var options = {
                duration: 700
            };
            this.nativePageTransitions.fade(options);
            this.navCtrl.setRoot('DashboardPage');
        }
    };
    LiveSingleDevice.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ngOnInit();
        this.ionViewDidLoad();
    };
    LiveSingleDevice.prototype.refreshMe1 = function () {
        this.ngOnDestroy();
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        var paramData = this.navParams.get("device");
        this.socketInit(paramData);
    };
    LiveSingleDevice.prototype.reCenterMe = function () {
        // console.log("getzoom level: " + this.allData.map.getCameraZoom());
        this.allData.map.moveCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            zoom: this.allData.map.getCameraZoom()
        }).then(function () {
        });
    };
    LiveSingleDevice.prototype.newMap = function () {
        var mapOptions = {
            camera: { zoom: 10 },
            gestures: {
                rotate: false,
                tilt: true
            }
        };
        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
        // Environment.setBackgroundColor("black");
        return map;
    };
    LiveSingleDevice.prototype.isNight = function () {
        //Returns true if the time is between
        //7pm to 5am
        var time = new Date().getHours();
        return (time > 5 && time < 19) ? false : true;
    };
    LiveSingleDevice.prototype.checktripstat = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "user_trip/getLastTrip?device=" + this.deviceDeatils._id + "&status=Started&tripInfo=last_trip";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            // debugger
            if (!data.message) {
                _this.allData.tripStat = data[0].trip_status;
                if (_this.allData.tripStat == 'Started') {
                    var sources = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].start_lat), parseFloat(data[0].start_long));
                    var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].end_lat), parseFloat(data[0].end_long));
                    _this.calcRoute(sources, dest);
                }
            }
        });
    };
    LiveSingleDevice.prototype.navigateFromCurrentLoc = function () {
        var _this = this;
        this.navigateBtn = true;
        this.allData.map.getMyLocation().then(function (location) {
            var myLocCoords = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](location.latLng.lat, location.latLng.lng);
            var vehLocCoords = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](_this.recenterMeLat, _this.recenterMeLng);
            _this.calcRoute(myLocCoords, vehLocCoords);
        });
    };
    LiveSingleDevice.prototype.calcRoute = function (start, end) {
        this.allData.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var that = this;
        var request = {
            origin: start,
            destination: end,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that.allData.AIR_PORTS.push({
                        lat: path.g[i].lat(), lng: path.g[i].lng()
                    });
                    // debugger
                    if (that.allData.AIR_PORTS.length > 1) {
                        if (!that.navigateBtn) {
                            that.allData.map.addMarker({
                                title: 'My Location',
                                position: start,
                                icon: 'green'
                            }).then(function (mark) {
                                that.marksArray.push(mark);
                            });
                            that.allData.map.addMarker({
                                title: 'Destination',
                                position: end,
                                icon: 'red'
                            }).then(function (mark) {
                                that.marksArray.push(mark);
                            });
                        }
                        that.allData.map.addPolyline({
                            'points': that.allData.AIR_PORTS,
                            'color': '#4aa9d5',
                            'width': 4,
                            'geodesic': true,
                        }).then(function (poly) {
                            that.polyLines.push(poly);
                            that.getTravelDetails(start, end);
                            //   that.showBtn = true;
                        });
                    }
                }
                that.apiCall.stopLoading();
            }
        });
    };
    LiveSingleDevice.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        var that = this;
        this._id = setInterval(function () {
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
                    console.log("expectation: ", that.expectation);
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
    };
    LiveSingleDevice.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    LiveSingleDevice.prototype.diff = function (start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        // If using time pickers with 24 hours format, add the below line get exact hours
        if (hours < 0)
            hours = hours + 24;
        return (hours <= 9 ? "0" : "") + hours + " hours " + (minutes <= 9 ? "0" : "") + minutes + " mins";
    };
    LiveSingleDevice.prototype.callendtripfunc = function (res) {
        var _this = this;
        var endtime = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(), 'DD/MM/YYY').format('hh:mm');
        var starttime = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(res.doc.start_time), 'DD/MM/YYYY').format('hh:mm');
        var duration = this.diff(starttime, endtime);
        var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](res.doc.start_lat, res.doc.start_long);
        var dest2 = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](res.doc.end_lat, res.doc.end_long);
        var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, dest2); // in meters
        var bUrl = this.apiCall.mainUrl + "user_trip/updatePlantrip";
        var payload = {
            "user": res.doc.user,
            "device": res.doc.device,
            "start_loc": {
                "lat": res.doc.start_lat,
                "long": res.doc.start_long
            },
            "end_loc": {
                "lat": res.doc.end_lat,
                "long": res.doc.end_long,
            },
            "duration": duration,
            "distance": distance,
            "trip_status": "completed",
            "trip_id": res.doc._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(bUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("tri end data: ", data);
            // alert("trip ended succesfully");
            var toast = _this.toastCtrl.create({
                message: 'Trip has ended succesfully.',
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
            var that = _this;
            // debugger
            that.allData.tripStat = 'completed';
            if (that.polyLines.length > 0) {
                for (var t = 0; t < that.polyLines.length; t++) {
                    that.polyLines[t].remove();
                }
            }
            if (that.marksArray.length > 0) {
                for (var e = 0; e < that.marksArray.length; e++) {
                    that.marksArray[e].remove();
                }
            }
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                localStorage.removeItem("livepagetravelDetailsObject");
            }
            _this.checktripstat();
        });
    };
    LiveSingleDevice.prototype.endTrip = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant('Confirm'),
            message: this.translate.instant('Do you want to end the trip?'),
            buttons: [
                {
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        _this.storage.get("TRIPDATA").then(function (res) {
                            if (res) {
                                _this.callendtripfunc(res);
                            }
                        });
                    }
                },
                {
                    text: this.translate.instant('Back')
                }
            ]
        });
        alert.present();
    };
    LiveSingleDevice.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].SATELLITE);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
                else {
                    if (maptype == 'HYBRID') {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
                    }
                }
            }
        }
    };
    LiveSingleDevice.prototype.createTrip = function () {
        console.log("create trip data: ", this.deviceDeatils);
        this.navCtrl.push('CreateTripPage', {
            'paramData': this.deviceDeatils,
        });
    };
    LiveSingleDevice.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.shareLive = function () {
        var that = this;
        that.showActionSheet = false;
        // that.drawerHidden = false;
        that.drawerState1 = __WEBPACK_IMPORTED_MODULE_12_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        that.showFooter = true;
    };
    LiveSingleDevice.prototype.sharedevices = function (param) {
        var that = this;
        if (param == '15mins') {
            that.condition = 'gpsc';
            that.condition1 = 'light';
            that.condition2 = 'light';
            that.tttime = 15;
            // that.tttime  = (15 * 60000); //for miliseconds
        }
        else {
            if (param == '1hour') {
                that.condition1 = 'gpsc';
                that.condition = 'light';
                that.condition2 = 'light';
                that.tttime = 60;
                // that.tttime  = (1 * 3600000); //for miliseconds
            }
            else {
                if (param == '8hours') {
                    that.condition2 = 'gpsc';
                    that.condition = 'light';
                    that.condition1 = 'light';
                    that.tttime = (8 * 60);
                    // that.tttime  = (8 * 3600000);
                }
            }
        }
    };
    LiveSingleDevice.prototype.shareLivetemp = function () {
        var _this = this;
        var that = this;
        if (that.tttime == undefined) {
            that.tttime = 15;
        }
        var data = {
            id: that.liveDataShare._id,
            imei: that.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: that.tttime // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LiveSingleDevice.prototype.liveShare = function () {
        var that = this;
        var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
        that.showActionSheet = true;
        that.showFooter = false;
        that.tttime = undefined;
    };
    LiveSingleDevice.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LiveSingleDevice.prototype.settings = function () {
        var _this = this;
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LiveSingleDevice.prototype.getAddress = function (coordinates) {
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.saveAddressToServer(str, coordinates.lat, coordinates.long);
            that.address = str;
        });
    };
    LiveSingleDevice.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    LiveSingleDevice.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        var that = this;
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                console.log("ping data: => ", d4);
            (function (data) {
                if (data == undefined) {
                    return;
                }
                localStorage.setItem("pdata", JSON.stringify(data));
                if (data._id != undefined && data.last_location != undefined) {
                    var key = data._id;
                    that.impkey = data._id;
                    that.deviceDeatils = data;
                    // let ic = _.cloneDeep(that.icons[data.iconType]);
                    // if (!ic) {
                    //   return;
                    // }
                    // ic.path = null;
                    console.log("status: " + data.status.toLowerCase());
                    console.log("last speed: " + data.last_speed);
                    console.log(that.getIconUrl(data));
                    // that.vehicle_speed = data.last_speed;
                    // that.todays_odo = data.today_odo;
                    // that.total_odo = data.total_odo;
                    // if (that.userdetails.fuel_unit == 'LITRE') {
                    //   that.fuel = data.currentFuel;
                    // } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
                    //   that.fuel = data.fuel_percent;
                    // } else {
                    //   that.fuel = data.currentFuel;
                    // }
                    // that.last_ping_on = data.last_ping_on;
                    // var tempvar = new Date(data.lastStoppedAt);
                    // if (data.lastStoppedAt != null) {
                    //   var fd = tempvar.getTime();
                    //   var td = new Date().getTime();
                    //   var time_difference = td - fd;
                    //   var total_min = time_difference / 60000;
                    //   var hours = total_min / 60
                    //   var rhours = Math.floor(hours);
                    //   var minutes = (hours - rhours) * 60;
                    //   var rminutes = Math.round(minutes);
                    //   that.lastStoppedAt = rhours + ':' + rminutes;
                    // } else {
                    //   that.lastStoppedAt = '00' + ':' + '00';
                    // }
                    // that.distFromLastStop = data.distFromLastStop;
                    // if (!isNaN(data.timeAtLastStop)) {
                    //   that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                    // } else {
                    //   that.timeAtLastStop = '00:00:00';
                    // }
                    // that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                    // that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                    // that.last_ACC = data.last_ACC;
                    // that.acModel = data.ac;
                    // that.currentFuel = data.currentFuel;
                    // that.power = data.power;
                    // that.gpsTracking = data.gpsTracking;
                    // that.recenterMeLat = data.last_location.lat;
                    // that.recenterMeLng = data.last_location.long;
                    that.otherValues(data);
                    if (that.allData[key]) {
                        that.socketSwitch[key] = data;
                        console.log("check mark is avalible or not? ", that.allData[key].mark);
                        if (that.allData[key].mark !== undefined) {
                            that.allData[key].mark.setIcon(that.getIconUrl(data));
                            // that.allData[key].mark.setIcon(ic);
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            var temp = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.allData[key].poly[1]);
                            that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.getAddress(data.last_location);
                            that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                        }
                        else {
                            return;
                        }
                    }
                    else {
                        that.allData[key] = {};
                        that.socketSwitch[key] = data;
                        that.allData[key].poly = [];
                        if (data.sec_last_location) {
                            that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                        }
                        else {
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        }
                        that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        if (data.last_location != undefined) {
                            that.allData.map.addMarker({
                                title: data.Device_Name,
                                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                icon: that.getIconUrl(data),
                            }).then(function (marker) {
                                that.allData[key].mark = marker;
                                marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                    .subscribe(function (e) {
                                    that.liveVehicleName = data.Device_Name;
                                    that.showActionSheet = true;
                                    that.showaddpoibtn = true;
                                    that.getAddressTitle(marker);
                                });
                                that.getAddress(data.last_location);
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
                            });
                        }
                    }
                }
            })(d4);
        });
    };
    LiveSingleDevice.prototype.getAddressTitle = function (marker) {
        this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            marker.setSnippet(str);
        });
    };
    LiveSingleDevice.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(PoiPage, {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    /////////// Anjali code ////////
    LiveSingleDevice.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                }
            }
        }
        return iconUrl;
    };
    LiveSingleDevice.prototype.otherValues = function (data) {
        var that = this;
        that.vehicle_speed = data.last_speed;
        that.todays_odo = data.today_odo;
        that.total_odo = data.total_odo;
        if (that.userdetails.fuel_unit == 'LITRE') {
            that.fuel = data.currentFuel;
        }
        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
            that.fuel = data.fuel_percent;
        }
        else {
            that.fuel = data.currentFuel;
        }
        that.last_ping_on = data.last_ping_on;
        var tempvar = new Date(data.lastStoppedAt);
        if (data.lastStoppedAt != null) {
            var fd = tempvar.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            that.lastStoppedAt = rhours + ':' + rminutes;
        }
        else {
            that.lastStoppedAt = '00' + ':' + '00';
        }
        that.distFromLastStop = data.distFromLastStop;
        if (!isNaN(data.timeAtLastStop)) {
            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
        }
        else {
            that.timeAtLastStop = '00:00:00';
        }
        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
        that.last_ACC = data.last_ACC;
        that.acModel = data.ac;
        that.currentFuel = data.currentFuel;
        that.power = data.power;
        that.gpsTracking = data.gpsTracking;
        that.recenterMeLat = data.last_location.lat;
        that.recenterMeLng = data.last_location.long;
    };
    LiveSingleDevice.prototype.ioReceiveLocation = function (pdata) {
        var that = this;
        if (that.fraction >= 1) {
            if (that.intevalId) {
                clearInterval(that.intevalId);
            }
            that.setPositionAnimate(0);
        }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            console.log("ioReceiveLocation ping: ", d4);
            var m = __WEBPACK_IMPORTED_MODULE_5_moment__();
            var ddate1 = m.milliseconds() + 1000 * (m.seconds() + 60 * (m.minutes() + 60 * m.hours()));
            console.log("date in miliseconds: ", ddate1);
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (data.last_location) {
                        that.latLngArray.push(data.last_location);
                    }
                    that.otherValues(data);
                    if (that.devices.indexOf(data._id) === -1) {
                        that.devices.push(data._id);
                        var indice = that.devices.indexOf(data._id);
                        that.createMarker(data, indice, that.getIconUrl(data));
                    }
                    else {
                        var indice = that.devices.indexOf(data._id);
                        that.setPositionAnimate(indice);
                    }
                })(d4);
        });
    };
    LiveSingleDevice.prototype.createMarker = function (location, indice, iconUrl) {
        var _this = this;
        var latlng = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](location.last_location.lat, location.last_location.long);
        var markerOptions = {
            title: 'Ionic',
            animation: 'BOUNCE',
            position: latlng,
            icon: iconUrl
        };
        this.allData.map.addMarker(markerOptions).then(function (marker) {
            _this.markersArray[indice] = marker;
            console.log('merkers array list: ', _this.markersArray);
        });
    };
    LiveSingleDevice.prototype.setPositionAnimate = function (indice) {
        var that = this;
        // if (that.latLngArray.length === 2) {
        if (that.latLngArray.length >= 2) {
            // let latlng = new LatLng(that.latLngArray[that.latLngArray.length - 1].lat, that.latLngArray[that.latLngArray.length - 1].long);
            // let latlng = new LatLng(that.latLngArray[0].lat, that.latLngArray[0].long);
            // that.markersArray[indice].setPosition(latlng);
            if (that.fraction >= 1) {
                console.log("when fraction 1: ", that.latLngArray.length);
                clearInterval(that.intevalId);
                if (that.latLngArray.length === 2) {
                    // this.speed = location.last_location.last_speed * 3.6;
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    this.allData.map.setCameraBearing(bearing);
                    var GOOGLE = london;
                    this.allData.map.setCameraTarget(GOOGLE);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true
                    });
                    that.fraction = 0;
                }
                else if (that.latLngArray.length > 2) {
                    // that.latLngArray.shift();
                    // this.speed = location.last_location.last_speed * 3.6;
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    this.allData.map.setCameraBearing(bearing);
                    var GOOGLE = london;
                    this.allData.map.setCameraTarget(GOOGLE);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    console.log("After shift from 1: ", that.latLngArray.length);
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true
                    });
                    that.fraction = 0;
                }
            }
            else if (that.fraction === 0) {
                console.log("when fraction 0: ", that.latLngArray.length);
                clearInterval(that.intevalId);
                if (that.latLngArray.length === 2) {
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    this.allData.map.setCameraBearing(bearing);
                    var GOOGLE = london;
                    this.allData.map.setCameraTarget(GOOGLE);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true
                    });
                    console.log("After shift from 0: ", that.latLngArray.length);
                }
                else if (that.latLngArray.length > 2) {
                    var nyc = { "lat": that.latLngArray[0].lat, "lng": that.latLngArray[0].long };
                    var london = { "lat": that.latLngArray[1].lat, "lng": that.latLngArray[1].long };
                    var bearing = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(nyc, london);
                    this.allData.map.setCameraBearing(bearing);
                    var GOOGLE = london;
                    this.allData.map.setCameraTarget(GOOGLE);
                    var points = [nyc, london];
                    that.latLngArray.shift();
                    this.allData.map.addPolyline({
                        "points": points,
                        "geodesic": true
                    });
                }
            }
            if (that.fraction <= 0) {
                that.intevalId = setInterval(function () {
                    if (that.fraction <= 1) {
                        that.fraction += 0.01 * that.direction;
                    }
                    that.markersArray[indice].setPosition(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].interpolate(nyc, london, that.fraction));
                }, 180);
            }
        }
        else {
            return;
        }
    };
    /////////// Anjali code ends ///////
    LiveSingleDevice.prototype.liveTrack = function (map, mark, coords, speed, delay, center, id, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                // debugger
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(lat, lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LiveSingleDevice.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    LiveSingleDevice.prototype.zoomout = function () {
        var that = this;
        that.allData.map.moveCameraZoomOut();
    };
    LiveSingleDevice.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.onClickShow = false;
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that.allData = {};
        that.socketChnl = [];
        that.socketSwitch = {};
        var style = [];
        if (localStorage.getItem('NightMode') != null) {
            if (localStorage.getItem('NightMode') === 'ON') {
                //Change Style to night between 7pm to 5am
                if (this.isNight()) {
                    style = __WEBPACK_IMPORTED_MODULE_14__live_map_style_model__["a" /* mapStyle */];
                }
            }
        }
        if (data) {
            if (data.last_location) {
                if (that.plt.is('android')) {
                    var mapOptions = {
                        backgroundColor: 'white',
                        controls: {
                            compass: true,
                            zoom: false,
                            myLocation: true,
                        },
                        gestures: {
                            rotate: false,
                            tilt: true
                        },
                        mapType: that.mapKey,
                        styles: style
                    };
                    var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                    // Environment.setBackgroundColor("black");
                    map.animateCamera({
                        target: { lat: 20.5937, lng: 78.9629 },
                        zoom: this.zoomLevel,
                        duration: 3000,
                        padding: 0 // default = 20px
                    });
                    // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    map.moveCamera({
                        target: {
                            lat: data.last_location['lat'], lng: data.last_location['long']
                        }
                    });
                    that.allData.map = map;
                    // that.onButtonClick();
                }
                else {
                    if (that.plt.is('ios')) {
                        var mapOptions = {
                            backgroundColor: 'white',
                            controls: {
                                compass: true,
                                zoom: false,
                                myLocation: true,
                            },
                            gestures: {
                                rotate: false,
                                tilt: true
                            },
                            camera: {
                                target: { lat: 20.5937, lng: 78.9629 },
                                zoom: this.zoomLevel,
                            },
                            mapType: that.mapKey,
                            styles: style
                        };
                        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                        // Environment.setBackgroundColor("black");
                        that.allData.map = map;
                        // that.onButtonClick();
                    }
                }
                // that.ioReceiveLocation(data);
                that.socketInit(data);
            }
            else {
                that.allData.map = that.newMap();
                that.socketInit(data);
            }
        }
    };
    LiveSingleDevice.prototype.showTrail = function (lat, lng, map) {
        var that = this;
        that.tempArray.push({ lat: lat, lng: lng });
        if (that.tempArray.length === 2) {
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.polylineID.push(polyline);
            });
        }
        else if (that.tempArray.length > 2) {
            that.tempArray.shift();
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.allData.polylineID.push(polyline);
            });
        }
    };
    LiveSingleDevice.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LiveSingleDevice.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                this.mapHideTraffic = !this.mapHideTraffic;
                if (this.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'mapTrail') {
                    this.mapTrail = !this.mapTrail;
                }
            }
        }
    };
    LiveSingleDevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\live-single-device\live-single-device.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start *ngIf="shwBckBtn">\n\n      <button ion-button small (click)="goBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>{{ "back" | translate }}\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{ "Live For " | translate }}{{ titleText }}</ion-title>\n\n    <ion-buttons end>\n\n      <button *ngIf="allData.tripStat != \'Started\'" ion-button icon-only (click)="createTrip()">\n\n        <ion-icon name="play"></ion-icon>\n\n      </button>\n\n      <button *ngIf="allData.tripStat == \'Started\'" ion-button icon-only (click)="endTrip()">\n\n        <ion-icon name="radio-button-on"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div id="map_canvas_single_device">\n\n  \n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-fab top left>\n\n          <button ion-fab color="light" mini (click)="onClickMainMenu()">\n\n            <ion-icon color="gpsc" name="map"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button style="margin: 0 2px;" ion-fab mini (click)="onClickMap(\'HYBRID\')" color="gpsc">\n\n              S\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n              T\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n              N\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n      <ion-col>\n\n        <p class="blink" style="text-align: center;font-size:24px;color:cornflowerblue;font-weight: 600;"\n\n          *ngIf="!vehicle_speed">\n\n          0\n\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n\n          *ngIf="vehicle_speed <= 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n\n          *ngIf="vehicle_speed > 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">{{ "Km/hr" | translate }}</span>\n\n        </p>\n\n      </ion-col>\n\n      <ion-col>\n\n        <ion-fab top right>\n\n          <button ion-fab color="gpsc" mini>\n\n            <ion-icon name="arrow-dropdown"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab *ngIf="showShareBtn"\n\n              (click)="shareLive($event)">\n\n              <ion-icon name="share"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapTrail\')">\n\n              <ion-icon name="eye"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n\n              <img src="assets/icon/trafficON.png" *ngIf="mapHideTraffic" />\n\n              <img src="assets/icon/trafficOFF.png" *ngIf="!mapHideTraffic" />\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="settings()">\n\n              <ion-icon name="cog"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="addPOI()">\n\n              <ion-icon name="flag"></ion-icon>\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-fab right style="margin-top:59%">\n\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n\n        <ion-icon name="locate"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:72%">\n\n      <button color="gpsc" ion-fab (click)="zoomin()" mini>\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:85%">\n\n      <button color="gpsc" ion-fab (click)="zoomout()" mini>\n\n        <ion-icon name="remove"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:46%">\n\n      <button ion-fab color="gpsc" mini (click)="navigateFromCurrentLoc()">\n\n        <ion-icon name="navigate"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab left style="margin-top:59%">\n\n      <button ion-fab color="gpsc" mini (click)="showNearby(\'police\')">\n\n        <ion-icon name="custom-station"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:72%">\n\n      <button ion-fab color="gpsc" mini (click)="showNearby(\'petrol\')">\n\n        <ion-icon name="custom-fuel"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:85%">\n\n      <button ion-fab color="gpsc" mini (click)="refreshMe1()">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-row *ngIf="expectation.distance"\n\n      style="margin-top:72%; background-color: rgb(0, 0, 0, 0.5); font-size: 0.8em; color: white;border-radius: 25px;width: 70%;margin: auto; padding:5px;">\n\n      <ion-col style="background-color: transparent; text-align: center;" col-6>\n\n        {{ "Distance" | translate }} {{ expectation.distance }}\n\n      </ion-col>\n\n      <ion-col style="background-color: transparent; text-align: center;" col-6>\n\n        {{ "Time" | translate }} {{ expectation.duration }}\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" [transition]="transition" (click)="setDocHeight()">\n\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n\n        {{ "Last Updated On" | translate }} &mdash;\n\n        {{ last_ping_on | date: "medium" }}\n\n      </p>\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n\n          <p>{{ "IGN" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n\n          <p>{{ "AC" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n\n          <p>{{ "FUEL" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n\n          <p>{{ "POWER" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="30" height="20" />\n\n          <p>{{ "GPS" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!total_odo">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="total_odo">\n\n            {{ total_odo | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Odometer" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!vehicle_speed">\n\n            0 {{ "Km/hr" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="vehicle_speed">\n\n            {{ vehicle_speed }} {{ "Km/hr" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">\n\n            {{ "Speed" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="fuel">\n\n            {{ fuel }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">{{ "Fuel" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!distFromLastStop">\n\n            0 {{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="distFromLastStop">\n\n            {{ distFromLastStop | number: "1.0-2" }} {{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!todays_odo">\n\n            0 {{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="todays_odo">\n\n            {{ todays_odo | number: "1.0-2" }} {{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!timeAtLastStop">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="timeAtLastStop">\n\n            {{ timeAtLastStop }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "At Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!today_stopped">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="today_stopped">\n\n            {{ today_stopped }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!lastStoppedAt">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="lastStoppedAt">\n\n            {{ lastStoppedAt }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!today_running">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="today_running">\n\n            {{ today_running }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>\n\n\n\n<div *ngIf="showFooter">\n\n  <ion-bottom-drawer [(state)]="drawerState1" [dockedHeight]="dockedHeight1" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop1" [minimumHeight]="minimumHeight1">\n\n    <div class="drawer-content">\n\n      <p style="font-size:1.2em; color:black;">\n\n        {{ "Share Live Vehicle" | translate }}\n\n      </p>\n\n      <ion-row>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{ condition }}" (click)="sharedevices(\'15mins\')">\n\n            {{ "15 mins" | translate }}\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{ condition1 }}" (click)="sharedevices(\'1hour\')">\n\n            {{ "1 hour" | translate }}\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{ condition2 }}" (click)="sharedevices(\'8hours\')">\n\n            {{ "8 hours" | translate }}\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-4></ion-col>\n\n        <ion-col col-4></ion-col>\n\n        <ion-col col-4 style="text-align: right;">\n\n          <ion-fab style="right: calc(10px + env(safe-area-inset-right));">\n\n            <button ion-fab mini (click)="shareLivetemp()" color="gpsc">\n\n              <ion-icon name="send" color="black"></ion-icon>\n\n            </button>\n\n          </ion-fab>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\live-single-device\live-single-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_11__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], LiveSingleDevice);
    return LiveSingleDevice;
}());

var PoiPage = /** @class */ (function () {
    function PoiPage(apicall, toastCtrl, navparam, viewCtrl) {
        this.apicall = apicall;
        this.toastCtrl = toastCtrl;
        this.navparam = navparam;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = this.navparam.get("param1");
        this.lat = this.params.mark.getPosition().lat;
        this.lng = this.params.mark.getPosition().lng;
    }
    PoiPage.prototype.ngOnInit = function () {
        var that = this;
        that.address = undefined;
        var geocoder = new google.maps.Geocoder;
        var latlng = new google.maps.LatLng(this.lat, this.lng);
        var request = { "latLng": latlng };
        geocoder.geocode(request, function (resp, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (resp[0] != null) {
                    that.address = resp[0].formatted_address;
                }
                else {
                    console.log("No address available");
                }
            }
            else {
                that.address = 'N/A';
            }
        });
    };
    PoiPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PoiPage.prototype.save = function () {
        var _this = this;
        if (this.poi_name == undefined || this.address == undefined) {
            var toast = this.toastCtrl.create({
                message: "POI name is required!",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
        }
        else {
            var payload = {
                "poi": [{
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                this.lng,
                                this.lat
                            ]
                        },
                        "poiname": this.poi_name,
                        "status": "Active",
                        "address": this.address,
                        "user": this.islogin._id
                    }]
            };
            this.apicall.startLoading().present();
            this.apicall.addPOIAPI(payload)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "POI added successfully!",
                    duration: 2000,
                    position: 'top'
                });
                toast.present();
                _this.viewCtrl.dismiss();
            }, function (err) {
                _this.apicall.stopLoading();
            });
        }
    };
    PoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\live-single-device\poi.html"*/'<!-- <ion-content padding> -->\n\n<div>\n\n    <ion-row>\n\n        <ion-col col-12 style="text-align: center; font-size: 2rem;">\n\n            <b>Add POI</b>&nbsp;&nbsp;&nbsp;\n\n            <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </ion-col>\n\n        <!-- <ion-col col-2 style="text-align: right">\n\n                <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n            </ion-col> -->\n\n    </ion-row><br />\n\n    <!-- <p style="text-align: center; font-size: 2rem;">\n\n            <b>Add POI&nbsp;&nbsp;<ion-icon class="close-button" id="close-button" name="close-circle"\n\n                (tap)="dismiss()"></ion-icon></b>\n\n        </p><br /> -->\n\n    <ion-row>\n\n        <ion-col col-12><b>POI Name:</b></ion-col>\n\n        <ion-col col-12>\n\n            <ion-input type="text" [(ngModel)]="poi_name"></ion-input>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-12><b>POI Address:</b></ion-col>\n\n        <ion-col col-12>\n\n            <ion-input type="text" [(ngModel)]="address"></ion-input>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-6>\n\n            <button ion-button block (tap)="save()" color="gpsc">SAVE</button>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n            <button ion-button block (tap)="dismiss()" color="gpsc">CANCEL</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</div>\n\n<!-- </ion-content> -->'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\live-single-device\poi.html"*/,
            selector: 'page-device-settings'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PoiPage);
    return PoiPage;
}());

//# sourceMappingURL=live-single-device.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AcReportPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ACDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AcReportPage = /** @class */ (function () {
    function AcReportPage(navCtrl, navParams, apiCAll) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCAll = apiCAll;
        this.portstemp = [];
        this.reportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    AcReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AcReportPage');
    };
    AcReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    AcReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCAll.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCAll.startLoading().present();
        this.apiCAll.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCAll.stopLoading();
            console.log(err);
        });
    };
    AcReportPage.prototype.getAcReportData = function () {
        var _this = this;
        var that = this;
        this.reportData = [];
        if (that.dev_id == undefined) {
            that.dev_id = "";
        }
        this.apiCAll.startLoading().present();
        ;
        this.apiCAll.getACReportAPI(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            console.log("ac report data: ", data);
            _this.reportData = data;
            // console.log("1stAction: ", data[0]['1stAction'])
        });
    };
    AcReportPage.prototype.getAcReporID = function (key) {
        console.log("key: ", key.Device_ID);
        this.dev_id = key.Device_ID;
    };
    AcReportPage.prototype.showDetail = function (pdata) {
        console.log("pdata: ", pdata);
        var that = this;
        this.navCtrl.push(ACDetailPage, {
            'param': pdata,
            'fdate': that.datetimeStart,
            'tdate': that.datetimeEnd,
            'uid': that.islogin._id
        });
    };
    AcReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ac-report',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\ac-report\ac-report.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "AC Report" | translate }}</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getAcReporID(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getAcReportData();">\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list *ngIf="reportData.length === 0">\n\n    <ion-item>\n\n      <p>Oops.. No record found..!</p>\n\n    </ion-item>\n\n  </ion-list>\n\n  <div *ngIf="reportData.length > 0">\n\n    <ion-card *ngFor="let rep of reportData;" (click)="showDetail(rep)">\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n        </ion-avatar>\n\n        <ion-label>{{rep.VehicleName}}</ion-label>\n\n      </ion-item>\n\n\n\n      <ion-card-content>\n\n        <ion-row style="padding-top:5px;">\n\n          <ion-col col-6>{{rep[\'1stAction\']}}</ion-col>\n\n          <ion-col col-6>{{rep[\'1stTime\']}}</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">ON Time(Min.)</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6>{{rep[\'2stAction\']}}</ion-col>\n\n          <ion-col col-6>{{rep[\'2stTime\']}}</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">OFF Time(Min.)</ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\ac-report\ac-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AcReportPage);
    return AcReportPage;
}());

var ACDetailPage = /** @class */ (function () {
    function ACDetailPage(apiCall, navParam) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.paramData = {};
        this.ddata = [];
        console.log("param parameters: ", this.navParam.get('param'));
        this.paramData = this.navParam.get('param');
        this.fdate = this.navParam.get('fdate');
        this.tdate = this.navParam.get('tdate');
        this.uid = this.navParam.get('uid');
    }
    ACDetailPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ACDetailPage.prototype.getDetails = function () {
        var _this = this;
        debugger;
        this.apiCall.startLoading().present();
        this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("detailed ac report data: ", data[0].s);
            _this.ddata = data[0].s;
        });
    };
    ACDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\ac-report\ac-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{ "Details for" | translate }} {{paramData.VehicleName}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-card *ngFor="let d of ddata;">\n\n        <ion-card-content>\n\n            <ion-row>\n\n                <ion-col col-6 style="font-size:13px;text-align:justify;">{{d.switch}}</ion-col>\n\n                <ion-col col-6 style="font-size:13px;text-align:justify;">\n\n                    {{d.timestamp | date:\'medium\'}}</ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ "ON/OFF" | translate }}</ion-col>\n\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">{{ "Time" | translate }}</ion-col>\n\n            </ion-row>\n\n        </ion-card-content>\n\n    </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\ac-report\ac-detail.html"*/,
            styles: ["\n  .col {\n    padding: 0px;\n}"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ACDetailPage);
    return ACDetailPage;
}());

//# sourceMappingURL=ac-report.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UploadDocPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UploadDocPage = /** @class */ (function () {
    function UploadDocPage(navCtrl, navParams, viewCtrl, platform, toastCtrl, loadingCtrl, apiCall, modalCtrl, popoverCtrl, event) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.event = event;
        this._docTypeList = [];
        this.imgData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = this.navParams.get("vehData");
        this.event.subscribe("reloaddoclist", function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
    }
    UploadDocPage.prototype.ngOnInit = function () {
        this.checkDevice();
    };
    UploadDocPage.prototype._addDocument = function () {
        this.navCtrl.push("AddDocPage", { vehData: this.vehData });
    };
    UploadDocPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UploadDocPage');
    };
    UploadDocPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadDocPage.prototype.onSelectChange = function (selectedValue) {
        console.log('Selected', selectedValue);
        this.docType = selectedValue;
    };
    UploadDocPage.prototype.brows = function () {
        this.docImge = true;
    };
    UploadDocPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    };
    UploadDocPage.prototype.viewDocFunc = function (imgData) {
        // let that = this;
        console.log("imgData: ", imgData);
        if (imgData == undefined) {
            this.showToast('Data not found');
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__["a" /* ViewDoc */], {
                param1: imgData
            });
            modal.present();
        }
    };
    UploadDocPage.prototype.checkDevice = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getsingledevice(this.vehData.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.imageDoc.length > 0) {
                // console.log("imagedoc data: " + JSON.stringify(data.imageDoc))
                for (var i = 0; i < data.imageDoc.length; i++) {
                    _this._docTypeList.push({
                        docname: data.imageDoc[i].doctype,
                        expDate: data.imageDoc[i].docdate ? data.imageDoc[i].docdate : 'N/A',
                        imageURL: data.imageDoc[i].image
                    });
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UploadDocPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var that = this;
        var popover = this.popoverCtrl.create(DocPopoverPage, {
            vehData: that.vehData,
            imgData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
        popover.present({
            ev: ev
        });
    };
    UploadDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload-doc',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\add-devices\upload-doc\upload-doc.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ "Documents" | translate }}</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="_addDocument()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list *ngIf="_docTypeList.length > 0">\n\n    <ion-item *ngFor="let doc of _docTypeList" (click)="viewDocFunc(doc)">\n\n      <ion-avatar item-start>\n\n        <img *ngIf="doc.docname == \'RC\'" src="assets/imgs/rc.png">\n\n        <img *ngIf="doc.docname == \'PUC\'" src="assets/imgs/puc.png">\n\n        <img *ngIf="doc.docname == \'Insurance\'" src="assets/imgs/insurence.png">\n\n        <img *ngIf="doc.docname == \'Licence\'" src="assets/imgs/dl.png">\n\n        <img *ngIf="doc.docname == \'Fitment\'" src="assets/imgs/rc.png">\n\n        <img *ngIf="doc.docname == \'Permit\'" src="assets/imgs/rc.png">\n\n      </ion-avatar>\n\n      <h1>{{doc.docname}}</h1>\n\n      <p>{{ "Expire On*" | translate }} &nbsp;\n\n        <span *ngIf="doc.expDate != \'N/A\'">{{doc.expDate | date:\'mediumDate\'}}</span>\n\n        <span *ngIf="doc.expDate == \'N/A\'">{{doc.expDate}}</span>\n\n      </p>\n\n      <button ion-button item-end small icon-only color="gpsc" (click)="presentPopover($event, doc); $event.stopPropagation()">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-card *ngIf="_docTypeList.length == 0">\n\n    <ion-card-content>\n\n      {{ " No doucments uploaded yet... Upload documents of your vehicle by pressing above + button" | translate }}\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\add-devices\upload-doc\upload-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], UploadDocPage);
    return UploadDocPage;
}());

var DocPopoverPage = /** @class */ (function () {
    function DocPopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, socialSharing) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.socialSharing = socialSharing;
        this.vehData = {};
        this.imgData = {};
        this.vehData = navParams.get("vehData");
        this.imgData = navParams.get("imgData");
        console.log("popover data=> ", this.imgData);
        var str = this.imgData.imageURL;
        var str1 = str.split('public/');
        this.link = "https://www.oneqlik.in/" + str1[1];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    DocPopoverPage.prototype.ngOnInit = function () { };
    DocPopoverPage.prototype.editItem = function () {
        console.log("edit");
    };
    DocPopoverPage.prototype.deleteDoc = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this Document?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    DocPopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        var that = this;
        var payload = {
            devId: d_id,
            doctype: that.imgData.docname
        };
        this.apiCall.startLoading().present();
        var baseUrl = "https://www.oneqlik.in/devices/deleteDocuments";
        this.apiCall.urlpasseswithdata(baseUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Document deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    DocPopoverPage.prototype.shareItem = function () {
        this.socialSharing.share(this.islogin.fn + " " + this.islogin.ln + " has shared the document with you.", "OneQlik- Vehicle Document", this.link, "");
    };
    DocPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteDoc()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      \n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], DocPopoverPage);
    return DocPopoverPage;
}());

//# sourceMappingURL=upload-doc.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FasttagPayNow; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FastagPage = /** @class */ (function () {
    function FastagPage(navCtrl, navParams, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.buttonColor = '#fff'; //Default Color
        this.buttonColor1 = '#fff';
        this.mainCost = 200;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    FastagPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FastagPage');
    };
    FastagPage.prototype.calc = function () {
        this.totalPayable = this.mainCost * this.numFastag;
    };
    FastagPage.prototype.addEvent = function () {
        this.vehicleType = 'Truck';
        this.buttonColor1 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent1 = function () {
        this.vehicleType = 'Bus';
        this.buttonColor = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor1 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent2 = function () {
        this.vehicleType = 'Taxi';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor2 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent3 = function () {
        this.vehicleType = 'Car';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor4 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor3 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent4 = function () {
        this.vehicleType = 'Bike';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#fff';
        this.buttonColor4 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.addEvent5 = function () {
        this.vehicleType = 'Other';
        this.buttonColor4 = '#fff';
        this.buttonColor2 = '#fff';
        this.buttonColor1 = '#fff';
        this.buttonColor = '#fff';
        this.buttonColor3 = '#fff';
        this.buttonColor5 = '#FBCCD2'; //desired Color
    };
    FastagPage.prototype.fastagReq = function () {
        var _this = this;
        if (this.vehicleType === undefined || this.numFastag === undefined) {
            this.toastCtrl.create({
                message: 'Please select the vehicle type and add number of requests.',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        var url = this.apiCall.mainUrl + "fastTag/addRequest";
        var payload = {};
        // debugger
        if (this.islogin.Dealer_ID === undefined) {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.supAdmin,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        else {
            payload = {
                user: this.islogin._id,
                Dealer: this.islogin.Dealer_ID._id,
                supAdmin: this.islogin.supAdmin,
                date: new Date().toISOString(),
                quantity: this.numFastag,
                vehicle_type: this.vehicleType
            };
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('response data: ', respData);
            if (respData.message === 'saved succesfully') {
                _this.toastCtrl.create({
                    message: 'Fastag added successfully.',
                    duration: 2000,
                    position: 'bottom'
                }).present();
                _this.navCtrl.pop();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagPage.prototype.payNow = function () {
        this.navCtrl.push(FasttagPayNow, {
            param: {
                amount: this.totalPayable,
                fastagType: this.vehicleType,
                quantity: this.numFastag
            }
        });
    };
    FastagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\fastag-list\fastag\fastag.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Add Fastag</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-row>\n    <ion-col>Choose Vehicle Type</ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4 (click)="addEvent();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/Truck.png" />\n          </ion-col>\n          <ion-col col-12 text-center>TRUCK</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent1();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor1}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/bus.png" />\n          </ion-col>\n          <ion-col col-12 text-center>BUS</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent2();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor2}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/cab.png" />\n          </ion-col>\n          <ion-col col-12 text-center>TAXI</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4 (click)="addEvent3();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor3}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/car.png" />\n          </ion-col>\n          <ion-col col-12 text-center>CAR</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent4();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor4}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/motorcycle.png" />\n          </ion-col>\n          <ion-col col-12 text-center>BIKE</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n    <ion-col col-4 (click)="addEvent5();">\n      <ion-card [ngStyle]="{\'background-color\': buttonColor5}">\n        <ion-row>\n          <ion-col col-12>\n            <img style="width: 50px; margin: auto;" src="assets/imgs/fastag/Truck.png" />\n          </ion-col>\n          <ion-col col-12 text-center>OTHERS</ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf="vehicleType">\n    <ion-col col-6>\n      <ion-list>\n        <ion-item>\n          <ion-label floating>Fastag Quantity</ion-label>\n          <ion-input type="number" [(ngModel)]="numFastag"></ion-input>\n        </ion-item>\n      </ion-list>\n    </ion-col>\n    <ion-col col-6><button ion-button *ngIf="numFastag" round color="primary" style="margin-top: 12%;"\n        (click)="calc()">Calculate Cost</button></ion-col>\n  </ion-row>\n  <!-- <ion-list *ngIf="vehicleType">\n    <ion-item>\n      <ion-label floating>Fastag Quantity</ion-label>\n      <ion-input type="number" [(ngModel)]="numFastag"></ion-input>\n    </ion-item>\n  </ion-list> -->\n  <ion-row *ngIf="totalPayable">\n    <ion-col col-12 style="border: 2px solid black;">\n      <p style="font-size: 1.3em;\n      text-align: center;\n      margin: 2px;">Total Payable Amount - {{totalPayable}}</p>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf="totalPayable">\n    <ion-col>\n\n      <button ion-button block color="gpsc" (click)="payNow()"><b>PAY NOW</b></button>\n      <button ion-button block color="primary" (click)="fastagReq()"><b>PAY LATTER</b></button>\n    </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\fastag-list\fastag\fastag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], FastagPage);
    return FastagPage;
}());

var FasttagPayNow = /** @class */ (function () {
    function FasttagPayNow(navParams) {
        this.navParams = navParams;
        this.razor_key = 'rzp_live_jB4onRx1BUUvxt';
        this.currency = 'INR';
        this.data = {};
        this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        this.data = this.navParams.get('param');
        this.paymentAmount = this.data.amount * 100;
        console.log(this.navParams.get('param'));
    }
    FasttagPayNow.prototype.payWithRazor = function () {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/GO0jiDP.jpg',
            currency: this.currency,
            key: this.razor_key,
            amount: this.paymentAmount,
            name: this.data.fastagType,
            prefill: {
                email: this.islogin.email,
                contact: this.islogin.phn,
                name: this.islogin.fn + ' ' + this.islogin.ln
            },
            theme: {
                color: '#d80622'
            },
            modal: {
                ondismiss: function () {
                    console.log('dismissed');
                }
            }
        };
        var successCallback = function (payment_id) {
            alert('payment_id: ' + payment_id);
        };
        var cancelCallback = function (error) {
            alert(error.description + ' (Error ' + error.code + ')');
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
    };
    FasttagPayNow = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\fastag-list\fastag\paynow.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{ "Payment Method" | translate }}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n    <ion-grid text-center>\n\n        <ion-row>\n\n            <ion-col>\n\n                ** Pay with razorpay **\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n    <ion-card class="welcome-card">\n\n        <img src="/assets/imgs/dc-Cover-u0b349upqugfio195s4lpk8144-20190213120303.Medi.jpeg">\n\n        <ion-card-header>\n\n            <!-- <ion-card-subtitle>Get Started</ion-card-subtitle> -->\n\n            <ion-card-title>{{"Fastag for vehicle type" | translate}} - {{data.fastagType}}</ion-card-title>\n\n            <!-- <p>{{"Total Quantity" | translate}} - {{data.quantity}}</p> -->\n\n            <ion-row>\n\n                <ion-col>\n\n                    {{"Total Quantity" | translate}}\n\n                </ion-col>\n\n                <ion-col>\n\n                    {{data.quantity}}\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col>\n\n                    {{"Total Payment Amount" | translate}}\n\n                </ion-col>\n\n                <ion-col>\n\n                    {{currencyIcon}}{{paymentAmount/100}}\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-card-header>\n\n        <ion-card-content>\n\n            <!-- <ion-button expand="full" color="success" (click)="payWithRazor()">Pay with RazorPay</ion-button> -->\n\n            <button ion-button full color="gpsc" (click)="payWithRazor()">Pay with RazorPay</button>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\fastag-list\fastag\paynow.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FasttagPayNow);
    return FasttagPayNow;
}());

//# sourceMappingURL=fastag.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FuelEntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FuelConsumptionPage = /** @class */ (function () {
    function FuelConsumptionPage(apiCall, navCtrl, navParams, toastCtrl, event) {
        var _this = this;
        this.apiCall = apiCall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.portstemp = [];
        this.fuelList = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_4_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_4_moment__().format();
        this.event.subscribe('reloadFuellist', function () {
            if (_this._vehId != undefined) {
                _this.getFuelList();
            }
        });
    }
    FuelConsumptionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FuelConsumptionPage');
    };
    FuelConsumptionPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionPage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    FuelConsumptionPage.prototype.addFuelEntry = function () {
        this.navCtrl.push(FuelEntryPage, {
            portstemp: this.portstemp
        });
    };
    FuelConsumptionPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionPage.prototype.getFuelList = function () {
        var _this = this;
        this.fuelList = [];
        if (this._vehId == undefined) {
            var toast = this.toastCtrl.create({
                message: 'Please select the vehicle first..',
                position: 'bottom',
                duration: 1500
            });
            toast.present();
        }
        else {
            var _baseURL = this.apiCall.mainUrl + "fuel/getFuels?vehicle=" + this._vehId + "&user=" + this.islogin._id;
            this.apiCall.startLoading().present();
            this.apiCall.getSOSReportAPI(_baseURL)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fule entry list: ", data);
                if (!data.message) {
                    _this.fuelList = data;
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "Report not found!",
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
            });
        }
    };
    FuelConsumptionPage.prototype.getid = function (veh) {
        this._vehId = veh._id;
    };
    FuelConsumptionPage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    FuelConsumptionPage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    FuelConsumptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\fuel\fuel-consumption\fuel-consumption.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "Fuel Entry" | translate }}</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">{{\n\n      "Select Vehicle" | translate\n\n    }}</ion-label>\n\n    <select-searchable\n\n      item-content\n\n      [(ngModel)]="selectedVehicle"\n\n      [items]="portstemp"\n\n      itemValueField="Device_Name"\n\n      itemTextField="Device_Name"\n\n      [canSearch]="true"\n\n      (onChange)="getid(selectedVehicle)"\n\n    >\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20 class="cust">\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n\n        <ion-datetime\n\n          displayFormat="DD-MM-YYYY hh:mm a"\n\n          pickerFormat="DD/MM/YY hh:mm a"\n\n          [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n\n        >\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20 class="cust">\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n\n        <ion-datetime\n\n          displayFormat="DD-MM-YYYY hh:mm a"\n\n          pickerFormat="DD/MM/YY hh:mm a"\n\n          [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n\n        >\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon\n\n          ios="ios-search"\n\n          md="md-search"\n\n          style="font-size:2.3em;"\n\n          (click)="getFuelList()"\n\n        >\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let f of fuelList">\n\n    <ion-card-content>\n\n      <ion-row>\n\n        <ion-col col-6 style="font-size: 1.2em;" no-padding>\n\n          <ion-row>\n\n            <ion-col col-2>\n\n              <ion-icon name="car" color="gpsc"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10>\n\n              {{ f.vehicle.Device_Name }}\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-6 style="text-align: right;" no-padding>\n\n          <ion-row>\n\n            <ion-col col-12>\n\n              <ion-badge color="gpsc" style="font-size: 1.2em;"\n\n                >{{ f.quantity }} {{\'liters\' | translate}}</ion-badge\n\n              >\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-6 no-padding>\n\n          <ion-row>\n\n            <ion-col col-2>\n\n              <ion-icon style="color:gray;" name="calendar"></ion-icon>\n\n            </ion-col>\n\n            <ion-col\n\n              col-10\n\n              style="color:gray;font-size:14px;font-weight: 400;"\n\n              >{{ f.Date | date: "mediumDate" }}</ion-col\n\n            >\n\n          </ion-row>\n\n        </ion-col>\n\n        <ion-col col-6 no-padding>\n\n          <ion-row style="text-align: right;">\n\n            <ion-col col-12 style="color:gray;font-size:14px;font-weight: 400;">\n\n              <ion-icon style="color:gray;" name="cash"></ion-icon\n\n              >&nbsp;&nbsp;{{ f.price }} INR\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n<ion-fab right bottom>\n\n  <button ion-fab color="gpsc" (click)="addFuelEntry()">\n\n    <ion-icon name="add"></ion-icon>\n\n  </button>\n\n</ion-fab>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\fuel\fuel-consumption\fuel-consumption.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], FuelConsumptionPage);
    return FuelConsumptionPage;
}());

var FuelEntryPage = /** @class */ (function () {
    function FuelEntryPage(navParams, apiCall, toastCtrl, event) {
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.portstemp = [];
        this.fuelType = 'CNG';
        this.liter = 0;
        this.amt = 0;
        this.ddate = new Date().toISOString();
        this.portstemp = this.navParams.get("portstemp");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
    }
    FuelEntryPage.prototype.onChnageEvent = function (veh) {
        console.log("vehicle info:", veh);
        debugger;
        var that = this;
        that.veh_id = veh._id;
        var sb = veh.total_odo;
        that.tot_odo = sb.toFixed(2);
    };
    FuelEntryPage.prototype.fixDecimals = function (value) {
        value = "" + value;
        value = value.trim();
        value = parseFloat(value).toFixed(2);
        return value;
    };
    FuelEntryPage.prototype.submit = function () {
        var _this = this;
        var that = this;
        debugger;
        if (this.liter == undefined || this.amt == undefined || this.tot_odo == undefined || this.fuelType == undefined || this.ddate == undefined || this.vehName == undefined) {
            var toast = this.toastCtrl.create({
                message: "Fields should not be empty.",
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }
        else {
            var payload = {};
            var _baseURL = this.apiCall.mainUrl + "fuel/addFuel";
            payload = {
                "vehicle": that.veh_id,
                "user": that.islogin._id,
                "date": new Date(that.ddate).toISOString(),
                "quantity": that.liter,
                "odometer": that.tot_odo,
                "price": that.amt,
                "fuel_type": that.fuelType,
                "comment": "First fill"
            };
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(_baseURL, payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fuel entry added: " + data);
                _this.liter = null;
                _this.amt = null;
                _this.tot_odo = null;
                _this.fuelType = null;
                _this.ddate = null;
                _this.vehName = null;
                _this.veh_id = null;
                var toast = _this.toastCtrl.create({
                    message: "Fuel entry added successfully! Add another entry..",
                    position: 'bottom',
                    duration: 2000
                });
                toast.present();
                _this.event.publish('reloadFuellist');
            });
        }
    };
    FuelEntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\fuel\fuel-consumption\fuel-entry.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Add Fuel Entry</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-item>\n\n        <ion-label>Select Vehicle</ion-label>\n\n        <ion-select [(ngModel)]="vehName">\n\n            <ion-option *ngFor="let veh of portstemp" [value]="veh.Device_Name" (ionSelect)="onChnageEvent(veh)">\n\n                {{veh.Device_Name}}</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n    <ion-item *ngIf="tot_odo != undefined">\n\n        <ion-label>Total Odo</ion-label>\n\n        <ion-input type="number" [(ngModel)]="tot_odo" readonly></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label>Fuel Type</ion-label>\n\n        <ion-select [(ngModel)]="fuelType">\n\n            <ion-option [value]="CNG" selected>CNG</ion-option>\n\n            <ion-option [value]="Petrol">PETROL</ion-option>\n\n            <ion-option [value]="Diesel">DIESEL</ion-option>\n\n            <ion-option [value]="Other">OTHER</ion-option>\n\n        </ion-select>\n\n    </ion-item>\n\n\n\n    <ion-item>\n\n        <ion-label>Liter(s)</ion-label>\n\n        <ion-input type="number" [(ngModel)]="liter"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label>Amount</ion-label>\n\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label>Date</ion-label>\n\n        <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="ddate" name="ddate">\n\n        </ion-datetime>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row no-padding>\n\n            <ion-col width-50 style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="submit()">SUBMIT</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\fuel\fuel-consumption\fuel-entry.html"*/,
            selector: 'page-fuel-consumption'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], FuelEntryPage);
    return FuelEntryPage;
}());

//# sourceMappingURL=fuel-consumption.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotifModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotifModalPage = /** @class */ (function () {
    function NotifModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.isAddEmail = false;
        this.isAddPhone = false;
        this.emailList = [];
        this.phonelist = [];
        this.femail = '';
        this.fphone = 0;
        this.model = {};
        this.fData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.notifType = navParams.get('notifData').notifType;
        this.data = navParams.get('notifData').compData;
        if (navParams.get('notifData').buttonClick == 'email') {
            this.isAddEmail = true;
        }
        if (navParams.get('notifData').buttonClick == 'phone') {
            this.isAddPhone = true;
        }
    }
    NotifModalPage.prototype.ngOnInit = function () {
        this.emailList = this.data[this.notifType].emails;
        this.phonelist = this.data[this.notifType].phones;
    };
    NotifModalPage.prototype.onSubmit = function () {
        console.log('Form submitted!!');
    };
    NotifModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    NotifModalPage.prototype.addEmail = function (id, index) {
        var _this = this;
        if (id == 'add') {
            if (!this.data[this.notifType].emails) {
                this.data[this.notifType].emails = [];
            }
            this.data[this.notifType].emails.push(this.model.email);
            console.log(this.data);
            this.fData.contactid = this.islogin._id;
            this.fData.alert = this.data;
            console.log("fData object =>", this.fData);
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.email = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Email Id added!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        if (id == 'delete') {
            var splicedmail = this.data[this.notifType].emails[index];
            console.log("spliceNumber =>", splicedmail);
            this.data[this.notifType].emails.splice(index, 1);
            this.fData.contactid = this.islogin._id;
            this.fData.alert = this.data;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                _this.model.email = null;
                console.log("Please check: ", respData);
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Email Id deleted!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
    };
    NotifModalPage.prototype.addPhone = function (id, index) {
        var _this = this;
        if (id == 'add') {
            if (!this.data[this.notifType].phones) {
                this.data[this.notifType].phones = [];
            }
            this.data[this.notifType].phones.push(this.model.phone);
            // console.log("in addd", this.fphone);
            this.fData.contactid = this.islogin._id;
            console.log("This.data=>", this.data);
            this.fData.alert = this.data;
            console.log("finalData=>", this.fData);
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.phone = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Phone number added!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
        if (id == 'delete') {
            var splicedNum = this.data[this.notifType].phones[index];
            console.log("spliceNumber =>", splicedNum);
            this.data[this.notifType].phones.splice(index, 1);
            this.fData.contactid = this.islogin._id;
            this.fData.alert = this.data;
            var url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(function (respData) {
                _this.apiCall.stopLoading();
                console.log("Please check: ", respData);
                _this.model.phone = null;
                if (respData) {
                    _this.toastCtrl.create({
                        message: 'Phone number deleted!!',
                        duration: 1000,
                        position: 'middle'
                    }).present();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                _this.toastCtrl.create({
                    message: 'Internal server error, please try again!!!',
                    duration: 1500,
                    position: 'bottom'
                }).present();
            });
        }
    };
    NotifModalPage.prototype.Close = function () {
        // this.dialogRef.close();
    };
    NotifModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-notif-modal',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\profile\settings\notif-setting\notif-modal.html"*/'<div class="mainDiv">\n\n    <div class="secondDiv">\n\n        <form novalidate #f="ngForm" *ngIf="isAddEmail">\n\n            <ion-list>\n\n                <ion-item>\n\n                    <ion-label floating>Add Email</ion-label>\n\n                    <ion-input type="email" name="femail" [(ngModel)]="model.email" required pattern="[^ @]*@[^ @]*"\n\n                        #femail="ngModel"></ion-input>\n\n                </ion-item>\n\n                <div style="padding: 0px 16px 0px; color: red"\n\n                    *ngIf="femail.errors && (femail.dirty || femail.touched)">\n\n                    <p *ngIf="femail.errors.required">Email is required</p>\n\n                    <p *ngIf="femail.errors.pattern">Email must contain at least the @ character</p>\n\n                </div>\n\n                <!-- <pre>Valid? {{f.form.controls.email?.valid}}</pre>\n\n                <pre>Dirty? {{f.form.controls.email?.dirty}}</pre>\n\n                <pre>Touched? {{f.form.controls.email?.touched}}</pre> -->\n\n            </ion-list>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="addEmail(\'add\',null)" color="gpsc" round\n\n                            small style="width: 100px;" [disabled]="f.invalid">Add</button></div>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="grey" round small\n\n                            style="width: 100px;">Close</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </form>\n\n        <form novalidate #f1="ngForm" *ngIf="isAddPhone">\n\n            <ion-list>\n\n                <ion-item>\n\n                    <ion-label floating>Add Phone Number</ion-label>\n\n                    <ion-input type="tel" name="fphone" [(ngModel)]="model.phone" required minlength="10" maxlength="10"\n\n                        #fphone="ngModel" pattern="[0-9]*"></ion-input>\n\n                </ion-item>\n\n                <div style="padding: 0px 16px 0px; color: red"\n\n                    *ngIf="fphone.errors && (fphone.dirty || fphone.touched)">\n\n                    <p *ngIf="fphone.errors.required">Phone number is required</p>\n\n                    <p *ngIf="fphone.errors.pattern">Phone number must contain 10 digits.</p>\n\n                </div>\n\n            </ion-list>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="addPhone(\'add\',null)" color="gpsc" round\n\n                            small style="width: 100px;" [disabled]="f1.invalid">Add</button></div>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="grey" round small\n\n                            style="width: 100px;">Close</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </form>\n\n        <ion-list *ngIf="isAddEmail">\n\n            <ion-item *ngFor=\'let email of emailList; let i = index\'>\n\n                <ion-grid item-content>\n\n                    <ion-row>\n\n                        <ion-col col-2>\n\n                            <ion-checkbox></ion-checkbox>\n\n                        </ion-col>\n\n                        <ion-col col-8>\n\n                            <p style="margin-top: 8px;">{{email}}</p>\n\n                        </ion-col>\n\n                        <ion-col col-2 style="margin: auto;">\n\n                            <ion-icon style="font-size: 1em;" color="danger" name="trash"\n\n                                (click)="addEmail(\'delete\',i)"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-item>\n\n        </ion-list>\n\n        <ion-list *ngIf="isAddPhone">\n\n            <ion-item *ngFor=\'let phone of phonelist; let i = index\'>\n\n                <ion-grid item-content>\n\n                    <ion-row>\n\n                        <ion-col col-2>\n\n                            <ion-checkbox></ion-checkbox>\n\n                        </ion-col>\n\n                        <ion-col col-8>\n\n                            <p style="margin-top: 8px;">{{phone}}</p>\n\n                        </ion-col>\n\n                        <ion-col col-2 style="margin: auto;">\n\n                            <ion-icon style="font-size: 1em;" color="danger" name="trash"\n\n                                (click)="addPhone(\'delete\',i)"></ion-icon>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-grid>\n\n            </ion-item>\n\n        </ion-list>\n\n        <!-- <pre>{{f.value | json }}</pre> -->\n\n    </div>\n\n</div>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\profile\settings\notif-setting\notif-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], NotifModalPage);
    return NotifModalPage;
}());

//# sourceMappingURL=notif-modal.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLanguages; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_model__ = __webpack_require__(470);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppLanguages = /** @class */ (function () {
    function AppLanguages(translate) {
        this.translate = translate;
        this._lang = [
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('English'), 'en'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Hindi'), 'hi'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Marathi'), 'mr'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Bangali'), 'bn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Tamil'), 'ta'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Telugu'), 'te'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Gujarati'), 'gu'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Kannada'), 'kn'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Malayalam'), 'ml'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Spanish'), 'sp'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Persian'), 'fa'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Arabic'), 'ar'),
            new __WEBPACK_IMPORTED_MODULE_2__app_model__["a" /* Language */](this.translate.instant('Nepali'), 'np'),
        ];
    }
    Object.defineProperty(AppLanguages.prototype, "Languages", {
        get: function () {
            return this._lang.slice();
        },
        enumerable: true,
        configurable: true
    });
    AppLanguages = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]])
    ], AppLanguages);
    return AppLanguages;
}());

//# sourceMappingURL=app-languages.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl, translate
        // private datePicker: DatePicker
    ) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: this.translate.instant('Overspeeding'), filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: this.translate.instant('Ignition'), filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: this.translate.instant('GEOFENCE'), filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: this.translate.instant('Stoppage'), filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: this.translate.instant('Fuel'), filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: this.translate.instant("AC"), filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: this.translate.instant('Power'), filterValue: 'power', checked: false },
            { filterID: 10, filterName: this.translate.instant('Immobilize'), filterValue: 'immo', checked: false },
            { filterID: 11, filterName: "Theft", filterValue: 'theft', checked: false },
            { filterID: 12, filterName: this.translate.instant('SOS'), filterValue: 'SOS', checked: false },
            { filterID: 13, filterName: 'Reminder', filterValue: 'Reminder', checked: false }
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
        // this.getNotifTypes()
    };
    FilterPage.prototype.getNotifTypes = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getVehicleListCall(this.apiCall.mainUrl + "notifs/getTypes")
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("notif type list: ", data);
            _this.filterList = data;
        });
    };
    FilterPage.prototype.applyFilter = function () {
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\all-notifications\filter\filter.html"*/'<ion-list>\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">{{ "Filter Type" | translate }}</ion-item-divider>\n\n        <div *ngIf="filterList.length > 0">\n\n            <ion-item *ngFor="let member of filterList">\n\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n\n            </ion-item>\n\n        </div>\n\n    </ion-item-group>\n\n</ion-list>\n\n<ion-row style="background-color: darkgray">\n\n    <ion-col width-50 style="text-align: center; background-color: #f53d3d;">\n\n        <button ion-button clear small color="light" (click)="cancel()">{{ "Cancel" | translate }}</button>\n\n    </ion-col>\n\n    <ion-col width-50 style="text-align: center; background-color: #11b700;">\n\n        <button ion-button clear small color="light" (click)="applyFilter()">{{ "Apply Filter" | translate }}</button>\n\n    </ion-col>\n\n</ion-row>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\all-notifications\filter\filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]
            // private datePicker: DatePicker
        ])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportSettingModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportSettingModal = /** @class */ (function () {
    function ReportSettingModal(viewCtrl, apiCall, toastCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.reportArray = [
            { name: "Daily Report", Rstatus: true, Astatus: false, value: "daily_report" },
            { name: "Daywise Report", Rstatus: true, Astatus: false, value: "daywise_report" },
            { name: "Speed Variation", Rstatus: true, Astatus: false, value: "speed_variation" },
            { name: "Fuel Report", Rstatus: true, Astatus: false, value: "fuel_report" },
            { name: "Idle Report", Rstatus: true, Astatus: false, value: "idle_report" },
            { name: "Fuel Consumption Report", Rstatus: true, Astatus: false, value: "fuel_consumption_report" },
            { name: "Trip Report", Rstatus: true, Astatus: false, value: "trip_report" },
            { name: "Travel Path Report", Rstatus: true, Astatus: false, value: "travel_path_report" },
            { name: "Summary Report", Rstatus: true, Astatus: false, value: "summary_report" },
            { name: "Geofence Report", Rstatus: true, Astatus: false, value: "geofence_report" },
            { name: "Overspeed Report", Rstatus: true, Astatus: false, value: "overspeed_report" },
            { name: "Route Violation Report", Rstatus: true, Astatus: false, value: "route_violation_report" },
            { name: "Stoppage Report", Rstatus: true, Astatus: false, value: "stoppage_report" },
            { name: "Ignition Report", Rstatus: true, Astatus: false, value: "ignition_report" },
            { name: "Distance Report", Rstatus: true, Astatus: false, value: "distance_report" },
            { name: "POI Report", Rstatus: true, Astatus: false, value: "poi_report" },
            { name: "SOS Report", Rstatus: true, Astatus: false, value: "sos_report" },
            { name: "AC Report", Rstatus: true, Astatus: false, value: "ac_report" },
            { name: "Driver Performance Report", Rstatus: true, Astatus: false, value: "driver_performance_report" },
            { name: "User Trip Report", Rstatus: true, Astatus: false, value: "user_trip_report" },
            { name: "Alert Report", Rstatus: true, Astatus: false, value: "alert_report" }
        ];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = navParams.get('param');
        console.log("navparams: ", this.params);
        this.getCostumerDetail();
    }
    ReportSettingModal.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ReportSettingModal.prototype.submit = function () {
        var _this = this;
        var report_preference_payload = [];
        report_preference_payload = JSON.parse(JSON.stringify(this.reportArray));
        var tempsemp = {};
        console.log(report_preference_payload);
        for (var index in report_preference_payload) {
            var k = report_preference_payload[index].value;
            if (report_preference_payload[index].Rstatus == false) {
                report_preference_payload[index].Astatus = false;
            }
            tempsemp[k] = report_preference_payload[index];
            delete tempsemp[k].name;
            delete tempsemp[k].value;
        }
        console.log(tempsemp);
        var reportPref = {
            reportsArr: tempsemp,
            id: this.params._id
        };
        var url = this.apiCall.mainUrl + "users/reportPrefrence";
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, reportPref).subscribe(function (res) {
            console.log("check status: ", res);
            _this.apiCall.stopLoading();
            _this.toastCtrl.create({
                message: 'Report preference updated',
                duration: 1000,
                position: 'top'
            }).present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error", err);
        });
    };
    ReportSettingModal.prototype.selectedStat = function (ind, ev, flag) {
        if (flag == 'rVisibility') {
            this.reportArray[ind].Rstatus = ev.checked;
        }
        // console.log('this.reportArray', this.reportArray);
    };
    ReportSettingModal.prototype.getCostumerDetail = function () {
        var that = this;
        this.apiCall.getcustToken(this.params._id).subscribe(function (res) {
            // console.log("resresresresresresresresres", res.cust.report_preference);
            var tempReport = res.cust.report_preference;
            var _loop_1 = function (dt) {
                var arr = that.reportArray.filter(function (d) {
                    return d.value == dt;
                });
                arr[0].Astatus = tempReport[dt].Astatus;
                arr[0].Rstatus = tempReport[dt].Rstatus;
            };
            for (var dt in tempReport) {
                _loop_1(dt);
            }
            // console.log("that.reportArray=>", that.reportArray);
        });
    };
    ReportSettingModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\customers\modals\report-setting\report-setting.html"*/'<ion-content style="background-color: rgba(0, 0, 0, 0.7);">\n\n    <div class="mainDiv">\n\n        <div class="secondDiv">\n\n            <ion-row>\n\n                <ion-col col-12 text-center>\n\n                    <h5 style="margin-top: -10px;">REPORT PREFERENCE</h5>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-list radio-group [(ngModel)]="min_time">\n\n                <ion-item *ngFor="let item of reportArray; let i = index">\n\n                    <ion-label>{{item.name}}</ion-label>\n\n                    <!-- <md-checkbox type="checkbox" [checked]="devStatus.Rstatus"\n\n                        (change)="selectedStatus(i,$event,\'rVisibility\')"></md-checkbox> -->\n\n                    <ion-checkbox item-end color="gpsc" [checked]="item.Rstatus"\n\n                        (ionChange)="selectedStat(i,$event,\'rVisibility\')"></ion-checkbox>\n\n                </ion-item>\n\n            </ion-list>\n\n            <ion-row>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="submit()" color="gpsc" block>Submit</button>\n\n                    </div>\n\n\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <div style="padding: 5px;"><button ion-button (click)="dismiss()" color="gpsc" block>Dismiss</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\customers\modals\report-setting\report-setting.html"*/,
            styles: ["\n    .mainDiv {\n        padding: 50px;\n        \n    }\n\n    .secondDiv {\n        padding-top: 20px;\n        border-radius: 18px;\n        border: 2px solid black;\n        background: white;\n    }"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"]])
    ], ReportSettingModal);
    return ReportSettingModal;
}());

//# sourceMappingURL=report-setting.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealerPermModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DealerPermModalPage = /** @class */ (function () {
    function DealerPermModalPage(navParams, viewCtrl, apiCall, toastCtrl) {
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.dev_permission = true;
        this.cust_permission = true;
        this.dealerData = navParams.get('param');
        console.log("dealer's data: ", this.dealerData);
        if (this.dealerData.device_add_permission != undefined && this.dealerData.cust_add_permission != undefined) {
            this.dev_permission = this.dealerData.device_add_permission;
            this.cust_permission = this.dealerData.cust_add_permission;
        }
    }
    DealerPermModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DealerPermModalPage.prototype.submit = function () {
        var _this = this;
        console.log("changed values: ", this.dev_permission);
        console.log("changed values: ", this.cust_permission);
        var url = this.apiCall.mainUrl + "users/setDealerPermission";
        var payload = {
            "device_add_permission": this.dev_permission,
            "cust_add_permission": this.cust_permission,
            "uid": this.dealerData._id
        };
        this.apiCall.urlpasseswithdata(url, payload).subscribe(function (respData) {
            console.log("response data check: ", respData);
            if (respData.message) {
                _this.toastCtrl.create({
                    message: respData.message,
                    duration: 1500,
                    position: 'middle'
                }).present();
                _this.dismiss();
            }
            else {
                _this.toastCtrl.create({
                    message: 'Something went wrong.. Please try after some time.',
                    duration: 1500,
                    position: 'middle'
                }).present();
                _this.dismiss();
            }
        });
    };
    DealerPermModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n    <div class=\"mainDiv\">\n        <div class=\"secondDiv\">\n        <div text-center>\n            <h5>Dealer Permission</h5>\n            </div>\n            <ion-list>\n                <ion-item>\n                    <ion-label>Add Vehicle</ion-label>\n                    <ion-toggle [(ngModel)]=\"dev_permission\"></ion-toggle>\n                </ion-item>\n                <ion-item>\n                    <ion-label>Add Customer</ion-label>\n                    <ion-toggle [(ngModel)]=\"cust_permission\"></ion-toggle>\n                </ion-item>\n            </ion-list>\n            <ion-row>\n                <ion-col col-6>\n                    <button ion-button (click)=\"submit()\" color=\"gpsc\" block>Submit</button>\n                </ion-col>\n                <ion-col col-6>\n                    <button ion-button (click)=\"dismiss()\" color=\"grey\" block>Dismiss</button>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DealerPermModalPage);
    return DealerPermModalPage;
}());

//# sourceMappingURL=dealer-perm.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PagerProvider = /** @class */ (function () {
    function PagerProvider(http) {
        this.http = http;
        console.log('Hello PagerProvider Provider');
    }
    PagerProvider.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        //calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);
        //ensure current page isn't out of range
        if (currentPage < 1) {
            currentPage = 1;
        }
        else if (currentPage > totalPages) {
            currentPage = totalPages;
        }
        var startPage, endPage;
        if (totalPages <= 10) {
            //less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        }
        else {
            //more that 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            }
            else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            }
            else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        // create an array of pages to ng-repeat in the pager control
        var pages = Array.from(Array((endPage + 1) - startPage).keys()).map(function (i) { return startPage + i; });
        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    };
    PagerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], PagerProvider);
    return PagerProvider;
}());

//# sourceMappingURL=pager.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { TextToSpeech } from '@ionic-native/text-to-speech';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams, events, formBuilder, storage, platform) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.platform = platform;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.footerState = __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.credentialsForm = this.formBuilder.group({
            fname: [this.islogin.fn],
            lname: [this.islogin.ln],
            email: [this.islogin.email],
            phonenum: [this.islogin.phn],
            org: [this.islogin._orgName],
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ProfilePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ProfilePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ProfilePage.prototype.settings = function () {
        this.navCtrl.push('SettingsPage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.onSignIn = function () {
        var _this = this;
        // this.logger.info('SignInPage: onSignIn()');
        console.log(this.credentialsForm.value);
        var data = {
            "fname": this.credentialsForm.value.fname,
            "lname": this.credentialsForm.value.lname,
            "org": this.credentialsForm.value.org,
            "noti": true,
            "uid": this.islogin._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.updateprofile(data)
            .subscribe(function (resdata) {
            _this.apiCall.stopLoading();
            console.log("response from server=> ", resdata);
            if (resdata.token) {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Profile updated succesfully!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                var logindata = JSON.stringify(resdata);
                                var logindetails = JSON.parse(logindata);
                                var userDetails = window.atob(logindetails.token.split('.')[1]);
                                var details = JSON.parse(userDetails);
                                console.log(details.email);
                                localStorage.setItem("loginflag", "loginflag");
                                localStorage.setItem('details', JSON.stringify(details));
                                localStorage.setItem('condition_chk', details.isDealer);
                                _this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                                _this.footerState = 1;
                                _this.toggleFooter();
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error from service=> ", err);
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        if (this.platform.is('android')) {
            var pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "android"
            };
        }
        else {
            var pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "ios"
            };
        }
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            _this.storage.clear().then(function () {
                                console.log("ionic storage cleared!");
                            });
                            _this.navCtrl.setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\profile\profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Me\' | translate}}</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (tap)="toggleFooter()">\n        <ion-icon color="light" name="create" *ngIf="footerState == 0"></ion-icon>\n        <ion-icon color="light" name="done-all" *ngIf="footerState == 1"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item>\n      <ion-avatar item-start>\n        <img src="assets/imgs/dummy-user-img.png">\n      </ion-avatar>\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n      <p>{{\'Account:\' | translate}}{{islogin.account}}</p>\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n    <ion-item (tap)="service()">\n      <ion-icon name="person" item-start></ion-icon>\n      {{\'Service Provider\' | translate}}\n    </ion-item>\n    <ion-item (tap)="password()">\n      <ion-icon name="lock" item-start></ion-icon>\n      {{\'Password\' | translate}}\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item (tap)="settings()">\n      <ion-icon name="cog" item-start></ion-icon>\n      {{\'Settings\' | translate}}\n    </ion-item>\n  </ion-item-group>\n  <ion-item-group>\n    <ion-item-divider color="light"></ion-item-divider>\n  </ion-item-group>\n  <ion-item-group (tap)="logout()">\n    <ion-item ion-text text-center>\n      <ion-icon name="log-out"></ion-icon>&nbsp;&nbsp;{{\'Logout\' | translate}}\n    </ion-item>\n  </ion-item-group>\n\n</ion-content>\n<ion-footer padding-bottom padding-top>\n  <div style="text-align: center;">{{\'Version\' | translate}} {{aVer}}</div>\n  <br />\n  <div id="photo" style="text-align: center">\n    <span style="vertical-align:middle">{{\'Powered by\' | translate}} </span>&nbsp;\n    <a href="http://www.roadwaysgps.com/">\n      <img style="vertical-align:middle" src="assets/imgs/sign.png" width="55" height="19">\n    </a>\n  </div>\n</ion-footer>\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n  <ion-content>\n    <ion-row padding>\n      <ion-col style="text-align: center; font-size: 15px">{{\'Update Profile\' | translate}}</ion-col>\n    </ion-row>\n    <form [formGroup]="credentialsForm">\n      <ion-item>\n        <ion-label floating>{{\'First Name\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'fname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Last Name\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'lname\']" type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Email Id\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'email\']" type="email"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Phone Number\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'phonenum\']" type="number"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>{{\'Organisation\' | translate}}</ion-label>\n        <ion-input [formControl]="credentialsForm.controls[\'org\']" type="text"></ion-input>\n      </ion-item>\n\n\n      <ion-row>\n        <ion-col text-center>\n          <button ion-button block color="gpsc" (click)="onSignIn()">\n            {{\' Update Account\' | translate}}\n          </button>\n        </ion-col>\n      </ion-row>\n\n    </form>\n  </ion-content>\n</ion-pullup>\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = {};
        this.sorted = [];
        this.uData = this.navParam.get("param");
        if (this.uData.Dealer_ID != undefined) {
            this.sorted = this.uData.Dealer_ID;
        }
        else {
            this.sorted.first_name = this.uData.fn;
            this.sorted.last_name = this.uData.ln;
            this.sorted.phone = this.uData.phn;
            this.sorted.email = this.uData.email;
        }
        console.log("udata=> ", this.uData);
        console.log("udata=> ", JSON.stringify(this.uData));
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\profile\service-provider.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Provider Info\' | translate}}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="div">\n\n        <img src="assets/imgs/dummy_user.jpg">\n\n    </div>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{\'Name:\' | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.first_name}}&nbsp;{{sorted.last_name}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{\'Contacts\' | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{"Mobile" | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{"E-mail" | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.email}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>{{\'Address\' | translate}}</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\profile\service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl, apiSrv, navCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiSrv = apiSrv;
        this.navCtrl = navCtrl;
        this.passData = this.navParam.get("param");
        console.log("passData=> " + JSON.stringify(this.passData));
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_2 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_3.present();
            }
            else {
                var data = {
                    "ID": this.passData._id,
                    "OLD_PASS": this.oldP,
                    "NEW_PASS": this.newP
                };
                this.apiSrv.startLoading().present();
                this.apiSrv.updatePassword(data)
                    .subscribe(function (respData) {
                    _this.apiSrv.stopLoading();
                    console.log("respData=> ", respData);
                    var toast = _this.toastCtrl.create({
                        message: 'Password Updated successfully',
                        position: "bottom",
                        duration: 2000
                    });
                    toast.onDidDismiss(function () {
                        _this.oldP = "";
                        _this.newP = "";
                        _this.cnewP = "";
                    });
                    toast.present();
                }, function (err) {
                    _this.apiSrv.stopLoading();
                    console.log("error in update password=> ", err);
                    // debugger
                    if (err.message == "Timeout has occurred") {
                        // alert("the server is taking much time to respond. Please try in some time.")
                        var alerttemp = _this.alertCtrl.create({
                            message: "the server is taking much time to respond. Please try in some time.",
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.navCtrl.setRoot("DashboardPage");
                                    }
                                }]
                        });
                        alerttemp.present();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: err._body.message,
                            position: "bottom",
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            _this.oldP = "";
                            _this.newP = "";
                            _this.cnewP = "";
                            _this.navCtrl.setRoot("DashboardPage");
                        });
                        toast.present();
                    }
                });
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\profile\update-password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{\'Change Password\' | translate}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="savePass()">\n\n                {{\'SAVE\' | translate}}\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="{{\'Old\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="oldP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="{{\'New\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="newP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="{{\'Confirm\' | translate}} {{\'New\' | translate}} {{\'Password\' | translate}}" [(ngModel)]="cnewP"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\profile\update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalPage = /** @class */ (function () {
    function ModalPage(
        // private navParams: NavParams,
        viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.min_time = "10";
    }
    ModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.min_time);
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n        <div class=\"mainDiv\">\n            <div class=\"secondDiv\">\n                <ion-list radio-group [(ngModel)]=\"min_time\">\n                    <ion-item>\n                        <ion-label>10 Mins</ion-label>\n                        <ion-radio value=\"10\" checked></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>20 Mins</ion-label>\n                        <ion-radio value=\"20\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>30 Mins</ion-label>\n                        <ion-radio value=\"30\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>40 Mins</ion-label>\n                        <ion-radio value=\"40\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>50 Mins</ion-label>\n                        <ion-radio value=\"50\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>60 Mins</ion-label>\n                        <ion-radio value=\"60\"></ion-radio>\n                    </ion-item>\n                    \n                </ion-list>\n                <div style=\"padding: 5px;\"><button ion-button (click)=\"dismiss()\" color=\"gpsc\" block>Submit</button></div>\n            </div>\n        </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sms__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_geocoder_geocoder__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, sms, modalCtrl, popoverCtrl, geocoderApi, plt, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.sms = sms;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.geocoderApi = geocoderApi;
        this.plt = plt;
        this.translate = translate;
        this.allDevices = [];
        this.allDevicesSearch = [];
        this.option_switch = false;
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.page = 0;
        this.limit = 5;
        this.searchQuery = '';
        this.clicked = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        // this.superAdmin = this.islogin.isSuperAdmin;
        // this.adbtn = localStorage.getItem("dlrchk");
        this.dealer_Permission = this.islogin.device_add_permission;
        this.isDealer = this.islogin.isDealer;
        this.isSuperAdmin = this.islogin.isSuperAdmin;
        this.islogindealer = localStorage.getItem('isDealervalue');
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
        }
        else {
            this.stausdevice = undefined;
        }
    }
    AddDevicesPage.prototype.fonctionTest = function (d) {
        var _this = this;
        var theft;
        theft = !(d.theftAlert);
        if (theft) {
            var alert_1 = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to activate anti theft alarm? On activating this alert you will get receive notification if vehicle moves.'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            var payload = {
                                "_id": d._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                var toast = _this.toastCtrl.create({
                                    message: _this.translate.instant('Anti theft alarm Activated!'),
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: this.translate.instant('Confirm'),
                message: this.translate.instant('Are you sure you want to deactivate anti theft alarm?'),
                buttons: [
                    {
                        text: this.translate.instant('YES PROCEED'),
                        handler: function () {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": d._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: _this.translate.instant('Anti theft alarm Deactivated!'),
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    AddDevicesPage.prototype.ngOnInit = function () {
        this.now = new Date().toISOString();
    };
    AddDevicesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.getdevices();
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
    };
    AddDevicesPage.prototype.ionViewWillEnter = function () {
        this.getdevicesTemp();
    };
    AddDevicesPage.prototype.activateVehicle = function (data) {
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": data
        });
    };
    AddDevicesPage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "the server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: this.translate.instant('Okay'),
                    handler: function () {
                        _this.navCtrl.setRoot("DashboardPage");
                    }
                }]
        });
        alerttemp.present();
    };
    AddDevicesPage.prototype.showVehicleDetails = function (vdata) {
        this.navCtrl.push('VehicleDetailsPage', {
            param: vdata,
            option_switch: this.option_switch
        });
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        var that = this;
        that.page = 0;
        that.limit = 5;
        console.log('Begin async operation', refresher);
        this.getdevices();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function (d_data) {
        var that = this;
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            inputs: [
                {
                    name: 'device_name',
                    value: d_data.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: this.translate.instant('Enter Email Id/Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Cancel'),
                    handler: function () {
                    }
                },
                {
                    text: this.translate.instant('Share'),
                    handler: function (data) {
                        that.sharedevices(data, d_data);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.showDeleteBtn = function (b) {
        // debugger;
        var that = this;
        if (localStorage.getItem('isDealervalue') == 'true') {
            return false;
        }
        else {
            if (b) {
                var u = b.split(",");
                for (var p = 0; p < u.length; p++) {
                    if (that.islogin._id == u[p]) {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    AddDevicesPage.prototype.sharedVehicleDelete = function (device) {
        var that = this;
        that.deivceId = device;
        var alert = that.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this share vehicle ?'),
            buttons: [{
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        that.removeDevice(that.deivceId._id);
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    AddDevicesPage.prototype.removeDevice = function (did) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
            .subscribe(function (data) {
            console.log(data);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Shared Device was deleted successfully!'),
                duration: 1500
            });
            toast.onDidDismiss(function () {
                _this.getdevices();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.showSharedBtn = function (a, b) {
        // debugger
        if (b) {
            return !(b.split(",").indexOf(a) + 1);
        }
        else {
            return true;
        }
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = _this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = _this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            if (_this.islogin.isSuperAdmin == true) {
                baseURLp += '&supAdmin=' + _this.islogin._id;
            }
            else {
                if (_this.islogin.isDealer == true) {
                    baseURLp += '&dealer=' + _this.islogin._id;
                }
            }
            that.ndata = [];
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (res) {
                that.ndata = res.devices;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.allDevices.push(that.ndata[i]);
                }
                that.allDevicesSearch = that.allDevices;
            }, function (error) {
                console.log(error);
            });
            infiniteScroll.complete();
        }, 500);
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        // this.apiCall.toastMsgStarted();
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            var that = _this;
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
        }, function (err) {
            console.log("error=> ", err);
            _this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
        });
    };
    AddDevicesPage.prototype.getdevicesTemp = function () {
        var _this = this;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        // this.apiCall.toastMsgStarted();
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            var that = _this;
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
        }, function (err) {
            console.log("error=> ", err);
            // this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
        });
    };
    AddDevicesPage.prototype.livetrack = function (device) {
        localStorage.setItem("LiveDevice", "LiveDevice");
        var animationsOptions;
        if (this.plt.is('android')) {
            this.navCtrl.push('LiveSingleDevice', { device: device });
        }
        else {
            if (this.plt.is('ios')) {
                animationsOptions = {
                    animation: 'ios-transition',
                    duration: 1000
                };
                this.navCtrl.push('LiveSingleDevice', { device: device }, animationsOptions);
            }
        }
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push('HistoryDevicePage', {
            device: device
        });
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var that = this;
        that.allDevices[index].address = "N/A";
        if (!device.last_location) {
            that.allDevices[index].address = "N/A";
        }
        else if (device.last_location) {
            this.geocoderApi.reverseGeocode(Number(device.last_location.lat), Number(device.last_location.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, device.last_location.lat, device.last_location.long);
                that.allDevices[index].address = str;
            });
        }
    };
    AddDevicesPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apiCall.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    // getDuration(device, index) {
    //   let that = this;
    //   that.allDevices[index].duration = "0hrs 0mins";
    //   if (!device.last_ping_on) {
    //     that.allDevices[index].duration = "0hrs 0mins";
    //   } else if (device.last_ping_on) {
    //     var a = moment(new Date().toISOString());//now
    //     var b = moment(new Date(device.last_ping_on).toISOString());
    //     var mins;
    //     mins = a.diff(b, 'minutes') % 60;
    //     that.allDevices[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
    //     console.log('Current time: ', new Date().toISOString());
    //     console.log('last updated time: ', new Date(device.last_ping_on).toISOString());
    //     console.log('duration: ', that.allDevices[index].duration)
    //     // console.log(a.diff(b, 'minutes')) // 44700
    //     // console.log(a.diff(b, 'hours')) // 745
    //     // console.log(a.diff(b, 'days')) // 31
    //     // console.log(a.diff(b, 'weeks')) // 4
    //   }
    // }
    AddDevicesPage.prototype.getDuration = function (device, index) {
        var that = this;
        that.allDevices[index].duration = "0hrs 0mins";
        if (!device.status_updated_at) {
            console.log("did not found last ping on");
            that.allDevices[index].duration = "0hrs 0mins";
        }
        else if (device.status_updated_at) {
            console.log("found last ping on");
            var a = __WEBPACK_IMPORTED_MODULE_2_moment__(new Date().toISOString()); //now
            var b = __WEBPACK_IMPORTED_MODULE_2_moment__(new Date(device.status_updated_at).toISOString());
            var mins;
            mins = a.diff(b, 'minutes') % 60;
            that.allDevices[index].duration = a.diff(b, 'hours') + 'hrs ' + mins + 'mins';
            console.log('Current time: ', new Date().toISOString());
            console.log('last updated time: ', new Date(device.status_updated_at).toISOString());
            console.log('duration: ', that.allDevices[index].duration);
            // console.log(a.diff(b, 'minutes')) // 44700
            // console.log(a.diff(b, 'hours')) // 745
            // console.log(a.diff(b, 'days')) // 31
            // console.log(a.diff(b, 'weeks')) // 4
        }
    };
    AddDevicesPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        var _baseURL;
        if (this.islogin.isDealer == true) {
            _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        }
        else {
            if (this.islogin.isSuperAdmin == true) {
                _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
            }
            else {
                _baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
            }
        }
        this.apiCall.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.allDevicesSearch = data.devices;
            _this.allDevices = data.devices;
            console.log("fuel percentage: " + data.devices[0].fuel_percent);
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.checkImmobilizePassword = function () {
        var _this = this;
        var rurl = this.apiCall.mainUrl + 'users/get_user_setting';
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(rurl, Var)
            .subscribe(function (data) {
            if (!data.engine_cut_psd) {
                _this.checkedPass = 'PASSWORD_NOT_SET';
            }
            else {
                _this.checkedPass = 'PASSWORD_SET';
            }
        });
    };
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        var that = this;
        if (d.last_ACC != null || d.last_ACC != undefined) {
            if (that.clicked) {
                var alert_3 = this.alertCtrl.create({
                    message: this.translate.instant('Process ongoing..'),
                    buttons: [this.translate.instant('Okay')]
                });
                alert_3.present();
            }
            else {
                this.checkImmobilizePassword();
                this.messages = undefined;
                this.dataEngine = d;
                var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    if (_this.dataEngine.ignitionLock == '1') {
                        _this.messages = _this.translate.instant('Do you want to unlock the engine?');
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            _this.messages = _this.translate.instant('Do you want to lock the engine?');
                        }
                    }
                    var alert = _this.alertCtrl.create({
                        message: _this.messages,
                        buttons: [{
                                text: 'YES',
                                handler: function () {
                                    if (_this.immobType == 0 || _this.immobType == undefined) {
                                        that.clicked = true;
                                        var devicedetail = {
                                            "_id": _this.dataEngine._id,
                                            "engine_status": !_this.dataEngine.engine_status
                                        };
                                        // this.apiCall.startLoading().present();
                                        _this.apiCall.deviceupdateCall(devicedetail)
                                            .subscribe(function (response) {
                                            // this.apiCall.stopLoading();
                                            _this.editdata = response;
                                            var toast = _this.toastCtrl.create({
                                                message: response.message,
                                                duration: 2000,
                                                position: 'top'
                                            });
                                            toast.present();
                                            // this.responseMessage = "Edit successfully";
                                            _this.getdevices();
                                            var msg;
                                            if (!_this.dataEngine.engine_status) {
                                                msg = _this.DeviceConfigStatus[0].resume_command;
                                            }
                                            else {
                                                msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                            }
                                            _this.sms.send(d.sim_number, msg);
                                            var toast1 = _this.toastCtrl.create({
                                                message: _this.translate.instant('SMS sent successfully'),
                                                duration: 2000,
                                                position: 'bottom'
                                            });
                                            toast1.present();
                                            that.clicked = false;
                                        }, function (error) {
                                            that.clicked = false;
                                            // this.apiCall.stopLoading();
                                            console.log(error);
                                        });
                                    }
                                    else {
                                        console.log("Call server code here!!");
                                        if (that.checkedPass === 'PASSWORD_SET') {
                                            _this.askForPassword(d);
                                            return;
                                        }
                                        that.serverLevelOnOff(d);
                                    }
                                }
                            },
                            {
                                text: _this.translate.instant('NO')
                            }]
                    });
                    alert.present();
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log("some error: ", error._body.message);
                });
            }
        }
    };
    ;
    AddDevicesPage.prototype.askForPassword = function (d) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Enter Password',
            message: "Enter password for engine cut",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Proceed',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("data: ", data);
                        // if (data.password !== data.cpassword) {
                        //   this.toastmsg("Entered password and confirm password did not match.")
                        //   return;
                        // }
                        _this.verifyPassword(data, d);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.verifyPassword = function (pass, d) {
        var _this = this;
        var ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
        var payLd = {
            "uid": this.islogin._id,
            "psd": pass.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(ryurl, payLd)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
            if (resp.message === 'password not matched') {
                _this.toastmsg(resp.message);
                return;
            }
            _this.serverLevelOnOff(d);
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    AddDevicesPage.prototype.serverLevelOnOff = function (d) {
        var _this = this;
        var that = this;
        that.clicked = true;
        var data = {
            "imei": d.Device_ID,
            "_id": this.dataEngine._id,
            "engine_status": d.ignitionLock,
            "protocol_type": d.device_model.device_type
        };
        // this.apiCall.startLoading().present();
        this.apiCall.serverLevelonoff(data)
            .subscribe(function (resp) {
            // this.apiCall.stopLoading();
            console.log("ignition on off=> ", resp);
            _this.respMsg = resp;
            // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
            _this.intervalID = setInterval(function () {
                _this.apiCall.callResponse(_this.respMsg._id)
                    .subscribe(function (data) {
                    console.log("interval=> " + data);
                    _this.commandStatus = data.status;
                    if (_this.commandStatus == 'SUCCESS') {
                        clearInterval(_this.intervalID);
                        that.clicked = false;
                        // this.apiCall.stopLoadingnw();
                        var toast1 = _this.toastCtrl.create({
                            message: _this.translate.instant('process has been completed successfully!'),
                            duration: 1500,
                            position: 'bottom'
                        });
                        toast1.present();
                        _this.getdevices();
                    }
                });
            }, 5000);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error in onoff=>", err);
            that.clicked = false;
        });
    };
    AddDevicesPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.allDevicesSearch);
    };
    AddDevicesPage.prototype.onClear = function (ev) {
        this.getdevices();
        ev.target.value = '';
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create('AddDeviceModalPage');
        profileModal.onDidDismiss(function (data) {
            console.log(data);
            _this.getdevices();
        });
        profileModal.present();
    };
    AddDevicesPage.prototype.upload = function (vehData) {
        this.navCtrl.push('UploadDocPage', { vehData: vehData });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])("step"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "steps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["Navbar"])
    ], AddDevicesPage.prototype, "navBar", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\add-devices\add-devices.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Vehicle List" | translate }}</ion-title>\n    <!-- <ion-buttons end *ngIf="isDealer || isSuperAdmin"> -->\n      <ion-buttons end *ngIf="((dealer_Permission == true)||(dealer_Permission === undefined)) || isSuperAdmin">\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n    <!-- <i style="margin-left: 5px" title="Add Device" class="fas fa-plus" (click)="adddev()"\n      *ngIf="(custtype && ((dealer_Permission == true)||(dealer_Permission === undefined)))||superAdmin||adbtn"></i> -->\n  </ion-navbar>\n  <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n      refreshingText="{{\'Refreshing...\' | translate}}">\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-card *ngFor="let d of allDevicesSearch; let i = index">\n    <div *ngIf="d.expiration_date < now" style="position: relative">\n      <div>\n        <ion-item style="background-color: rgba(0, 0, 0, 0.9)">\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'car\'" />\n            <img src="assets/imgs/truck_icon_grey.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'truck\'" />\n            <img src="assets/imgs/bike_grey_icon.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'bike\'" />\n            <img src="assets/imgs/jcb_gray.png" title="{{ d.Device_Name }}" *ngIf="d.vehicleType.iconType == \'jcb\'" />\n            <img src="assets/imgs/bus_gray.png" title="{{ d.Device_Name }}" *ngIf="d.vehicleType.iconType == \'bus\'" />\n            <img src="assets/imgs/user-dummy.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'user\'" />\n            <img src="assets/imgs/tractor_gray.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'tractor\'" />\n          </ion-avatar>\n\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'car\'" />\n            <img src="assets/imgs/truck_icon_grey.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'truck\'" />\n            <img src="assets/imgs/bike_grey_icon.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'bike\'" />\n            <img src="assets/imgs/jcb_gray.png" title="{{ d.Device_Name }}" *ngIf="d.vehicleType.iconType == \'jcb\'" />\n            <img src="assets/imgs/bus_gray.png" title="{{ d.Device_Name }}" *ngIf="d.vehicleType.iconType == \'bus\'" />\n            <img src="assets/imgs/user-dummy.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'user\'" />\n            <img src="assets/imgs/tractor_gray.png" title="{{ d.Device_Name }}"\n              *ngIf="d.vehicleType.iconType == \'tractor\'" />\n          </ion-avatar>\n\n          <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null">\n            <img src="assets/imgs/car_grey_icon.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'car\'" />\n            <img src="assets/imgs/truck_icon_grey.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'truck\'" />\n            <img src="assets/imgs/bike_grey_icon.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'bike\'" />\n            <img src="assets/imgs/jcb_gray.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'jcb\'" />\n            <img src="assets/imgs/bus_gray.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'bus\'" />\n            <img src="assets/imgs/user-dummy.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'user\'" />\n            <img src="assets/imgs/tractor_gray.png" title="{{ d.Device_Name }}" *ngIf="d.iconType == \'tractor\'" />\n          </ion-avatar>\n\n          <div>\n            <h2 style="color: gray">{{ d.Device_Name }}</h2>\n            <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n              <span style="text-transform: uppercase; color: red;">{{ d.status }}\n              </span>\n              <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n                  d.status_updated_at | date: "medium"\n                }}\n              </span>\n            </p>\n          </div>\n\n          <button ion-button item-end clear *ngIf="!showDeleteBtn(d.SharedWith)">\n            <ion-icon ios="ios-more" md="md-more" color="grey" *ngIf="option_switch"></ion-icon>\n          </button>\n\n          <button ion-button item-end clear *ngIf="!option_switch">\n            <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id, d.SharedWith)"></ion-icon>\n          </button>\n\n          <button ion-button item-end clear *ngIf="showDeleteBtn(d.SharedWith)">\n            <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger"></ion-icon>\n          </button>\n\n\n          <!-- <div item-end *ngIf="d.fuel_percent">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png" />\n            </ion-avatar>\n            <p\n              style="color: gray; font-size: 10px;font-weight: 400;margin:0px;"\n            >\n              <b>{{ d.fuel_percent }}%</b>\n            </p>\n          </div> -->\n        </ion-item>\n        <ion-row style="background-color: rgba(0, 0, 0, 0.9)" padding-right>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="pin" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">\n              {{ "Live" | translate }}\n            </p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="clock" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">\n              {{ "History" | translate }}\n            </p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="power" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">\n              {{ "Ignition" | translate }}\n            </p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="lock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock == \'1\'"></ion-icon>\n            <ion-icon name="unlock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock == \'0\'"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">\n              {{ "Immobilize" | translate }}\n            </p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="battery-charging" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">\n              {{ "Power" | translate }}\n            </p>\n          </ion-col>\n          <ion-col width-20 style="text-align:center">\n            <ion-icon name="call" style="color:gray;font-size: 12px;"></ion-icon>\n            <p style="color:gray;font-size: 10px;font-weight: 400;">\n              {{ "Driver" | translate }}\n            </p>\n          </ion-col>\n        </ion-row>\n\n        <ion-item item-start class="itemStyle" style="background-color: rgba(0, 0, 0, 0.9)">\n          <ion-row>\n            <ion-col (onCreate)="device_address(d, i)" class="colSt2">\n              <div class="overme" style="color: gray">\n                {{ d.address }}\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-item>\n      </div>\n      <div>\n        <button class="activeBtn" ion-button outline color="secondary" (tap)="activateVehicle(d)">\n          Activate\n        </button>\n      </div>\n    </div>\n\n    <div *ngIf="d.expiration_date == null || d.expiration_date > now">\n      <ion-item>\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null"\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/car_blue_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/car_yellow_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/car_gray_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/truck_icon_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/truck_icon_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/bike_blue_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/bike_yellow_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/jcb_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/jcb_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/bus_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/bus_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/tractor_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/tractor_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'NO GPS FIX\'))">\n\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null"\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/car_blue_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/car_yellow_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/truck_icon_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/truck_icon_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/bike_blue_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/bike_yellow_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/jcb_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/jcb_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/img/bus_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/bus_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/img/tractor_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/tractor_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/img/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}"\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'NO GPS FIX\'))">\n\n        </ion-avatar>\n\n        <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null"\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/car_blue_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/car_yellow_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/truck_icon_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/truck_icon_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/bike_blue_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/bike_yellow_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/jcb_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/jcb_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/bus_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/bus_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/tractor_blue.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/tractor_yellow.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'NO GPS FIX\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'STOPPED\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'RUNNING\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'OUT OF REACH\'))">\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'IDLING\'))">\n          <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}"\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'NO GPS FIX\'))">\n\n        </ion-avatar>\n        <div>\n          <p style="color: black; font-size: 4vw; padding-right: 8px;">\n            {{ d.Device_Name }} &nbsp;&nbsp;\n          </p>\n\n          <!-- <p style="color:#cf1c1c;font-size: 3vw;font-weight: 400;margin:0px;" ion-text text-wrap>\n            <span style="text-transform: uppercase;">{{ d.status }} </span>\n            <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n                d.status_updated_at | date: "medium"\n              }}\n            </span>\n          </p> -->\n\n          <p style="color:#cf1c1c;font-size: 0.55em;font-weight: 400;margin:0px;" ion-text text-wrap\n            (onCreate)="getDuration(d, i)">\n            <span style="text-transform: uppercase;">{{ d.status | lowercase }} </span>\n            <span *ngIf="d.last_ping_on">{{ "from" | translate }}&nbsp;{{\n                d.duration}} <br /> {{" Last updated - " | translate}} {{d.last_ping_on | date:\'shortTime\'}},\n              {{d.last_ping_on | date: \'mediumDate\'}}\n            </span>\n          </p>\n        </div>\n\n        <div item-end *ngIf="islogin.fuel_unit === \'PERCENTAGE\'">\n          <div *ngIf="d.fuel_percent">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png" />\n            </ion-avatar>\n            <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">\n              <b>{{ d.fuel_percent }}%</b>\n            </p>\n          </div>\n        </div>\n\n        <div item-end *ngIf="islogin.fuel_unit === \'LITRE\'">\n          <div *ngIf="d.currentFuel">\n            <ion-avatar class="ava">\n              <img src="assets/imgs/fuel.png" />\n            </ion-avatar>\n            <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">\n              <b>{{ d.currentFuel }}L</b>\n            </p>\n          </div>\n        </div>\n\n        <div item-end *ngIf="d.last_speed">\n          <ion-badge color="gpsc">{{ d.last_speed }} km/h</ion-badge>\n        </div>\n        <button ion-button item-end clear color="gpsc" (click)="presentPopover($event, d); $event.stopPropagation()"\n          *ngIf="!showDeleteBtn(d.SharedWith)" style="font-size: 4vw; margin: 0px;">\n          <ion-icon ios="ios-more" md="md-more" *ngIf="option_switch"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear color="gpsc" (click)="shareVehicle(d)" *ngIf="!option_switch"\n          style="font-size: 4vw; margin: 0px;">\n          <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id, d.SharedWith)"></ion-icon>\n        </button>\n\n        <button ion-button item-end clear color="danger" (click)="sharedVehicleDelete(d)"\n          *ngIf="showDeleteBtn(d.SharedWith)" style="font-size: 4vw; margin: 0px;">\n          <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n        </button>\n      </ion-item>\n\n      <ion-row style="background-color: #fafafa;">\n        <ion-col col-2 style="text-align:center" (click)="livetrack(d)">\n          <ion-icon name="pin" style="color:#ffc900; font-size: 3vw;"></ion-icon>\n          <p style="color:#131212; font-size: 2vw; font-weight: 400;">\n            {{ "Live" | translate }}\n          </p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center" (click)="showHistoryDetail(d)">\n          <ion-icon name="clock" style="color:#d675ea;font-size: 3vw;"></ion-icon>\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n            {{ "History" | translate }}\n          </p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center">\n          <ion-icon name="power" style="color:#ef473a;font-size: 3vw;" *ngIf="d.last_ACC == \'0\'"></ion-icon>\n          <ion-icon name="power" style="color:#1de21d;font-size: 3vw;" *ngIf="d.last_ACC == \'1\'"></ion-icon>\n          <ion-icon name="power" style="color:gray;font-size: 3vw;" *ngIf="d.last_ACC == null"></ion-icon>\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n            {{ "Ignition" | translate }}\n          </p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center" (click)="IgnitionOnOff(d)">\n          <ion-icon name="lock" style="color:#ef473a;font-size: 3vw;" *ngIf="d.ignitionLock == \'1\'"></ion-icon>\n          <ion-icon name="unlock" style="color:#1de21d;font-size: 3vw;" *ngIf="d.ignitionLock == \'0\'"></ion-icon>\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n            {{ "Immobilize" | translate }}\n          </p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center">\n          <ion-icon name="battery-charging" style="color:#ef473a;font-size: 3vw;" *ngIf="d.power != \'1\'"></ion-icon>\n          <ion-icon name="battery-charging" style="color:#1de21d;font-size: 3vw;" *ngIf="d.power == \'1\'"></ion-icon>\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n            {{ "Power" | translate }}\n          </p>\n        </ion-col>\n        <ion-col col-2 style="text-align:center" (click)="dialNumber(d.contact_number)">\n          <ion-icon name="call" style="color:#0000FF;font-size: 3vw;"></ion-icon>\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n            {{ "Driver" | translate }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-item item-start class="itemStyle" style="background:#696D74;">\n        <ion-row>\n          <ion-col (onCreate)="device_address(d, i)" class="colSt2">\n            <div class="overme">\n              {{ d.address }}\n            </div>\n          </ion-col>\n          <ion-col class="colSt1">\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">\n              {{ "Today\'s distance" | translate }}\n            </p>\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">\n              {{ d.today_odo | number: "1.0-2" }}{{ "Kms" | translate }}\n            </p>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </div>\n  </ion-card>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{ \'Loading more data...\' | translate }}">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\add-devices\add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_5__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, translate) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.vehData = navParams.get("vehData");
        console.log("popover data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    PopoverPage.prototype.ngOnInit = function () { };
    PopoverPage.prototype.editItem = function () {
        var _this = this;
        console.log("edit");
        var modal = this.modalCtrl.create('UpdateDevicePage', {
            vehData: this.vehData
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.viewCtrl.dismiss();
        });
        modal.present();
    };
    // valScreen(d_id) {
    //   let modal = this.modalCtrl.create('DailyReportNewPage', {
    //     vehData: this.vehData
    //   });
    //   console.log(this.vehData);
    //   modal.onDidDismiss(() => {
    //     console.log("modal dismissed!")
    //     this.viewCtrl.dismiss();
    //   })
    //   modal.present();
    // }
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        console.log("delete");
        var alert = this.alertCtrl.create({
            message: this.translate.instant('Do you want to delete this vehicle ?'),
            buttons: [{
                    text: this.translate.instant('YES PROCEED'),
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: this.translate.instant('NO')
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Vehicle deleted successfully!'),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: this.translate.instant('Share Vehicle'),
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: this.translate.instant('Enter Email Id/Mobile Number')
                },
            ],
            buttons: [
                {
                    text: this.translate.instant('Cancel'),
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.translate.instant('Share'),
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    // valScreen() {
    //   let that = this
    //   console.log("selected=>", that.vehData);
    //   localStorage.selectedVal = JSON.stringify(that.vehData);
    //   this.navCtrl.push('DailyReportNewPage');
    //   // if(this.vehData == "Device_Name" ) {
    //   //   console.log(this.vehData);
    //   //   this.navCtrl.push('DailyReportNewPage');
    //   // }
    // }
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        console.log(data.shareId);
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            // var editdata = data;
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;{{'edit' | translate}}\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;{{'delete' | translate}}\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;{{'share' | translate}}\n      </ion-item>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__polyfills__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(421);



Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_web_animations_js_web_animations_min__);

//# sourceMappingURL=polyfills.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_app_languages__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_network_network__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_menu_menu__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_google_maps__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_add_devices_add_devices__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__node_modules_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_groups_update_group_update_group__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_all_notifications_filter_filter__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_geolocation__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_app_version__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_text_to_speech__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_call_number__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_storage__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_path__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_transfer__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_live_single_device_live_single_device__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_native_geocoder__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_view_doc_view_doc__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_add_devices_upload_doc_upload_doc__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_ac_report_ac_report__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_fuel_fuel_consumption_fuel_consumption__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_native_page_transitions__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_screen_orientation__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ngx_translate_http_loader__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__angular_common_http__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_login_login__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_geocoder_geocoder__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__providers_pager_pager__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_history_device_modal__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_profile_settings_notif_setting_notif_modal__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_native_audio__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_customers_modals_report_setting_report_setting__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_fastag_list_fastag_fastag__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_dealers_dealer_perm_dealer_perm__ = __webpack_require__(396);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















// Custom components







// import { Pro } from '@ionic/pro';
// import { Injectable, Injector } from '@angular/core';






















// Import the plugin at the top (along with other imports)




// Pro.init('501b929f', {
//   appVersion: '20.1'
// })
// @Injectable()
// export class MyErrorHandler implements ErrorHandler {
//   ionicErrorHandler: IonicErrorHandler;
//   constructor(injector: Injector) {
//     try {
//       this.ionicErrorHandler = injector.get(IonicErrorHandler);
//     } catch (e) {
//       // Unable to get the IonicErrorHandler provider, ensure
//       // IonicErrorHandler has been added to the providers list below
//     }
//   }
//   handleError(err: any): void {
//     Pro.monitoring.handleNewError(err);
//     // Remove this if you want to disable Ionic's auto exception handling
//     // in development mode.
//     this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
//   }
// }
function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_42__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_15__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                // OnCreate,
                __WEBPACK_IMPORTED_MODULE_19__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_36__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_login_login__["a" /* LanguagesPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_history_device_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_profile_settings_notif_setting_notif_modal__["a" /* NotifModalPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_customers_modals_report_setting_report_setting__["a" /* ReportSettingModal */],
                __WEBPACK_IMPORTED_MODULE_51__pages_fastag_list_fastag_fastag__["b" /* FasttagPayNow */],
                __WEBPACK_IMPORTED_MODULE_52__pages_dealers_dealer_perm_dealer_perm__["a" /* DealerPermModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_43__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/ac-report/ac-report.module#AcReportPageModule', name: 'AcReportPage', segment: 'ac-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/immobilize/immobilize.module#ImmobilizePageModule', name: 'ImmobilizePage', segment: 'immobilize', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/add-doc/add-doc.module#AddDocPageModule', name: 'AddDocPage', segment: 'add-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/upload-doc.module#UploadDocPageModule', name: 'UploadDocPage', segment: 'upload-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/notif-map/notif-map.module#NotifMapPageModule', name: 'NotifMapPage', segment: 'notif-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/group-modal/group-modal.module#GroupModalPageModule', name: 'GroupModalPage', segment: 'group-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/add-dealer/add-dealer.module#AddDealerPageModule', name: 'AddDealerPage', segment: 'add-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/edit-dealer/edit-dealer.module#EditDealerPageModule', name: 'EditDealerPage', segment: 'edit-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/add-expense/add-expense.module#AddExpensePageModule', name: 'AddExpensePage', segment: 'add-expense', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/expense-type-detail/expense-type-detail.module#ExpenseTypeDetailPageModule', name: 'ExpenseTypeDetailPage', segment: 'expense-type-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expenses/expenses.module#ExpensesPageModule', name: 'ExpensesPage', segment: 'expenses', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fastag-list/fastag-list.module#FastagListPageModule', name: 'FastagListPage', segment: 'fastag-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fastag-list/fastag/fastag.module#FastagPageModule', name: 'FastagPage', segment: 'fastag', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-chart/fuel-chart.module#FuelChartPageModule', name: 'FuelChartPage', segment: 'fuel-chart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption/fuel-consumption.module#FuelConsumptionPageModule', name: 'FuelConsumptionPage', segment: 'fuel-consumption', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-events/fuel-events.module#FuelEventsComponentModule', name: 'FuelEventsComponent', segment: 'fuel-events', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence-show/geofence-show.module#GeofenceShowPageModule', name: 'GeofenceShowPage', segment: 'geofence-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/idle-report/idle-report.module#IdleReportPageModule', name: 'IdleReportPage', segment: 'idle-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/device-settings/device-settings.module#DeviceSettingsPageModule', name: 'DeviceSettingsPage', segment: 'device-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/expired/expired.module#ExpiredPageModule', name: 'ExpiredPage', segment: 'expired', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintenance-reminder/add-reminder/add-reminder.module#AddReminderPageModule', name: 'AddReminderPage', segment: 'add-reminder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/poi-list.module#PoiListPageModule', name: 'PoiListPage', segment: 'poi-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-report/poi-report.module#POIReportPageModule', name: 'POIReportPage', segment: 'poi-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/notif-setting/notif-setting.module#NotifSettingPageModule', name: 'NotifSettingPage', segment: 'notif-setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-map-show/route-map-show.module#RouteMapShowPageModule', name: 'RouteMapShowPage', segment: 'route-map-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup-otp/signup-otp.module#SignupOtpPageModule', name: 'SignupOtpPage', segment: 'signup-otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sos-report/sos-report.module#SosReportPageModule', name: 'SosReportPage', segment: 'sos-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/your-trips/your-trips.module#YourTripsPageModule', name: 'YourTripsPage', segment: 'your-trips', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/update-device/update-device.module#UpdateDevicePageModule', name: 'UpdateDevicePage', segment: 'update-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-trip/create-trip.module#CreateTripPageModule', name: 'CreateTripPage', segment: 'create-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-customer-modal/add-customer-modal.module#AddCustomerModalModule', name: 'AddCustomerModal', segment: 'add-customer-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-device-modal.module#AddDeviceModalPageModule', name: 'AddDeviceModalPage', segment: 'add-device-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/update-cust/update-cust.module#UpdateCustModalPageModule', name: 'UpdateCustModalPage', segment: 'update-cust', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report-new/daily-report-new.module#DailyReportNewPageModule', name: 'DailyReportNewPage', segment: 'daily-report-new', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/dealers.module#DealerPageModule', name: 'DealerPage', segment: 'dealers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption-report/fuel-consumption-report.module#FuelConsumptionReportPageModule', name: 'FuelConsumptionReportPage', segment: 'fuel-consumption-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-review/trip-review.module#TripReviewPageModule', name: 'TripReviewPage', segment: 'trip-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/maintenance-reminder/maintenance-reminder.module#MaintenanceReminderPageModule', name: 'MaintenanceReminderPage', segment: 'maintenance-reminder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/add-poi/add-poi.module#AddPoiPageModule', name: 'AddPoiPage', segment: 'add-poi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/vehicle-details/vehicle-details.module#VehicleDetailsPageModule', name: 'VehicleDetailsPage', segment: 'vehicle-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_27__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_41__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_41__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_43__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_16__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_19__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_36__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_login_login__["a" /* LanguagesPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_history_device_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_profile_settings_notif_setting_notif_modal__["a" /* NotifModalPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_customers_modals_report_setting_report_setting__["a" /* ReportSettingModal */],
                __WEBPACK_IMPORTED_MODULE_51__pages_fastag_list_fastag_fastag__["b" /* FasttagPayNow */],
                __WEBPACK_IMPORTED_MODULE_52__pages_dealers_dealer_perm_dealer_perm__["a" /* DealerPermModalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
                // [{ provide: ErrorHandler, useClass: MyErrorHandler }],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] },
                __WEBPACK_IMPORTED_MODULE_11__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_12__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_13__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_google_maps__["h" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_40__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_8__providers_app_languages__["a" /* AppLanguages */],
                __WEBPACK_IMPORTED_MODULE_45__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_45__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_46__providers_pager_pager__["a" /* PagerProvider */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_native_audio__["a" /* NativeAudio */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 186,
	"./af.js": 186,
	"./ar": 187,
	"./ar-dz": 188,
	"./ar-dz.js": 188,
	"./ar-kw": 189,
	"./ar-kw.js": 189,
	"./ar-ly": 190,
	"./ar-ly.js": 190,
	"./ar-ma": 191,
	"./ar-ma.js": 191,
	"./ar-sa": 192,
	"./ar-sa.js": 192,
	"./ar-tn": 193,
	"./ar-tn.js": 193,
	"./ar.js": 187,
	"./az": 194,
	"./az.js": 194,
	"./be": 195,
	"./be.js": 195,
	"./bg": 196,
	"./bg.js": 196,
	"./bm": 197,
	"./bm.js": 197,
	"./bn": 198,
	"./bn.js": 198,
	"./bo": 199,
	"./bo.js": 199,
	"./br": 200,
	"./br.js": 200,
	"./bs": 201,
	"./bs.js": 201,
	"./ca": 202,
	"./ca.js": 202,
	"./cs": 203,
	"./cs.js": 203,
	"./cv": 204,
	"./cv.js": 204,
	"./cy": 205,
	"./cy.js": 205,
	"./da": 206,
	"./da.js": 206,
	"./de": 207,
	"./de-at": 208,
	"./de-at.js": 208,
	"./de-ch": 209,
	"./de-ch.js": 209,
	"./de.js": 207,
	"./dv": 210,
	"./dv.js": 210,
	"./el": 211,
	"./el.js": 211,
	"./en-au": 212,
	"./en-au.js": 212,
	"./en-ca": 213,
	"./en-ca.js": 213,
	"./en-gb": 214,
	"./en-gb.js": 214,
	"./en-ie": 215,
	"./en-ie.js": 215,
	"./en-il": 216,
	"./en-il.js": 216,
	"./en-nz": 217,
	"./en-nz.js": 217,
	"./eo": 218,
	"./eo.js": 218,
	"./es": 219,
	"./es-do": 220,
	"./es-do.js": 220,
	"./es-us": 221,
	"./es-us.js": 221,
	"./es.js": 219,
	"./et": 222,
	"./et.js": 222,
	"./eu": 223,
	"./eu.js": 223,
	"./fa": 224,
	"./fa.js": 224,
	"./fi": 225,
	"./fi.js": 225,
	"./fo": 226,
	"./fo.js": 226,
	"./fr": 227,
	"./fr-ca": 228,
	"./fr-ca.js": 228,
	"./fr-ch": 229,
	"./fr-ch.js": 229,
	"./fr.js": 227,
	"./fy": 230,
	"./fy.js": 230,
	"./gd": 231,
	"./gd.js": 231,
	"./gl": 232,
	"./gl.js": 232,
	"./gom-latn": 233,
	"./gom-latn.js": 233,
	"./gu": 234,
	"./gu.js": 234,
	"./he": 235,
	"./he.js": 235,
	"./hi": 236,
	"./hi.js": 236,
	"./hr": 237,
	"./hr.js": 237,
	"./hu": 238,
	"./hu.js": 238,
	"./hy-am": 239,
	"./hy-am.js": 239,
	"./id": 240,
	"./id.js": 240,
	"./is": 241,
	"./is.js": 241,
	"./it": 242,
	"./it.js": 242,
	"./ja": 243,
	"./ja.js": 243,
	"./jv": 244,
	"./jv.js": 244,
	"./ka": 245,
	"./ka.js": 245,
	"./kk": 246,
	"./kk.js": 246,
	"./km": 247,
	"./km.js": 247,
	"./kn": 248,
	"./kn.js": 248,
	"./ko": 249,
	"./ko.js": 249,
	"./ky": 250,
	"./ky.js": 250,
	"./lb": 251,
	"./lb.js": 251,
	"./lo": 252,
	"./lo.js": 252,
	"./lt": 253,
	"./lt.js": 253,
	"./lv": 254,
	"./lv.js": 254,
	"./me": 255,
	"./me.js": 255,
	"./mi": 256,
	"./mi.js": 256,
	"./mk": 257,
	"./mk.js": 257,
	"./ml": 258,
	"./ml.js": 258,
	"./mn": 259,
	"./mn.js": 259,
	"./mr": 260,
	"./mr.js": 260,
	"./ms": 261,
	"./ms-my": 262,
	"./ms-my.js": 262,
	"./ms.js": 261,
	"./mt": 263,
	"./mt.js": 263,
	"./my": 264,
	"./my.js": 264,
	"./nb": 265,
	"./nb.js": 265,
	"./ne": 266,
	"./ne.js": 266,
	"./nl": 267,
	"./nl-be": 268,
	"./nl-be.js": 268,
	"./nl.js": 267,
	"./nn": 269,
	"./nn.js": 269,
	"./pa-in": 270,
	"./pa-in.js": 270,
	"./pl": 271,
	"./pl.js": 271,
	"./pt": 272,
	"./pt-br": 273,
	"./pt-br.js": 273,
	"./pt.js": 272,
	"./ro": 274,
	"./ro.js": 274,
	"./ru": 275,
	"./ru.js": 275,
	"./sd": 276,
	"./sd.js": 276,
	"./se": 277,
	"./se.js": 277,
	"./si": 278,
	"./si.js": 278,
	"./sk": 279,
	"./sk.js": 279,
	"./sl": 280,
	"./sl.js": 280,
	"./sq": 281,
	"./sq.js": 281,
	"./sr": 282,
	"./sr-cyrl": 283,
	"./sr-cyrl.js": 283,
	"./sr.js": 282,
	"./ss": 284,
	"./ss.js": 284,
	"./sv": 285,
	"./sv.js": 285,
	"./sw": 286,
	"./sw.js": 286,
	"./ta": 287,
	"./ta.js": 287,
	"./te": 288,
	"./te.js": 288,
	"./tet": 289,
	"./tet.js": 289,
	"./tg": 290,
	"./tg.js": 290,
	"./th": 291,
	"./th.js": 291,
	"./tl-ph": 292,
	"./tl-ph.js": 292,
	"./tlh": 293,
	"./tlh.js": 293,
	"./tr": 294,
	"./tr.js": 294,
	"./tzl": 295,
	"./tzl.js": 295,
	"./tzm": 296,
	"./tzm-latn": 297,
	"./tzm-latn.js": 297,
	"./tzm.js": 296,
	"./ug-cn": 298,
	"./ug-cn.js": 298,
	"./uk": 299,
	"./uk.js": 299,
	"./ur": 300,
	"./ur.js": 300,
	"./uz": 301,
	"./uz-latn": 302,
	"./uz-latn.js": 302,
	"./uz.js": 301,
	"./vi": 303,
	"./vi.js": 303,
	"./x-pseudo": 304,
	"./x-pseudo.js": 304,
	"./yo": 305,
	"./yo.js": 305,
	"./zh-cn": 306,
	"./zh-cn.js": 306,
	"./zh-hk": 307,
	"./zh-hk.js": 307,
	"./zh-tw": 308,
	"./zh-tw.js": 308
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 453;

/***/ }),

/***/ 470:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Language; });
var Language = /** @class */ (function () {
    function Language(name, key) {
        this.name = name;
        this.key = key;
    }
    return Language;
}());

//# sourceMappingURL=app.model.js.map

/***/ }),

/***/ 491:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapStyle; });
var mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#263c3f"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#6b9a76"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#38414e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#212a37"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9ca5b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#1f2835"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#f3d19c"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#2f3948"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#515c6d"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    }
];
//# sourceMappingURL=map-style.model.js.map

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_network__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












// import { BackgroundMode } from '@ionic-native/background-mode';
// import { PowerManagement } from '@ionic-native/power-management';
var MyApp = /** @class */ (function () {
    // private languages: Array<String>;
    function MyApp(translate, platform, statusBar, splashScreen, 
        // private network: Network,
        events, networkProvider, menuProvider, menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl, tts) {
        var _this = this;
        this.translate = translate;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuProvider = menuProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__["ReplaySubject"](0);
        this.textDir = "ltr";
        this.pageRoot = "DashboardPage";
        this.headerContent = "header";
        translate.setDefaultLang('en');
        this.events.subscribe('lang:key', function (key) {
            console.log("subscribed key: ", key);
            translate.setDefaultLang(key);
        });
        // this.backgroundMode.enable();
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            console.log("islogin=> " + JSON.stringify(_this.islogin));
            _this.checkReportStatus();
        });
        this.events.subscribe('notif:updated', function (notifData) {
            console.log("text to speech updated=> ", notifData);
            // this.notfiD = notifData;
            localStorage.setItem("notifValue", notifData);
        });
        this.events.subscribe('Screen:Changed', function (screenData) {
            console.log("screen data changed: ", screenData);
            if (screenData === 'vehiclelist') {
                _this.rootPage = 'AddDevicesPage';
                _this.pageRoot = 'AddDevicesPage';
            }
            else if (screenData === 'live') {
                _this.rootPage = 'LivePage';
                _this.pageRoot = 'LivePage';
            }
            else {
                _this.rootPage = 'DashboardPage';
                _this.pageRoot = 'DashboardPage';
            }
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                if (activeView.name == "DashboardPage") {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_1.present();
                }
                else {
                    if (nav.canGoBack()) {
                        nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
                    }
                    else {
                        platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
                    }
                }
            });
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        this.dealerStatus = this.islogin.isDealer;
        this.getSideMenuData();
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    this.rootPage = 'LivePage';
                }
                else if (localStorage.getItem("SCREEN") === 'dashboard') {
                    this.rootPage = 'DashboardPage';
                }
                else if (localStorage.getItem("SCREEN") === 'vehiclelist') {
                    this.rootPage = 'AddDevicesPage';
                }
            }
            else {
                this.rootPage = 'DashboardPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
    }
    MyApp.prototype.resize = function () {
        var offset = this.headerTag.nativeElement.offsetHeight;
        this.scrollableTag.nativeElement.style.marginTop = offset + 'px';
    };
    MyApp.prototype.getSideMenuData = function () {
        this.pages = this.menuProvider.getSideMenus();
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
                that.pushSetup();
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
        this.push.createChannel({
            id: "ignitionon",
            description: "ignition on description",
            sound: 'ignitionon',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('ignitionon Channel created'); });
        this.push.createChannel({
            id: "ignitionoff",
            description: "ignition off description",
            sound: 'ignitionoff',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('ignitionoff Channel created'); });
        this.push.createChannel({
            id: "devicepowerdisconnected",
            description: "devicepowerdisconnected description",
            sound: 'devicepowerdisconnected',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('devicepowerdisconnected Channel created'); });
        this.push.createChannel({
            id: "default",
            description: "default description",
            sound: 'default',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('default Channel created'); });
        this.push.createChannel({
            id: "sos",
            description: "default description",
            sound: 'notif',
            // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
            importance: 4
        }).then(function () { return console.log('sos Channel created'); });
        // Delete a channel (Android O and above)
        // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));
        // Return a list of currently configured channels
        this.push.listChannels().then(function (channels) { return console.log('List of channels', channels); });
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
                icon: 'icicon',
                iconColor: 'red'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            if (localStorage.getItem("notifValue") != null) {
                if (localStorage.getItem("notifValue") == 'true') {
                    _this.tts.speak(notification.message)
                        .then(function () { return console.log('Success'); })
                        .catch(function (reason) { return console.log(reason); });
                }
            }
            if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            // that.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.platform.pause.subscribe(function (ev) {
                console.log("platform paused");
                localStorage.setItem("backgroundModeTime", JSON.stringify(new Date()));
            });
            that.pushNotify();
            that.networkProvider.initializeNetworkEvents();
            // Offline event
            that.events.subscribe('network:offline', function () {
                // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
                alert("Internet is not connected... please make sure the internet connection is working properly.");
            });
            // Online event
            that.events.subscribe('network:online', function () {
                alert('network:online ==> ' + _this.networkProvider.getNetworkType());
            });
            // that.backBtnHandler();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        // debugger
        if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && (this.islogin.isDealer == false || this.islogin.isDealer == undefined)) {
            this.options.push({
                iconName: 'home',
                displayText: this.translate.instant('Home'),
                component: this.pageRoot,
            });
            this.options.push({
                iconName: 'people',
                displayText: this.translate.instant('Groups'),
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: this.translate.instant('Dealers'),
                component: 'DealerPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: this.translate.instant('Customers'),
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'notifications',
                displayText: this.translate.instant('Notifications'),
                component: 'AllNotificationsPage'
            });
            this.options.push({
                iconName: 'list',
                displayText: this.translate.instant('POI List'),
                component: 'PoiListPage'
            });
            this.options.push({
                iconName: 'cash',
                displayText: this.translate.instant('Expenses'),
                component: 'ExpensesPage'
            });
            // this.options.push({
            //   iconName: 'custom-fastag',
            //   displayText: this.translate.instant('Fastag'),
            //   component: 'FastagListPage'
            // });
            this.options.push({
                displayText: this.translate.instant('Fuel'),
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Entry'),
                        component: 'FuelConsumptionPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Consumption'),
                        component: 'FuelConsumptionReportPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: this.translate.instant('Fuel Chart'),
                        component: 'FuelChartPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant('Vehicle Maintenance'),
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'notifications',
                        displayText: this.translate.instant('Reminders'),
                        component: 'MaintenanceReminderPage'
                    }
                ]
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: this.translate.instant(this.translate.instant('Reports')),
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'clipboard',
                        displayText: 'Idle Report',
                        component: 'IdleReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('AC Report'),
                        component: 'AcReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Daily Report'),
                        component: 'DailyReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Value Screen'),
                        component: 'DailyReportNewPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('User Trip report'),
                        component: 'YourTripsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Summary Report'),
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Geofence Report'),
                        component: 'GeofenceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Over Speed Report'),
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Ignition Report'),
                        component: 'IgnitionReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Stoppages Report'),
                        component: 'StoppagesRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Distance Report'),
                        component: 'DistanceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Trip Report'),
                        component: 'TripReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Route Violation Report'),
                        component: 'RouteVoilationsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('Speed Variation Report'),
                        component: 'SpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('SOS'),
                        component: 'SosReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: this.translate.instant('POI Report'),
                        component: 'POIReportPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            this.options.push({
                displayText: this.translate.instant('Routes'),
                iconName: 'map',
                component: 'RoutePage'
            });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: this.translate.instant('Customer Support'),
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: this.translate.instant('Rate Us'),
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: this.translate.instant('Contact Us'),
                        component: 'ContactUsPage'
                    }
                ]
            });
            this.options.push({
                displayText: this.translate.instant('Profile'),
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
                this.options.push({
                    iconName: 'home',
                    displayText: this.translate.instant('Home'),
                    component: this.pageRoot,
                });
                this.options.push({
                    iconName: 'people',
                    displayText: this.translate.instant('Groups'),
                    component: 'GroupsPage'
                });
                this.options.push({
                    iconName: 'notifications',
                    displayText: this.translate.instant('Notifications'),
                    component: 'AllNotificationsPage'
                });
                // this.options.push({
                //   iconName: 'walk',
                //   displayText: this.translate.instant('Your Trips'),
                //   component: 'YourTripsPage'
                // });
                this.options.push({
                    iconName: 'list',
                    displayText: this.translate.instant('POI List'),
                    component: 'PoiListPage'
                });
                this.options.push({
                    iconName: 'cash',
                    displayText: this.translate.instant('Expenses'),
                    component: 'ExpensesPage'
                });
                this.options.push({
                    iconName: 'custom-fastag',
                    displayText: this.translate.instant('Fastag'),
                    component: 'FastagListPage'
                });
                // this.options.push({
                //   displayText: this.translate.instant('Fuel'),
                //   iconName: 'arrow-dropright',
                //   suboptions: this.fuelSuboptions
                // });
                this.options.push({
                    displayText: this.translate.instant('Fuel'),
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'custom-fuel',
                            displayText: this.translate.instant('Fuel Entry'),
                            component: 'FuelConsumptionPage'
                        },
                        {
                            iconName: 'custom-fuel',
                            displayText: this.translate.instant('Fuel Consumption'),
                            component: 'FuelConsumptionReportPage'
                        },
                        {
                            iconName: 'custom-fuel',
                            displayText: this.translate.instant('Fuel Chart'),
                            component: 'FuelChartPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: this.translate.instant('Vehicle Maintenance'),
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'notifications',
                            displayText: this.translate.instant('Reminders'),
                            component: 'MaintenanceReminderPage'
                        }
                    ]
                });
                // Load options with nested items (with icons)
                // -----------------------------------------------
                // this.options.push({
                //   displayText: this.translate.instant('Reports'),
                //   iconName: 'arrow-dropright',
                //   suboptions: this.suboptions
                // });
                this.options.push({
                    displayText: this.translate.instant(this.translate.instant('Reports')),
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Idle Report'),
                            component: 'IdleReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('AC Report'),
                            component: 'AcReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Daily Report'),
                            component: 'DailyReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Value Screen'),
                            component: 'DailyReportNewPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('User Trip report'),
                            component: 'YourTripsPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Summary Report'),
                            component: 'DeviceSummaryRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Geofence Report'),
                            component: 'GeofenceReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Over Speed Report'),
                            component: 'OverSpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Ignition Report'),
                            component: 'IgnitionReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Stoppages Report'),
                            component: 'StoppagesRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Distance Report'),
                            component: 'DistanceReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Trip Report'),
                            component: 'TripReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Route Violation Report'),
                            component: 'RouteVoilationsPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('Speed Variation Report'),
                            component: 'SpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('SOS'),
                            component: 'SosReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: this.translate.instant('POI Report'),
                            component: 'POIReportPage'
                        }
                    ]
                });
                // Load options with nested items (without icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: this.translate.instant('Routes'),
                    iconName: 'map',
                    component: 'RoutePage'
                });
                // Load special options
                // -----------------------------------------------
                this.options.push({
                    displayText: this.translate.instant('Customer Support'),
                    suboptions: [
                        {
                            iconName: 'star',
                            displayText: this.translate.instant('Rate Us'),
                            component: 'FeedbackPage'
                        },
                        {
                            iconName: 'mail',
                            displayText: this.translate.instant('Contact Us'),
                            component: 'ContactUsPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: this.translate.instant('Profile'),
                    iconName: 'person',
                    component: 'ProfilePage'
                });
            }
            else {
                if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
                    this.options.push({
                        iconName: 'home',
                        displayText: this.translate.instant('Home'),
                        component: this.pageRoot,
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: this.translate.instant('Groups'),
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'contacts',
                        displayText: this.translate.instant('Customers'),
                        component: 'CustomersPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: this.translate.instant('Notifications'),
                        component: 'AllNotificationsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: this.translate.instant('POI List'),
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        iconName: 'cash',
                        displayText: this.translate.instant('Expenses'),
                        component: 'ExpensesPage'
                    });
                    this.options.push({
                        displayText: this.translate.instant('Fuel'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Entry'),
                                component: 'FuelConsumptionPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Consumption'),
                                component: 'FuelConsumptionReportPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: this.translate.instant('Fuel Chart'),
                                component: 'FuelChartPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Vehicle Maintenance'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'notifications',
                                displayText: this.translate.instant('Reminders'),
                                component: 'MaintenanceReminderPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Reports'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: 'Idle Report',
                                component: 'IdleReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('AC Report'),
                                component: 'AcReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Daily Report'),
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Value Screen'),
                                component: 'DailyReportNewPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('User Trip report'),
                                component: 'YourTripsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Summary Report'),
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Geofence Report'),
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Over Speed Report'),
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Ignition Report'),
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Stoppages Report'),
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Distance Report'),
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Trip Report'),
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Route Violation Report'),
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('Speed Variation Report'),
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('SOS'),
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: this.translate.instant('POI Report'),
                                component: 'POIReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Routes'),
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Customer Support'),
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: this.translate.instant('Rate Us'),
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: this.translate.instant('Contact Us'),
                                component: 'ContactUsPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Profile'),
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
                else {
                    this.options.push({
                        iconName: 'home',
                        displayText: this.translate.instant('Home'),
                        component: this.pageRoot,
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: this.translate.instant('Groups'),
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: this.translate.instant('Notifications'),
                        component: 'AllNotificationsPage'
                    });
                    // this.options.push({
                    //   iconName: 'walk',
                    //   displayText: this.translate.instant('Your Trips'),
                    //   component: 'YourTripsPage'
                    // });
                    this.options.push({
                        iconName: 'list',
                        displayText: this.translate.instant('POI List'),
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        iconName: 'cash',
                        displayText: this.translate.instant('Expenses'),
                        component: 'ExpensesPage'
                    });
                    this.options.push({
                        iconName: 'custom-fastag',
                        displayText: this.translate.instant('Fastag'),
                        component: 'FastagListPage'
                    });
                    this.options.push({
                        displayText: this.translate.instant('Fuel'),
                        iconName: 'arrow-dropright',
                        suboptions: this.fuelSuboptions
                    });
                    this.options.push({
                        displayText: this.translate.instant('Vehicle Maintenance'),
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'notifications',
                                displayText: this.translate.instant('Reminders'),
                                component: 'MaintenanceReminderPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Reports'),
                        iconName: 'arrow-dropright',
                        suboptions: this.suboptions
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Routes'),
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: this.translate.instant('Customer Support'),
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: this.translate.instant('Rate Us'),
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: this.translate.instant('Contact Us'),
                                component: 'ContactUsPage'
                            },
                        ]
                    });
                    this.options.push({
                        displayText: this.translate.instant('Profile'),
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
            }
        }
        console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"));
        var _DealerStat = localStorage.getItem("dealer_status");
        var _CustStat = localStorage.getItem("custumer_status");
        var onlyDeal = localStorage.getItem("OnlyDealer");
        if (_DealerStat != null || _CustStat != null) {
            if (_DealerStat == 'ON' && _CustStat == 'OFF') {
                this.options[2].displayText = this.translate.instant('Admin');
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
                this.options[3].displayText = this.translate.instant('Customers');
                this.options[3].iconName = 'contacts';
                this.options[3].component = 'CustomersPage';
            }
            else {
                if (_DealerStat == 'OFF' && _CustStat == 'ON') {
                    this.options[2].displayText = this.translate.instant('Dealers');
                    this.options[2].iconName = 'person';
                    this.options[2].component = 'DashboardPage';
                }
                else {
                    if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
                        this.options[2].displayText = this.translate.instant('Dealers');
                        this.options[2].iconName = 'person';
                        this.options[2].component = 'DealerPage';
                        this.options[3].displayText = this.translate.instant('Customers');
                        this.options[3].iconName = 'contacts';
                        this.options[3].component = 'CustomersPage';
                    }
                    else {
                        if (onlyDeal == 'true') {
                            this.options[2].displayText = this.translate.instant('Customers');
                            this.options[2].iconName = 'contacts';
                            this.options[2].component = 'CustomersPage';
                        }
                    }
                }
            }
        }
        this.events.subscribe("sidemenu:event", function (data) {
            console.log("sidemenu:event=> ", data);
            if (data) {
                _this.options[2].displayText = _this.translate.instant('Dealers');
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    MyApp.prototype.checkReportStatus = function () {
        this.suboptions = [];
        this.fuelSuboptions = [];
        if (this.islogin.report_preference.ac_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('AC Report'),
                component: 'AcReportPage'
            });
        }
        if (this.islogin.report_preference.sos_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('SOS'),
                component: 'SosReportPage'
            });
        }
        if (this.islogin.report_preference.poi_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('POI Report'),
                component: 'POIReportPage'
            });
        }
        if (this.islogin.report_preference.distance_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Distance Report'),
                component: 'DistanceReportPage'
            });
        }
        if (this.islogin.report_preference.ignition_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Ignition Report'),
                component: 'IgnitionReportPage'
            });
        }
        if (this.islogin.report_preference.stoppage_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Stoppages Report'),
                component: 'StoppagesRepoPage'
            });
        }
        if (this.islogin.report_preference.route_violation_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Route Violation Report'),
                component: 'RouteVoilationsPage'
            });
        }
        if (this.islogin.report_preference.overspeed_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Over Speed Report'),
                component: 'OverSpeedRepoPage'
            });
        }
        if (this.islogin.report_preference.geofence_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Geofence Report'),
                component: 'GeofenceReportPage'
            });
        }
        if (this.islogin.report_preference.summary_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Summary Report'),
                component: 'DeviceSummaryRepoPage'
            });
        }
        if (this.islogin.report_preference.trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Trip Report'),
                component: 'TripReportPage'
            });
        }
        if (this.islogin.report_preference.user_trip_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('User Trip report'),
                component: 'YourTripsPage'
            });
        }
        if (this.islogin.report_preference.idle_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Idle Report'),
                component: 'IdleReportPage'
            });
        }
        if (this.islogin.report_preference.speed_variation.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Speed Variation Report'),
                component: 'SpeedRepoPage'
            });
        }
        if (this.islogin.report_preference.daily_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('Daily Report'),
                component: 'DailyReportPage'
            });
        }
        if (this.islogin.report_preference.ac_report.Rstatus) {
            this.suboptions.push({
                iconName: 'clipboard',
                displayText: this.translate.instant('AC Report'),
                component: 'AcReportPage'
            });
        }
        this.suboptions.push({
            iconName: 'clipboard',
            displayText: this.translate.instant('Value Screen'),
            component: 'DailyReportNewPage'
        });
        console.log("suboptions list: ", this.suboptions);
        this.fuelSuboptions.push({
            iconName: 'custom-fuel',
            displayText: this.translate.instant('Fuel Entry'),
            component: 'FuelConsumptionPage'
        });
        if (this.islogin.report_preference.fuel_consumption_report.Rstatus) {
            this.fuelSuboptions.push({
                iconName: 'custom-fuel',
                displayText: this.translate.instant('Fuel Consumption'),
                component: 'FuelConsumptionReportPage'
            });
        }
        this.fuelSuboptions.push({
            iconName: 'custom-fuel',
            displayText: this.translate.instant('Fuel Chart'),
            component: 'FuelChartPage'
        });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                var params = option.custom && option.custom.param;
                if (option.displayText == _this.translate.instant('Admin') && option.component == 'DashboardPage') {
                    localStorage.setItem("dealer_status", 'OFF');
                    localStorage.setItem('details', localStorage.getItem("superAdminData"));
                    localStorage.removeItem('superAdminData');
                }
                if (option.displayText == _this.translate.instant('Dealers') && option.component == 'DashboardPage') {
                    if (localStorage.getItem('custumer_status') == 'ON') {
                        var _dealdata = JSON.parse(localStorage.getItem("dealer"));
                        if (localStorage.getItem("superAdminData") != null || _this.islogin.isSuperAdmin == true) {
                            localStorage.setItem("dealer_status", 'ON');
                        }
                        else {
                            if (_dealdata.isSuperAdmin == true) {
                                localStorage.setItem("dealer_status", 'OFF');
                            }
                            else {
                                localStorage.setItem("OnlyDealer", "true");
                            }
                        }
                        localStorage.setItem("custumer_status", 'OFF');
                        localStorage.setItem('details', localStorage.getItem("dealer"));
                    }
                    else {
                        console.log("something wrong!!");
                    }
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.chkCondition = function () {
        var _this = this;
        this.resize();
        this.events.subscribe("event_sidemenu", function (data) {
            _this.islogin = JSON.parse(data);
            _this.options[2].displayText = _this.translate.instant('Dealers');
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            _this.initializeOptions();
        });
        this.initializeOptions();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('headerTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "headerTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('scrollableTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "scrollableTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\app\app.html"*/'<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)="chkCondition()">\n\n  <ion-header> </ion-header>\n\n  <ion-content id="outerNew">\n\n    <div class="headProf" #headerTag ion-fixed>\n\n      <img src="assets/imgs/dummy-user-img.png" />\n\n      <div>\n\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}\n\n        </p>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}\n\n        </p>\n\n      </div>\n\n    </div>\n\n    <!-- <ion-scroll style="width:100%; height:100vh" scrollY="true"> -->\n\n    <div #scrollableTag>\n\n      <side-menu-content style="width:100%; height:100vh" [settings]="sideMenuSettings" [options]="options"\n\n        (change)="onOptionSelected($event)"></side-menu-content>\n\n    </div>\n\n    <!-- </ion-scroll> -->\n\n\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__["a" /* MenuProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__["a" /* TextToSpeech */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            grouptype: ['']
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\groups\update-group\update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\groups\update-group\update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GeocoderProvider = /** @class */ (function () {
    function GeocoderProvider(_GEOCODE) {
        this._GEOCODE = _GEOCODE;
        console.log('Hello GeocoderProvider Provider');
    }
    GeocoderProvider.prototype.reverseGeocode = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var a = result[0].thoroughfare ? result[0].thoroughfare : null;
                var b = result[0].subThoroughfare ? result[0].subThoroughfare : null;
                var c = result[0].subLocality ? result[0].subLocality : null;
                var d = result[0].subAdministrativeArea ? result[0].subAdministrativeArea : null;
                var e = result[0].postalCode ? result[0].postalCode : null;
                var f = result[0].locality ? result[0].locality : null;
                var g = result[0].countryName ? result[0].countryName : null;
                var h = result[0].administrativeArea ? result[0].administrativeArea : null;
                var str = '';
                if (a != null && a != 'Unnamed Road')
                    str = a + ', ';
                if (b != null && b != 'Unnamed Road')
                    str = str + b + ', ';
                if (c != null && c != 'Unnamed Road')
                    str = str + c + ', ';
                if (d != null && d != 'Unnamed Road')
                    str = str + d + ', ';
                if (e != null && e != 'Unnamed Road')
                    str = str + e + ', ';
                if (f != null && f != 'Unnamed Road')
                    str = str + f + ', ';
                if (g != null && g != 'Unnamed Road')
                    str = str + g + ', ';
                if (h != null && h != 'Unnamed Road')
                    str = str + h + ', ';
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider.prototype.geocoderResult = function (lat, lng) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                "position": {
                    lat: lat,
                    lng: lng
                }
            }).then(function (results) {
                var addr;
                if (results.length == 0) {
                    addr = 'N/A';
                    resolve(addr);
                }
                else {
                    addr = results[0].extra.lines[0];
                    resolve(addr);
                }
            }).catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__["a" /* NativeGeocoder */]])
    ], GeocoderProvider);
    return GeocoderProvider;
}());

//# sourceMappingURL=geocoder.js.map

/***/ })

},[403]);
//# sourceMappingURL=main.js.map