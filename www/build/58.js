webpackJsonp([58],{

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaytmwalletloginPageModule", function() { return PaytmwalletloginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__ = __webpack_require__(601);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PaytmwalletloginPageModule = /** @class */ (function () {
    function PaytmwalletloginPageModule() {
    }
    PaytmwalletloginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__paytmwalletlogin__["a" /* PaytmwalletloginPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], PaytmwalletloginPageModule);
    return PaytmwalletloginPageModule;
}());

//# sourceMappingURL=paytmwalletlogin.module.js.map

/***/ }),

/***/ 601:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaytmwalletloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaytmwalletloginPage = /** @class */ (function () {
    function PaytmwalletloginPage(navCtrl, navParams, toastCtrl, apiCall, menu, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.menu = menu;
        this.toast = toast;
        this.successresponse = false;
        this.inputform = false;
        this.razor_key = 'rzp_live_jB4onRx1BUUvxt';
        this.paymentAmount = 120000;
        this.currency = 'INR';
        // this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.islogin = localStorage.getItem('details') ? JSON.parse(localStorage.getItem('details')) : "";
        this.paytmnumber = this.islogin.phn;
        this.parameters = navParams.get('param');
        console.log("parameters", this.parameters);
    }
    PaytmwalletloginPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(true);
    };
    PaytmwalletloginPage.prototype.payWithRazor = function () {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/GO0jiDP.jpg',
            currency: this.currency,
            key: this.razor_key,
            amount: this.paymentAmount,
            name: this.parameters.Device_Name,
            prefill: {
                email: this.islogin.email,
                contact: this.paytmnumber,
                name: this.islogin.fn + ' ' + this.islogin.ln
            },
            theme: {
                color: '#d80622'
            },
            modal: {
                ondismiss: function () {
                    console.log('dismissed');
                }
            }
        };
        var successCallback = function (payment_id) {
            alert('payment_id: ' + payment_id);
            this.updateExpDate();
        };
        var cancelCallback = function (error) {
            alert(error.description + ' (Error ' + error.code + ')');
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
    };
    PaytmwalletloginPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    PaytmwalletloginPage.prototype.updateExpDate = function () {
        var _this = this;
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        var expDate = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("updated expiry date: ", expDate);
        var url = this.apiCall.mainUrl + 'devices/editDevMaster';
        var payload = {
            _id: this.parameters.Device_ID,
            expiration_date: new Date(expDate).toISOString()
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, payload)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log("respData of expiry date updation: ", respData);
            var toast = _this.toastCtrl.create({
                message: 'Payment success!!',
                duration: 1000,
                position: 'middle'
            });
            toast.onDidDismiss(function () {
                _this.navCtrl.pop();
            });
            toast.present();
        }, function (err) {
            console.log("oops got error: ", err);
            _this.apiCall.stopLoading();
        });
    };
    PaytmwalletloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-paytmwalletlogin',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\add-devices\paytmwalletlogin\paytmwalletlogin.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "Payment Method" | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding>\n\n  <ion-grid text-center>\n\n    <ion-row>\n\n      <ion-col>\n\n        ** Pay with razorpay **\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-card class="welcome-card">\n\n    <img src="/assets/imgs/dc-Cover-u0b349upqugfio195s4lpk8144-20190213120303.Medi.jpeg">\n\n    <ion-card-header>\n\n      <!-- <ion-card-subtitle>Get Started</ion-card-subtitle> -->\n\n      <ion-card-title>Vehicle Name - {{parameters.Device_Name}}</ion-card-title>\n\n      <ion-row>\n\n        <ion-col>\n\n          Total Payment\n\n        </ion-col>\n\n        <ion-col>\n\n          {{currencyIcon}}{{paymentAmount/100}}\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <!-- <ion-button expand="full" color="success" (click)="payWithRazor()">Pay with RazorPay</ion-button> -->\n\n      <button ion-button full color="gpsc" (click)="payWithRazor()">Pay with RazorPay</button>\n\n    </ion-card-content>\n\n  </ion-card>\n\n  <!-- <ion-list lines="none">\n\n    <ion-list-header>\n\n      <ion-label>Resources</ion-label>\n\n    </ion-list-header>\n\n    <ion-item href="https://github.com/razorpay/razorpay-cordova">\n\n      <ion-icon slot="start" color="medium" name="book"></ion-icon>\n\n      <ion-label>Razorpay Plugin Documentation</ion-label>\n\n    </ion-item>\n\n    <ion-item href="https://medium.com/enappd/how-to-integrate-razorpay-in-ionic-4-apps-and-pwa-612bb11482d9">\n\n      <ion-icon slot="start" color="medium" name="grid"></ion-icon>\n\n      <ion-label>Ionic 4 - Razorpay Integration</ion-label>\n\n    </ion-item>\n\n    <ion-item href="https://store.enappd.com">\n\n      <ion-icon slot="start" color="medium" name="color-fill"></ion-icon>\n\n      <ion-label>More Ionic 4 Starters</ion-label>\n\n    </ion-item>\n\n  </ion-list> -->\n\n\n\n  <!-- <div style="padding: 0% 10% 0% 10%;">\n\n\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">{{ "Paytm" | translate }}</h4>\n\n    <ion-item style="padding-left: 0px;">\n\n      <ion-input [disabled]=\'inputform\' [(ngModel)]="paytmnumber" type="number" placeholder="Mobile Number*">\n\n      </ion-input>\n\n    </ion-item>\n\n    <p style="color:grey">{{ "Linking wallet is one time process. Next time, checkout will be a breeze!" | translate }}\n\n    </p>\n\n  </div>\n\n  <button [disabled]=\'inputform\' ion-button full color="gpsc"\n\n    style="height: 35px;width: 45%;margin-left: 30%;font-size: 15px;color: white;margin-top: 15px;color: rgb(255, 255, 255);"\n\n    (click)="paytmwallet()">{{ "Continue" | translate }}</button>\n\n  <ion-row style="padding: 2% 10% 0% 10%;" *ngIf="successresponse">\n\n    <h4 style="font-size: 1.8rem;letter-spacing: 2px;color: #777575;">{{ "Enter verification code" | translate }}</h4>\n\n    <p style="margin-top: 0px;color: grey;margin-bottom: 0;">{{ "Paytm has send verification code on" | translate }}\n\n      {{paytmnumber}} {{ "via sms Please enter it below, and you are done!" | translate }}</p>\n\n    <ion-item style="padding: 0px;">\n\n      <ion-input [(ngModel)]="paytmotp" type="number" placeholder="Enter OTP"></ion-input>\n\n    </ion-item>\n\n    <ion-col col-sm-6>\n\n      <button ion-button full\n\n        style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n\n        (click)="resndOTP()">{{ "Resend" | translate }}</button>\n\n    </ion-col>\n\n    <ion-col col-sm-6>\n\n      <button ion-button full [disabled]="paytmotp == undefined"\n\n        style="height: 35px;background: rgb(220, 220, 220);font-size: 15px;transition: none 0s ease 0s;margin-top: 15px;color: #4a4747;"\n\n        (click)="paytmAuthantication()">{{ "Proceed" | translate }}</button>\n\n    </ion-col>\n\n  </ion-row> -->\n\n</ion-content>'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\add-devices\paytmwalletlogin\paytmwalletlogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], PaytmwalletloginPage);
    return PaytmwalletloginPage;
}());

//# sourceMappingURL=paytmwalletlogin.js.map

/***/ })

});
//# sourceMappingURL=58.js.map