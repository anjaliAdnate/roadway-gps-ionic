webpackJsonp([57],{

/***/ 574:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateDevicePageModule", function() { return UpdateDevicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_device__ = __webpack_require__(652);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var UpdateDevicePageModule = /** @class */ (function () {
    function UpdateDevicePageModule() {
    }
    UpdateDevicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__update_device__["a" /* UpdateDevicePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__update_device__["a" /* UpdateDevicePage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ]
        })
    ], UpdateDevicePageModule);
    return UpdateDevicePageModule;
}());

//# sourceMappingURL=update-device.module.js.map

/***/ }),

/***/ 652:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdateDevicePage = /** @class */ (function () {
    function UpdateDevicePage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.allGroup = [];
        this.devicedetail = {};
        this.vehData = navPar.get("vehData");
        console.log("vehicle data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);
        this.currentYear = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");
        if (this.vehData.expiration_date == null) {
            this.eDate = this.currentYear;
        }
        else {
            this.eDate = __WEBPACK_IMPORTED_MODULE_3_moment__(this.vehData.expiration_date).format('YYYY-MM-DD');
        }
        var d_type, d_user, v_type, g_name;
        if (this.vehData.device_model != undefined) {
            d_type = this.vehData.device_model['device_type'];
        }
        else {
            d_type = '';
        }
        if (this.vehData.user != undefined) {
            d_user = this.vehData.user['first_name'];
        }
        else {
            d_user = '';
        }
        if (this.vehData.vehicleType != undefined) {
            v_type = this.vehData.vehicleType['brand'];
        }
        else {
            v_type = '';
        }
        if (this.vehData.vehicleGroup != undefined) {
            g_name = this.vehData.vehicleGroup['name'];
        }
        else {
            g_name = '';
        }
        // console.log("device model=> ", this.vehData.device_model['device_type'])
        // console.log("user => ", this.vehData.user['first_name'])
        // console.log("brand => ", this.vehData.vehicleType['brand'])
        // console.log("device model=> ", this.vehData.device_model['device_type'])
        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        console.log("one month later date=> " + __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD"));
        // =============== end
        this.updatevehForm = formBuilder.group({
            device_name: [this.vehData.Device_Name, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            device_id: [this.vehData.Device_ID, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            driver: [this.vehData.driver_name],
            sim_number: [this.vehData.sim_number, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            contact_number: [this.vehData.contact_number, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"]],
            SpeedLimit: [this.vehData.SpeedLimit],
            ExipreDate: [this.eDate, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            gName: [g_name],
            device_type: [d_type],
            first_name: [d_user],
            brand: [v_type]
        });
        console.log("date format=> " + __WEBPACK_IMPORTED_MODULE_3_moment__(this.vehData.expiration_date).format('YYYY-MM-DD'));
        // this.updatevehForm.patchValue({
        //     ExipreDate: moment(this.vehData.expiration_date).format('YYYY-MM-DD')
        // })
    }
    UpdateDevicePage.prototype.ngOnInit = function () {
        this.getGroup();
        this.getDeviceModel();
        this.getSelectUser();
        this.getVehicleType();
    };
    UpdateDevicePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateDevicePage.prototype.updateDevices = function () {
        var that = this;
        that.submitAttempt = true;
        if (that.updatevehForm.valid) {
            ///////////////////////////
            // debugger;
            console.log("form valuess=> ", that.updatevehForm.value);
            if (that.updatevehForm.value.device_type != '') {
                if (that.vehData.device_model != null || that.vehData.device_model != undefined) {
                    if (that.updatevehForm.value.device_type == that.vehData.device_model['device_type']) {
                        that.modeldata_id = that.vehData.device_model['_id'];
                    }
                    else {
                        that.modeldata_id = that.modeldata._id;
                    }
                }
                else {
                    if (that.modeldata._id != undefined) {
                        that.modeldata_id = that.modeldata._id;
                    }
                    else {
                        that.modeldata_id = null;
                    }
                }
            }
            else {
                that.modeldata_id = null;
            }
            if (that.updatevehForm.value.gName != '') {
                if (that.vehData.vehicleGroup != null || that.vehData.vehicleGroup != undefined) {
                    if (that.updatevehForm.value.gName == that.vehData.vehicleGroup['name']) {
                        that.groupstaus_id = that.vehData.vehicleGroup['_id'];
                    }
                    else {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                }
                else {
                    if (that.groupstaus._id != undefined) {
                        that.groupstaus_id = that.groupstaus._id;
                    }
                    else {
                        that.groupstaus_id = null;
                    }
                }
            }
            else {
                that.groupstaus_id = null;
            }
            if (that.updatevehForm.value.first_name != '') {
                if (that.vehData.user != null || that.vehData.user != undefined) {
                    if (that.updatevehForm.value.first_name == that.vehData.user['first_name']) {
                        that.userdata_id = that.vehData.user['_id'];
                    }
                    else {
                        that.userdata_id = that.userdata._id;
                    }
                }
                else {
                    if (that.userdata._id != undefined) {
                        that.userdata_id = that.userdata._id;
                    }
                    else {
                        that.userdata_id = null;
                    }
                }
            }
            else {
                that.userdata_id = null;
            }
            if (that.updatevehForm.value.brand != '') {
                if (that.vehData.vehicleType != null || that.vehData.vehicleType != undefined) {
                    if (that.updatevehForm.value.brand == that.vehData.vehicleType['brand']) {
                        that.vehicleType_id = that.vehData.vehicleType['_id'];
                    }
                    else {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                }
                else {
                    if (that.vehicleType._id != undefined) {
                        that.vehicleType_id = that.vehicleType._id;
                    }
                    else {
                        that.vehicleType_id = null;
                    }
                }
            }
            else {
                that.vehicleType_id = null;
            }
            if (that.updatevehForm.value.SpeedLimit != '') {
                that.speed = that.updatevehForm.value.SpeedLimit;
            }
            else {
                that.speed = 1;
            }
            ///////////////////////////
            debugger;
            that.devicedetail = {
                "_id": that.vehData._id,
                "devicename": that.updatevehForm.value.device_name,
                "drname": that.updatevehForm.value.driver,
                "deviceid": that.updatevehForm.value.device_id,
                "sim": that.updatevehForm.value.sim_number,
                "iconType": null,
                "dphone": that.updatevehForm.value.contact_number,
                "speed": that.speed,
                "vehicleGroup": that.groupstaus_id,
                "device_model": that.modeldata_id,
                "expiration_date": new Date(that.updatevehForm.value.ExipreDate).toISOString(),
                "user": that.userdata_id,
                "vehicleType": that.vehicleType_id
            };
            console.log("all details=> " + that.devicedetail);
            that.apiCall.startLoading().present();
            that.apiCall.deviceupdateCall(that.devicedetail)
                .subscribe(function (data) {
                that.apiCall.stopLoading();
                var editdata = data;
                console.log("editdata=> " + editdata);
                var toast = that.toastCtrl.create({
                    message: editdata.message,
                    position: 'top',
                    duration: 2000
                });
                toast.onDidDismiss(function () {
                    console.log('Dismissed toast');
                    that.viewCtrl.dismiss(editdata);
                });
                toast.present();
            }, function (err) {
                that.apiCall.stopLoading();
                var body = err._body;
                var msg = JSON.parse(body);
                var alert = that.alerCtrl.create({
                    title: 'Oops!',
                    message: msg.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    UpdateDevicePage.prototype.deviceModelata = function (deviceModel) {
        console.log("deviceModel" + deviceModel);
        this.modeldata = deviceModel;
        console.log("modal data device_type=> " + this.modeldata.device_type);
    };
    UpdateDevicePage.prototype.GroupStatusdata = function (status) {
        console.log(status);
        this.groupstaus = status;
        console.log("groupstaus=> " + this.groupstaus._id);
    };
    UpdateDevicePage.prototype.userselectData = function (userselect) {
        console.log(userselect);
        this.userdata = userselect;
        console.log("userdata=> " + this.userdata.first_name);
    };
    UpdateDevicePage.prototype.vehicleTypeselectData = function (vehicletype) {
        console.log(vehicletype);
        this.vehicleType = vehicletype;
        console.log("vehType=> " + this.vehicleType._id);
    };
    UpdateDevicePage.prototype.getGroup = function () {
        var _this = this;
        console.log("get group");
        var baseURLp = 'https://www.oneqlik.in/groups/getGroups_list?uid=' + this.islogin._id;
        this.apiCall.startLoading().present();
        this.apiCall.groupsCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data["group_details"] !== undefined) {
                if (data["group_details"].length > 0) {
                    _this.allGroup = data["group_details"];
                    for (var i = 0; i < _this.allGroup.length; i++) {
                        _this.allGroupName = _this.allGroup[i].name;
                        // console.log("allGroupName=> "+this.allGroupName);
                    }
                    console.log("allGroupName=> " + _this.allGroupName);
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UpdateDevicePage.prototype.getDeviceModel = function () {
        var _this = this;
        console.log("getdevices");
        var baseURLp = 'https://www.oneqlik.in/deviceModel/getDeviceModel';
        this.apiCall.getDeviceModelCall(baseURLp)
            .subscribe(function (data) {
            _this.deviceModel = data;
            console.log("selected user=> ", _this.deviceModel);
        }, function (err) {
            console.log(err);
        });
    };
    UpdateDevicePage.prototype.getSelectUser = function () {
        var _this = this;
        console.log("get user");
        var baseURLp = 'https://www.oneqlik.in/users/getAllUsers?dealer=' + this.islogin._id;
        this.apiCall.getAllUsersCall(baseURLp)
            .subscribe(function (data) {
            _this.selectUser = data;
            console.log("selected user=> ", _this.selectUser);
        }, function (error) {
            console.log(error);
        });
    };
    UpdateDevicePage.prototype.getVehicleType = function () {
        var _this = this;
        console.log("get getVehicleType");
        var baseURLp = 'https://www.oneqlik.in/vehicleType/getVehicleTypes?user=' + this.islogin._id;
        // this.apiCall.startLoading().present();
        this.apiCall.getVehicleTypesCall(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.allVehicle = data;
            console.log("all vehicles=> ", _this.allVehicle);
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    UpdateDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-device',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\add-devices\update-device\update-device.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{ "Update Vehicle Details" | translate }}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="dismiss()">\n\n                <ion-icon name="close-circle"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="updatevehForm">\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "Registration Number*" | translate }}</ion-label>\n\n            <ion-input formControlName="device_name" type="text" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.device_name.valid && (updatevehForm.controls.device_name.dirty || submitAttempt)">\n\n            <p>{{ "registration number is required!" | translate }}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "IMEI" | translate }} {{ "id" | translate }}*</ion-label>\n\n            <ion-input formControlName="device_id" type="text" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "Sim Number*" | translate }}</ion-label>\n\n            <ion-input formControlName="sim_number" type="number" maxlength="13" minlength="10" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.sim_number.valid && (updatevehForm.controls.sim_number.dirty || submitAttempt)">\n\n            <p>{{ "sim number is required & sould be 10 or 13 digits!" | translate }}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "Driver Name" | translate }}</ion-label>\n\n            <ion-input formControlName="driver" type="text" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "Driver\'s Number" | translate }}</ion-label>\n\n            <ion-input formControlName="contact_number" type="number" maxlength="10" minlength="10" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.contact_number.valid && (updatevehForm.controls.contact_number.dirty || submitAttempt)">\n\n            <p>{{ "mobile number sould be 10 digits!" | translate }}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "Speed Limit" | translate }}</ion-label>\n\n            <ion-input formControlName="SpeedLimit" type="number" style="margin-left: 2px;"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label fixed style="min-width: 50% !important;">{{ "Expiry Date:" | translate }}*</ion-label>\n\n            <ion-input type="date" formControlName="ExipreDate" min="{{minDate}}" style="margin-left: 2px;"></ion-input>\n\n\n\n        </ion-item>\n\n        <ion-item class="logitem1" *ngIf="!updatevehForm.controls.ExipreDate.valid && (updatevehForm.controls.ExipreDate.dirty || submitAttempt)">\n\n            <p>{{ "expiry date is required!" | translate }}</p>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{ "Device Model" | translate }}</ion-label>\n\n            <ion-select formControlName="device_type" style="min-width:49%;">\n\n                <ion-option *ngFor="let deviceModelname of deviceModel" [value]="deviceModelname.device_type" (ionSelect)="deviceModelata(deviceModelname)">{{deviceModelname.device_type}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{ "Group" | translate }}</ion-label>\n\n            <ion-select formControlName="gName" style="min-width:49%;">\n\n                <ion-option *ngFor="let groupname of allGroup" [value]="groupname.name" (ionSelect)="GroupStatusdata(groupname)">{{groupname.name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{ "Select User" | translate }}</ion-label>\n\n            <ion-select formControlName="first_name" style="min-width:49%;">\n\n                <ion-option *ngFor="let user of selectUser" [value]="user.first_name" (ionSelect)="userselectData(user)">{{user.first_name}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n            <ion-label>{{ "Vehicle Type" | translate }}</ion-label>\n\n            <ion-select formControlName="brand" style="min-width:49%;">\n\n                <ion-option *ngFor="let veh of allVehicle" [value]="veh.brand" (ionSelect)="vehicleTypeselectData(veh)">{{veh.brand}}</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n    </form>\n\n\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col style="text-align: center;">\n\n                <button ion-button clear color="light" (click)="updateDevices()">{{ "UPDATE VEHICLE DETAILS" | translate }}</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\add-devices\update-device\update-device.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], UpdateDevicePage);
    return UpdateDevicePage;
}());

//# sourceMappingURL=update-device.js.map

/***/ })

});
//# sourceMappingURL=57.js.map