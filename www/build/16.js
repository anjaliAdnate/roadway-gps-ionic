webpackJsonp([16],{

/***/ 570:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SosReportPageModule", function() { return SosReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sos_report__ = __webpack_require__(645);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SosReportPageModule = /** @class */ (function () {
    function SosReportPageModule() {
    }
    SosReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sos_report__["a" /* SosReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__sos_report__["a" /* SosReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], SosReportPageModule);
    return SosReportPageModule;
}());

//# sourceMappingURL=sos-report.module.js.map

/***/ }),

/***/ 645:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SosReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SosReportPage = /** @class */ (function () {
    function SosReportPage(navCtrl, navParams, apicall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicall = apicall;
        this.toastCtrl = toastCtrl;
        this.pageNo = 0;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        this.getdevices();
    }
    SosReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SosReportPage');
    };
    SosReportPage.prototype.getsosdevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.sos_id = selectedVehicle.Device_Name;
    };
    SosReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicall.startLoading().present();
        this.apicall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.getSOSReport();
        }, function (err) {
            _this.apicall.stopLoading();
            console.log(err);
        });
    };
    SosReportPage.prototype.getSOS = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.sos_id = selectedVehicle.Device_ID;
    };
    SosReportPage.prototype.getSOSReport = function () {
        var _this = this;
        var _url;
        // if (this.sos_id == undefined) {
        //   this.sos_id = "";
        //   _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&_u=' + this.islogin._id;
        // } else {
        //   _url = 'https://www.oneqlik.in/notifs/SOSReport?from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&dev_id=' + this.sos_id + '&_u=' + this.islogin._id;
        // }
        _url = "https://www.oneqlik.in/notifs/statusReport";
        var payload = {};
        debugger;
        if (this.sos_id == undefined) {
            payload = {
                "draw": 3,
                "columns": [
                    {
                        "data": "_id"
                    },
                    {
                        "data": "device"
                    },
                    {
                        "data": "vehicleName"
                    },
                    {
                        "data": "timestamp"
                    },
                    {
                        "data": "address"
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": this.pageNo,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false
                },
                "find": {
                    "user": this.islogin._id,
                    "type": "SOS",
                    "timestamp": {
                        "$gte": new Date(this.datetimeStart).toISOString(),
                        "$lte": new Date(this.datetimeEnd).toISOString()
                    }
                }
            };
        }
        else {
            payload = {
                "draw": 3,
                "columns": [
                    {
                        "data": "_id"
                    },
                    {
                        "data": "device"
                    },
                    {
                        "data": "vehicleName"
                    },
                    {
                        "data": "timestamp"
                    },
                    {
                        "data": "address"
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": this.pageNo,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false
                },
                "find": {
                    "user": this.islogin._id,
                    "type": "SOS",
                    "device": this.sos_id,
                    "timestamp": {
                        "$gte": new Date(this.datetimeStart).toISOString(),
                        "$lte": new Date(this.datetimeEnd).toISOString()
                    }
                }
            };
        }
        this.apicall.startLoading().present();
        // this.apicall.getSOSReportAPI(_url)
        this.apicall.urlpasseswithdata(_url, payload)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.sosData = data.data;
            if (_this.sosData.length == 0) {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected Dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicall.stopLoading();
            console.log(error);
        });
    };
    SosReportPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.pageNo += 1;
        setTimeout(function () {
            that.innerFunc(infiniteScroll);
        }, 200);
    };
    SosReportPage.prototype.innerFunc = function (infiniteScroll) {
        var _this = this;
        var that = this;
        var _url1 = "https://www.oneqlik.in/notifs/statusReport";
        var payload = {};
        if (that.sos_id == undefined) {
            that.sos_id = "";
            payload = {
                "draw": 3,
                "columns": [
                    {
                        "data": "_id"
                    },
                    {
                        "data": "device"
                    },
                    {
                        "data": "vehicleName"
                    },
                    {
                        "data": "timestamp"
                    },
                    {
                        "data": "address"
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": that.pageNo,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false
                },
                "find": {
                    "user": that.islogin._id,
                    "type": "SOS",
                    "timestamp": {
                        "$gte": new Date(that.datetimeStart).toISOString(),
                        "$lte": new Date(that.datetimeEnd).toISOString()
                    }
                }
            };
        }
        else {
            payload = {
                "draw": 3,
                "columns": [
                    {
                        "data": "_id"
                    },
                    {
                        "data": "device"
                    },
                    {
                        "data": "vehicleName"
                    },
                    {
                        "data": "timestamp"
                    },
                    {
                        "data": "address"
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": that.pageNo,
                "length": 10,
                "search": {
                    "value": "",
                    "regex": false
                },
                "find": {
                    "user": that.islogin._id,
                    "type": "SOS",
                    "device": that.sos_id,
                    "timestamp": {
                        "$gte": new Date(that.datetimeStart).toISOString(),
                        "$lte": new Date(that.datetimeEnd).toISOString()
                    }
                }
            };
        }
        this.apicall.urlpasseswithdata(_url1, payload)
            .subscribe(function (data) {
            for (var i = 0; i < data.data.length; i++) {
                that.sosData.push(data.data[i]);
            }
            console.log("data length: " + that.sosData.length);
            infiniteScroll.complete();
        }, function (error) {
            _this.apicall.stopLoading();
            console.log(error);
            infiniteScroll.complete();
        });
    };
    SosReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sos-report',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\sos-report\sos-report.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{\'SOS Report\' | translate}}</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">{{\'Select Vehicle\' | translate}}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getSOS(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{\'From Date\' | translate}}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{\'To Date\' | translate}}</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getSOSReport();"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let s of sosData">\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_green_icon.png">\n\n        <!-- <img src="assets/imgs/truck_icon_green.png" *ngIf="(s.device.iconType == \'truck\')">\n\n        <img src="assets/imgs/bus_green.png" *ngIf="(s.device.iconType == \'bus\')">\n\n        <img src="assets/imgs/bike_green_icon.png" *ngIf="(s.device.iconType == \'bike\')"> -->\n\n      </ion-avatar>\n\n      <ion-label>{{s.vehicleName}}</ion-label>\n\n    </ion-item>\n\n    <ion-card-content>\n\n      <ion-row style="padding-top:5px;">\n\n        <ion-col col-1>\n\n            <ion-icon name="time" style="font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n            {{s.timestamp | date:\'full\'}}\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{\'Loading more data...\' | translate}}">\n\n      </ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\sos-report\sos-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], SosReportPage);
    return SosReportPage;
}());

//# sourceMappingURL=sos-report.js.map

/***/ })

});
//# sourceMappingURL=16.js.map