webpackJsonp([39],{

/***/ 544:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpensesPageModule", function() { return ExpensesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expenses__ = __webpack_require__(614);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ExpensesPageModule = /** @class */ (function () {
    function ExpensesPageModule() {
    }
    ExpensesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expenses__["a" /* ExpensesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__expenses__["a" /* ExpensesPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], ExpensesPageModule);
    return ExpensesPageModule;
}());

//# sourceMappingURL=expenses.module.js.map

/***/ }),

/***/ 614:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpensesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { TranslateService } from '@ngx-translate/core';
var ExpensesPage = /** @class */ (function () {
    function ExpensesPage(navCtrl, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.portstemp = [];
        this._vehId = {};
        this.expensesData = [];
        this.total = 0;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format();
    }
    ExpensesPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    ExpensesPage.prototype.onTypeDetail = function (exp) {
        console.log(exp);
        this.navCtrl.push('ExpenseTypeDetailPage', {
            expense: exp,
            dateFrom: this.datetimeStart,
            dateTo: this.datetimeEnd,
            userId: this.islogin._id,
            portstemp: this.portstemp,
            vehId: this._vehId._id
        });
    };
    ExpensesPage.prototype.getExpenceTypes = function () {
        var _this = this;
        var _bUrl;
        this.total = 0;
        if (this._vehId._id != undefined) {
            _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString() + "&vehicle=" + this._vehId._id;
        }
        else {
            _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString();
        }
        // const _bUrl = this.apiCall.mainUrl + "expense/expensebycateogry?user=" + this.islogin._id + "&fdate=" + new Date(this.datetimeStart).toISOString() + "&tdate=" + new Date(this.datetimeEnd).toISOString();
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_bUrl)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.expensesData = [];
            _this.expensesData = data.expenseobj;
            for (var j = 0; j < data.expenseobj.length; j++) {
                _this.total += data.expenseobj[j].total;
            }
            console.log("expense type=> " + data);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    ExpensesPage.prototype.addExpence = function () {
        this.navCtrl.push('AddExpensePage', { vehicleList: this.portstemp });
    };
    ExpensesPage.prototype.getExpenseList = function () {
        this.getExpenceTypes();
    };
    ExpensesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ExpensesPage');
    };
    ExpensesPage.prototype.ionViewWillEnter = function () {
        this.showBtn = false;
        this._vehId = {};
        this.selectedVehicle = undefined;
        console.log("view will enter");
        this.getExpenseList();
    };
    ExpensesPage.prototype.toastMessage = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        });
        toast.present();
    };
    ExpensesPage.prototype.getid = function (veh) {
        this._vehId = veh;
        this.showBtn = true;
    };
    ExpensesPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            // this.apiCall.stopLoading();
            console.log(err);
        });
    };
    ExpensesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-expenses',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\expenses\expenses.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "Expenses" | translate }}</ion-title>\n\n    <ion-buttons end *ngIf="showBtn">\n\n      <button ion-button (click)="ionViewWillEnter()">\n\n        {{ "All Vehicles" | translate }}\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">{{\n\n      "Select Vehicle" | translate\n\n    }}</ion-label>\n\n    <select-searchable\n\n      item-content\n\n      [(ngModel)]="selectedVehicle"\n\n      [items]="portstemp"\n\n      itemValueField="Device_Name"\n\n      itemTextField="Device_Name"\n\n      [canSearch]="true"\n\n      (onChange)="getid(selectedVehicle)"\n\n    >\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20 class="cust">\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n\n        <ion-datetime\n\n          displayFormat="DD-MM-YYYY hh:mm a"\n\n          pickerFormat="DD/MM/YY hh:mm a"\n\n          [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n\n        >\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20 class="cust">\n\n      <ion-label>\n\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n\n        <ion-datetime\n\n          displayFormat="DD-MM-YYYY hh:mm a"\n\n          pickerFormat="DD/MM/YY hh:mm a"\n\n          [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n\n        >\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon\n\n          ios="ios-search"\n\n          md="md-search"\n\n          style="font-size:2.3em;"\n\n          (click)="getExpenseList()"\n\n        >\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row>\n\n          <ion-col\n\n            col-6\n\n            text-center\n\n            *ngFor="let exp of expensesData; let i = index"\n\n            offest-sm="2"\n\n            no-padding\n\n            (click)="onTypeDetail(exp)"\n\n          >\n\n            <ion-card style="border-radius: 16px;">\n\n              <ion-card-header style="padding: 16px 0px 0px 0px;">\n\n                <img\n\n                  src="assets/imgs/001-wage.png"\n\n                  *ngIf="exp._id === \'salary\'"\n\n                />\n\n                <img src="assets/imgs/002-gas.png" *ngIf="exp._id === \'fuel\'" />\n\n                <img\n\n                  src="assets/imgs/settings.png"\n\n                  *ngIf="exp._id === \'tools\'"\n\n                />\n\n                <img\n\n                  src="assets/imgs/carpenter.png"\n\n                  *ngIf="exp._id === \'labor\'"\n\n                />\n\n                <img\n\n                  src="assets/imgs/003-customer-support.png"\n\n                  *ngIf="exp._id === \'service\'"\n\n                />\n\n                <img\n\n                  src="assets/imgs/toll-road.png"\n\n                  *ngIf="exp._id === \'toll\'"\n\n                />\n\n                <img\n\n                  src="assets/imgs/004-wheel.png"\n\n                  *ngIf="exp._id === \'other\'"\n\n                />\n\n                <ion-card-title style="font-size: 1.7rem;" color="gpsc">{{\n\n                  exp._id | titlecase\n\n                }}</ion-card-title>\n\n              </ion-card-header>\n\n              <ion-card-content\n\n                ><p style="margin: 0px; padding: 0px; font-size: 1em">\n\n                  {{ exp.total | number: "2." }} {{ exp.currency }}\n\n                </p></ion-card-content\n\n              >\n\n            </ion-card>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-fab right bottom>\n\n    <button ion-fab color="gpsc" (click)="addExpence()">\n\n      <ion-icon name="add"></ion-icon>\n\n    </button>\n\n  </ion-fab>\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n  <ion-toolbar>\n\n    <ion-row no-padding>\n\n      <ion-col\n\n        width-50\n\n        style="text-align: center; color: #fff; font-size: 1.5em;"\n\n      >\n\n        <b>{{ "Total" | translate }} {{ total | number: "2." }} INR</b>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\expenses\expenses.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ExpensesPage);
    return ExpensesPage;
}());

//# sourceMappingURL=expenses.js.map

/***/ })

});
//# sourceMappingURL=39.js.map