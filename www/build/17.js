webpackJsonp([17],{

/***/ 568:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(643);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 643:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, formBuilder, apiService, toastCtrl, alertCtrl, platform, keyboard) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.apiService = apiService;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.keyboard = keyboard;
        this.type1 = "password";
        this.type = "password";
        this.show = false;
        this.show1 = false;
        this.signupForm = formBuilder.group({
            mob_num: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].maxLength(13), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required])],
            email_add: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            Name: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            pass: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            cnfrm_passwrd: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
        });
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.doLogin = function () {
        this.navCtrl.setRoot("LoginPage");
    };
    SignupPage.prototype.IsDealer = function (check) {
        this.isdealer = check;
    };
    SignupPage.prototype.getotp = function () {
        var _this = this;
        debugger;
        this.isdealer = false;
        this.submitAttempt = true;
        if (this.signupForm.valid) {
            this.usersignupdetails = this.signupForm.value;
            localStorage.setItem('usersignupdetails', this.usersignupdetails);
            this.signupDetails = localStorage.getItem("usersignupdetails");
            if (this.signupForm.value.cnfrm_passwrd && this.signupForm.value.email_add && this.signupForm.value.mob_num && this.signupForm.value.Name && this.signupForm.value.pass) {
                if (this.signupForm.value.pass == this.signupForm.value.cnfrm_passwrd) {
                    var usersignupdata = {
                        "first_name": this.signupForm.value.Name,
                        "last_name": '',
                        "email": this.signupForm.value.email_add,
                        "password": this.signupForm.value.pass,
                        "confirmpass": this.signupForm.value.cnfrm_passwrd,
                        "phone": String(this.signupForm.value.mob_num),
                        "status": false,
                        "supAdmin": "5d28a70dae1cbd6fdfa83620",
                        "isDealer": false
                    };
                    this.apiService.startLoading();
                    this.apiService.signupApi(usersignupdata)
                        .subscribe(function (response) {
                        var phone = usersignupdata.phone;
                        localStorage.setItem("mobnum", phone);
                        _this.apiService.stopLoading();
                        _this.signupUseradd = response;
                        var toast = _this.toastCtrl.create({
                            message: response.message,
                            duration: 3000,
                            position: 'top'
                        });
                        toast.onDidDismiss(function () {
                            if (response.message === 'Email ID or Mobile Number already exists') {
                                _this.navCtrl.push("LoginPage");
                            }
                            else if (response.message === "OTP sent successfully") {
                                _this.navCtrl.push('SignupOtpPage');
                            }
                        });
                        toast.present();
                    }, function (err) {
                        _this.apiService.stopLoading();
                        var toast = _this.toastCtrl.create({
                            message: "Something went wrong. Please check your net connection..",
                            duration: 2500,
                            position: "top"
                        });
                        toast.present();
                    });
                }
                else {
                    var alertPopup = this.alertCtrl.create({
                        title: 'Warning!',
                        message: "Password and Confirm Password Not Matched",
                        buttons: ['OK']
                    });
                    alertPopup.present();
                }
            }
        }
    };
    SignupPage.prototype.gotoOtp = function () {
        this.navCtrl.push('SignupOtpPage');
    };
    SignupPage.prototype.gotoLogin = function () {
        this.navCtrl.push("LoginPage");
    };
    SignupPage.prototype.toggleShow = function (ev) {
        if (ev == 0) {
            this.show = !this.show;
            if (this.show) {
                this.type = "text";
            }
            else {
                this.type = "password";
            }
        }
        else {
            this.show1 = !this.show1;
            if (this.show1) {
                this.type1 = "text";
            }
            else {
                this.type1 = "password";
            }
        }
    };
    SignupPage.prototype.upload = function () {
        this.navCtrl.push("DrivingLicensePage");
    };
    SignupPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup',template:/*ion-inline-start:"D:\New\roadway-gps-ionic\src\pages\signup\signup.html"*/'<ion-content no-padding>\n\n  <div width="100%" height="100%">\n\n    <div\n\n      style="background: #d80622;padding-top: 15%;padding-bottom: 10%;"\n\n      height="50%"\n\n    >\n\n      <ion-card\n\n        style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;"\n\n      >\n\n        <img src="assets/imgs/icon.png" style="width: 60%;margin: 18%;" />\n\n      </ion-card>\n\n      <h5\n\n        style="color: white;font-weight: 500;text-align: center;font-size: 3.6rem;padding-top: 0%;padding-bottom: 14%;"\n\n      >\n\n        {{ "SIGN UP" | translate }}\n\n      </h5>\n\n    </div>\n\n    <div>\n\n      <div class="row">\n\n        <ion-card\n\n          class="col-sm-12 col-12 col-md-12"\n\n          style="margin-top: -21%;border-radius: 4%;height: auto;overflow-y: auto;padding-bottom: 7%;"\n\n        >\n\n          <form [formGroup]="signupForm">\n\n            <ion-row\n\n              style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;"\n\n            >\n\n              <ion-col col-10 no-padding>\n\n                <ion-input\n\n                  placeholder="{{ \'First Name*\' | translate }}"\n\n                  type="text"\n\n                  formControlName="Name"\n\n                ></ion-input>\n\n              </ion-col>\n\n              <ion-col\n\n                col-2\n\n                no-padding\n\n                style="text-align: right;margin-top: 2%; color: gray;"\n\n              >\n\n                <ion-icon name="person" style="font-size: 2.8rem"></ion-icon>\n\n              </ion-col>\n\n            </ion-row>\n\n            <ion-row\n\n              style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;"\n\n            >\n\n              <ion-col col-10 no-padding>\n\n                <ion-input\n\n                  placeholder="{{ \'Mobile Number*\' | translate }}"\n\n                  type="number"\n\n                  formControlName="mob_num"\n\n                  minlength="10"\n\n                  maxlength="13"\n\n                >\n\n                </ion-input>\n\n              </ion-col>\n\n              <ion-col\n\n                col-2\n\n                no-padding\n\n                style="text-align: right;margin-top: 2%;color: gray;"\n\n              >\n\n                <ion-icon name="call" style="font-size:2.8rem"></ion-icon>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n            <p\n\n              *ngIf="\n\n                !signupForm.controls.mob_num.valid &&\n\n                (signupForm.controls.mob_num.dirty || submitAttempt)\n\n              "\n\n              class="err"\n\n            >\n\n              {{ "mobile number should be 10 digits!" | translate }}\n\n            </p>\n\n            <ion-row\n\n              style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;"\n\n            >\n\n              <ion-col col-10 no-padding>\n\n                <ion-input\n\n                  type="email"\n\n                  placeholder="{{ \'Email ID*\' | translate }}"\n\n                  formControlName="email_add"\n\n                ></ion-input>\n\n              </ion-col>\n\n              <ion-col\n\n                col-2\n\n                no-padding\n\n                style="text-align: right;margin-top: 2%;color: gray;"\n\n              >\n\n                <ion-icon name="mail" style="font-size:2.8rem"></ion-icon>\n\n              </ion-col>\n\n            </ion-row>\n\n            <p\n\n              *ngIf="\n\n                !signupForm.controls.email_add.valid &&\n\n                (signupForm.controls.email_add.dirty || submitAttempt)\n\n              "\n\n              class="err"\n\n            >\n\n              {{ "please enter valid email id!" | translate }}\n\n            </p>\n\n\n\n            <ion-row\n\n              style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;"\n\n            >\n\n              <ion-col col-10 no-padding>\n\n                <ion-input\n\n                  placeholder="{{\'Password*\' | translate}}"\n\n                  type="{{ type }}"\n\n                  formControlName="pass"\n\n                ></ion-input>\n\n              </ion-col>\n\n              <ion-col\n\n                col-2\n\n                no-padding\n\n                style="text-align: right;margin-top: 2%;color: gray;position:initial"\n\n                (click)="toggleShow(0)"\n\n              >\n\n                <ion-icon name="eye" style="font-size:2.8rem"></ion-icon>\n\n              </ion-col>\n\n            </ion-row>\n\n            <p\n\n              *ngIf="\n\n                !signupForm.controls.pass.valid &&\n\n                (signupForm.controls.pass.dirty || submitAttempt)\n\n              "\n\n              class="err"\n\n            >\n\n              {{\'Password required!\' | translate}}\n\n            </p>\n\n            <ion-row\n\n              style="border-bottom: 1px solid #b6afaf;margin: 8px 30px 0px 30px;"\n\n            >\n\n              <ion-col col-10 no-padding>\n\n                <ion-input\n\n                  type="{{ type1 }}"\n\n                  placeholder="{{\'Confirm Password*\' | translate}}"\n\n                  formControlName="cnfrm_passwrd"\n\n                ></ion-input>\n\n              </ion-col>\n\n              <ion-col\n\n                col-2\n\n                no-padding\n\n                style="text-align: right;margin-top: 2%;color: gray;"\n\n                (click)="toggleShow(1)"\n\n              >\n\n                <ion-icon name="eye" style="font-size:2.8rem"></ion-icon>\n\n              </ion-col>\n\n            </ion-row>\n\n            <p\n\n              *ngIf="\n\n                !signupForm.controls.cnfrm_passwrd.valid &&\n\n                (signupForm.controls.cnfrm_passwrd.dirty || submitAttempt)\n\n              "\n\n              class="err"\n\n            >\n\n              {{\'Please enter Confirm Password\' | translate}}\n\n            </p>\n\n          </form>\n\n        </ion-card>\n\n        <div class="col-sm-12 col-12 col-md-12 " style="margin:auto">\n\n          <button\n\n            ion-button\n\n            round\n\n            style="width: 18.2rem;color:white;background: #d80622;margin:-46px 0px 0px 2px;font-weight:400;text-transform: none;"\n\n            (click)="getotp()"\n\n          >\n\n            <b>{{\'SIGN UP\' | translate}}</b>\n\n          </button>\n\n          <div (click)="gotoLogin()">\n\n            <p style="color:grey;margin-left: -13px;">\n\n              {{\'Already have an account?\' | translate}}<span\n\n                style="color: black;margin-left: 5px;"\n\n                >{{\'SIGN IN\' | translate}}</span\n\n              >\n\n            </p>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\New\roadway-gps-ionic\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Keyboard"]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

});
//# sourceMappingURL=17.js.map